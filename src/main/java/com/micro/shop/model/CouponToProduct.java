package com.micro.shop.model;

import java.io.Serializable;

public class CouponToProduct implements Serializable {
    /**
     * 
     * 表 : bestfenxiao_coupon_to_product
     * 对应字段 : id
     */
    private Integer id;

    /**
     * 优惠券id
     * 表 : bestfenxiao_coupon_to_product
     * 对应字段 : coupon_id
     */
    private Integer couponId;

    /**
     * 产品id
     * 表 : bestfenxiao_coupon_to_product
     * 对应字段 : product_id
     */
    private Integer productId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table bestfenxiao_coupon_to_product
     *
     * @mbggenerated Sun Apr 22 10:00:05 CST 2018
     */
    private static final long serialVersionUID = 1L;

    /**
     * get method 
     *
     * @return bestfenxiao_coupon_to_product.id：
     */
    public Integer getId() {
        return id;
    }

    /**
     * set method 
     *
     * @param id  
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_coupon_to_product.coupon_id：优惠券id
     */
    public Integer getCouponId() {
        return couponId;
    }

    /**
     * set method 
     *
     * @param couponId  优惠券id
     */
    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_coupon_to_product.product_id：产品id
     */
    public Integer getProductId() {
        return productId;
    }

    /**
     * set method 
     *
     * @param productId  产品id
     */
    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    /**
     *
     * @param that
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        CouponToProduct other = (CouponToProduct) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getCouponId() == null ? other.getCouponId() == null : this.getCouponId().equals(other.getCouponId()))
            && (this.getProductId() == null ? other.getProductId() == null : this.getProductId().equals(other.getProductId()));
    }

    /**
     *
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getCouponId() == null) ? 0 : getCouponId().hashCode());
        result = prime * result + ((getProductId() == null) ? 0 : getProductId().hashCode());
        return result;
    }
}