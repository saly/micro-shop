package com.micro.shop.model;

import java.util.ArrayList;
import java.util.List;

import com.micro.shop.core.mybatis.Page;

public class OrderPeerpayCriteria {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table bestfenxiao_order_peerpay
     *
     * @mbggenerated Mon Apr 23 22:23:43 CST 2018
     */
    protected String orderByClause;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table bestfenxiao_order_peerpay
     *
     * @mbggenerated Mon Apr 23 22:23:43 CST 2018
     */
    protected boolean distinct;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table bestfenxiao_order_peerpay
     *
     * @mbggenerated Mon Apr 23 22:23:43 CST 2018
     */
    protected List<Criteria> oredCriteria;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table bestfenxiao_order_peerpay
     *
     * @mbggenerated Mon Apr 23 22:23:43 CST 2018
     */
    protected Page page;

    /**
     *
     */
    public OrderPeerpayCriteria() {
        oredCriteria = new ArrayList<Criteria>();
    }

    /**
     *
     * @param orderByClause
     */
    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    /**
     *
     */
    public String getOrderByClause() {
        return orderByClause;
    }

    /**
     *
     * @param distinct
     */
    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    /**
     *
     */
    public boolean isDistinct() {
        return distinct;
    }

    /**
     *
     */
    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    /**
     *
     * @param criteria
     */
    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    /**
     *
     */
    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    /**
     *
     */
    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
     *
     */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    /**
     *
     */
    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     *
     * @param page
     */
    public void setPage(Page page) {
        this.page=page;
    }

    /**
     *
     */
    public Page getPage() {
        return page;
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table bestfenxiao_order_peerpay
     *
     * @mbggenerated Mon Apr 23 22:23:43 CST 2018
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andDatelineIsNull() {
            addCriterion("dateline is null");
            return (Criteria) this;
        }

        public Criteria andDatelineIsNotNull() {
            addCriterion("dateline is not null");
            return (Criteria) this;
        }

        public Criteria andDatelineEqualTo(Integer value) {
            addCriterion("dateline =", value, "dateline");
            return (Criteria) this;
        }

        public Criteria andDatelineNotEqualTo(Integer value) {
            addCriterion("dateline <>", value, "dateline");
            return (Criteria) this;
        }

        public Criteria andDatelineGreaterThan(Integer value) {
            addCriterion("dateline >", value, "dateline");
            return (Criteria) this;
        }

        public Criteria andDatelineGreaterThanOrEqualTo(Integer value) {
            addCriterion("dateline >=", value, "dateline");
            return (Criteria) this;
        }

        public Criteria andDatelineLessThan(Integer value) {
            addCriterion("dateline <", value, "dateline");
            return (Criteria) this;
        }

        public Criteria andDatelineLessThanOrEqualTo(Integer value) {
            addCriterion("dateline <=", value, "dateline");
            return (Criteria) this;
        }

        public Criteria andDatelineIn(List<Integer> values) {
            addCriterion("dateline in", values, "dateline");
            return (Criteria) this;
        }

        public Criteria andDatelineNotIn(List<Integer> values) {
            addCriterion("dateline not in", values, "dateline");
            return (Criteria) this;
        }

        public Criteria andDatelineBetween(Integer value1, Integer value2) {
            addCriterion("dateline between", value1, value2, "dateline");
            return (Criteria) this;
        }

        public Criteria andDatelineNotBetween(Integer value1, Integer value2) {
            addCriterion("dateline not between", value1, value2, "dateline");
            return (Criteria) this;
        }

        public Criteria andOrderIdIsNull() {
            addCriterion("order_id is null");
            return (Criteria) this;
        }

        public Criteria andOrderIdIsNotNull() {
            addCriterion("order_id is not null");
            return (Criteria) this;
        }

        public Criteria andOrderIdEqualTo(Integer value) {
            addCriterion("order_id =", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotEqualTo(Integer value) {
            addCriterion("order_id <>", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdGreaterThan(Integer value) {
            addCriterion("order_id >", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("order_id >=", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdLessThan(Integer value) {
            addCriterion("order_id <", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdLessThanOrEqualTo(Integer value) {
            addCriterion("order_id <=", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdIn(List<Integer> values) {
            addCriterion("order_id in", values, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotIn(List<Integer> values) {
            addCriterion("order_id not in", values, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdBetween(Integer value1, Integer value2) {
            addCriterion("order_id between", value1, value2, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotBetween(Integer value1, Integer value2) {
            addCriterion("order_id not between", value1, value2, "orderId");
            return (Criteria) this;
        }

        public Criteria andPeerpayNoIsNull() {
            addCriterion("peerpay_no is null");
            return (Criteria) this;
        }

        public Criteria andPeerpayNoIsNotNull() {
            addCriterion("peerpay_no is not null");
            return (Criteria) this;
        }

        public Criteria andPeerpayNoEqualTo(String value) {
            addCriterion("peerpay_no =", value, "peerpayNo");
            return (Criteria) this;
        }

        public Criteria andPeerpayNoNotEqualTo(String value) {
            addCriterion("peerpay_no <>", value, "peerpayNo");
            return (Criteria) this;
        }

        public Criteria andPeerpayNoGreaterThan(String value) {
            addCriterion("peerpay_no >", value, "peerpayNo");
            return (Criteria) this;
        }

        public Criteria andPeerpayNoGreaterThanOrEqualTo(String value) {
            addCriterion("peerpay_no >=", value, "peerpayNo");
            return (Criteria) this;
        }

        public Criteria andPeerpayNoLessThan(String value) {
            addCriterion("peerpay_no <", value, "peerpayNo");
            return (Criteria) this;
        }

        public Criteria andPeerpayNoLessThanOrEqualTo(String value) {
            addCriterion("peerpay_no <=", value, "peerpayNo");
            return (Criteria) this;
        }

        public Criteria andPeerpayNoLike(String value) {
            addCriterion("peerpay_no like", value, "peerpayNo");
            return (Criteria) this;
        }

        public Criteria andPeerpayNoNotLike(String value) {
            addCriterion("peerpay_no not like", value, "peerpayNo");
            return (Criteria) this;
        }

        public Criteria andPeerpayNoIn(List<String> values) {
            addCriterion("peerpay_no in", values, "peerpayNo");
            return (Criteria) this;
        }

        public Criteria andPeerpayNoNotIn(List<String> values) {
            addCriterion("peerpay_no not in", values, "peerpayNo");
            return (Criteria) this;
        }

        public Criteria andPeerpayNoBetween(String value1, String value2) {
            addCriterion("peerpay_no between", value1, value2, "peerpayNo");
            return (Criteria) this;
        }

        public Criteria andPeerpayNoNotBetween(String value1, String value2) {
            addCriterion("peerpay_no not between", value1, value2, "peerpayNo");
            return (Criteria) this;
        }

        public Criteria andMoneyIsNull() {
            addCriterion("money is null");
            return (Criteria) this;
        }

        public Criteria andMoneyIsNotNull() {
            addCriterion("money is not null");
            return (Criteria) this;
        }

        public Criteria andMoneyEqualTo(Float value) {
            addCriterion("money =", value, "money");
            return (Criteria) this;
        }

        public Criteria andMoneyNotEqualTo(Float value) {
            addCriterion("money <>", value, "money");
            return (Criteria) this;
        }

        public Criteria andMoneyGreaterThan(Float value) {
            addCriterion("money >", value, "money");
            return (Criteria) this;
        }

        public Criteria andMoneyGreaterThanOrEqualTo(Float value) {
            addCriterion("money >=", value, "money");
            return (Criteria) this;
        }

        public Criteria andMoneyLessThan(Float value) {
            addCriterion("money <", value, "money");
            return (Criteria) this;
        }

        public Criteria andMoneyLessThanOrEqualTo(Float value) {
            addCriterion("money <=", value, "money");
            return (Criteria) this;
        }

        public Criteria andMoneyIn(List<Float> values) {
            addCriterion("money in", values, "money");
            return (Criteria) this;
        }

        public Criteria andMoneyNotIn(List<Float> values) {
            addCriterion("money not in", values, "money");
            return (Criteria) this;
        }

        public Criteria andMoneyBetween(Float value1, Float value2) {
            addCriterion("money between", value1, value2, "money");
            return (Criteria) this;
        }

        public Criteria andMoneyNotBetween(Float value1, Float value2) {
            addCriterion("money not between", value1, value2, "money");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("`name` is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("`name` is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("`name` =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("`name` <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("`name` >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("`name` >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("`name` <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("`name` <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("`name` like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("`name` not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("`name` in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("`name` not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("`name` between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("`name` not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andContentIsNull() {
            addCriterion("content is null");
            return (Criteria) this;
        }

        public Criteria andContentIsNotNull() {
            addCriterion("content is not null");
            return (Criteria) this;
        }

        public Criteria andContentEqualTo(String value) {
            addCriterion("content =", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotEqualTo(String value) {
            addCriterion("content <>", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentGreaterThan(String value) {
            addCriterion("content >", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentGreaterThanOrEqualTo(String value) {
            addCriterion("content >=", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLessThan(String value) {
            addCriterion("content <", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLessThanOrEqualTo(String value) {
            addCriterion("content <=", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLike(String value) {
            addCriterion("content like", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotLike(String value) {
            addCriterion("content not like", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentIn(List<String> values) {
            addCriterion("content in", values, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotIn(List<String> values) {
            addCriterion("content not in", values, "content");
            return (Criteria) this;
        }

        public Criteria andContentBetween(String value1, String value2) {
            addCriterion("content between", value1, value2, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotBetween(String value1, String value2) {
            addCriterion("content not between", value1, value2, "content");
            return (Criteria) this;
        }

        public Criteria andPayDatelineIsNull() {
            addCriterion("pay_dateline is null");
            return (Criteria) this;
        }

        public Criteria andPayDatelineIsNotNull() {
            addCriterion("pay_dateline is not null");
            return (Criteria) this;
        }

        public Criteria andPayDatelineEqualTo(Integer value) {
            addCriterion("pay_dateline =", value, "payDateline");
            return (Criteria) this;
        }

        public Criteria andPayDatelineNotEqualTo(Integer value) {
            addCriterion("pay_dateline <>", value, "payDateline");
            return (Criteria) this;
        }

        public Criteria andPayDatelineGreaterThan(Integer value) {
            addCriterion("pay_dateline >", value, "payDateline");
            return (Criteria) this;
        }

        public Criteria andPayDatelineGreaterThanOrEqualTo(Integer value) {
            addCriterion("pay_dateline >=", value, "payDateline");
            return (Criteria) this;
        }

        public Criteria andPayDatelineLessThan(Integer value) {
            addCriterion("pay_dateline <", value, "payDateline");
            return (Criteria) this;
        }

        public Criteria andPayDatelineLessThanOrEqualTo(Integer value) {
            addCriterion("pay_dateline <=", value, "payDateline");
            return (Criteria) this;
        }

        public Criteria andPayDatelineIn(List<Integer> values) {
            addCriterion("pay_dateline in", values, "payDateline");
            return (Criteria) this;
        }

        public Criteria andPayDatelineNotIn(List<Integer> values) {
            addCriterion("pay_dateline not in", values, "payDateline");
            return (Criteria) this;
        }

        public Criteria andPayDatelineBetween(Integer value1, Integer value2) {
            addCriterion("pay_dateline between", value1, value2, "payDateline");
            return (Criteria) this;
        }

        public Criteria andPayDatelineNotBetween(Integer value1, Integer value2) {
            addCriterion("pay_dateline not between", value1, value2, "payDateline");
            return (Criteria) this;
        }

        public Criteria andThirdIdIsNull() {
            addCriterion("third_id is null");
            return (Criteria) this;
        }

        public Criteria andThirdIdIsNotNull() {
            addCriterion("third_id is not null");
            return (Criteria) this;
        }

        public Criteria andThirdIdEqualTo(String value) {
            addCriterion("third_id =", value, "thirdId");
            return (Criteria) this;
        }

        public Criteria andThirdIdNotEqualTo(String value) {
            addCriterion("third_id <>", value, "thirdId");
            return (Criteria) this;
        }

        public Criteria andThirdIdGreaterThan(String value) {
            addCriterion("third_id >", value, "thirdId");
            return (Criteria) this;
        }

        public Criteria andThirdIdGreaterThanOrEqualTo(String value) {
            addCriterion("third_id >=", value, "thirdId");
            return (Criteria) this;
        }

        public Criteria andThirdIdLessThan(String value) {
            addCriterion("third_id <", value, "thirdId");
            return (Criteria) this;
        }

        public Criteria andThirdIdLessThanOrEqualTo(String value) {
            addCriterion("third_id <=", value, "thirdId");
            return (Criteria) this;
        }

        public Criteria andThirdIdLike(String value) {
            addCriterion("third_id like", value, "thirdId");
            return (Criteria) this;
        }

        public Criteria andThirdIdNotLike(String value) {
            addCriterion("third_id not like", value, "thirdId");
            return (Criteria) this;
        }

        public Criteria andThirdIdIn(List<String> values) {
            addCriterion("third_id in", values, "thirdId");
            return (Criteria) this;
        }

        public Criteria andThirdIdNotIn(List<String> values) {
            addCriterion("third_id not in", values, "thirdId");
            return (Criteria) this;
        }

        public Criteria andThirdIdBetween(String value1, String value2) {
            addCriterion("third_id between", value1, value2, "thirdId");
            return (Criteria) this;
        }

        public Criteria andThirdIdNotBetween(String value1, String value2) {
            addCriterion("third_id not between", value1, value2, "thirdId");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("`status` is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("`status` is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("`status` =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("`status` <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("`status` >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("`status` >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("`status` <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("`status` <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("`status` in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("`status` not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("`status` between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("`status` not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andUntreadMoneyIsNull() {
            addCriterion("untread_money is null");
            return (Criteria) this;
        }

        public Criteria andUntreadMoneyIsNotNull() {
            addCriterion("untread_money is not null");
            return (Criteria) this;
        }

        public Criteria andUntreadMoneyEqualTo(Float value) {
            addCriterion("untread_money =", value, "untreadMoney");
            return (Criteria) this;
        }

        public Criteria andUntreadMoneyNotEqualTo(Float value) {
            addCriterion("untread_money <>", value, "untreadMoney");
            return (Criteria) this;
        }

        public Criteria andUntreadMoneyGreaterThan(Float value) {
            addCriterion("untread_money >", value, "untreadMoney");
            return (Criteria) this;
        }

        public Criteria andUntreadMoneyGreaterThanOrEqualTo(Float value) {
            addCriterion("untread_money >=", value, "untreadMoney");
            return (Criteria) this;
        }

        public Criteria andUntreadMoneyLessThan(Float value) {
            addCriterion("untread_money <", value, "untreadMoney");
            return (Criteria) this;
        }

        public Criteria andUntreadMoneyLessThanOrEqualTo(Float value) {
            addCriterion("untread_money <=", value, "untreadMoney");
            return (Criteria) this;
        }

        public Criteria andUntreadMoneyIn(List<Float> values) {
            addCriterion("untread_money in", values, "untreadMoney");
            return (Criteria) this;
        }

        public Criteria andUntreadMoneyNotIn(List<Float> values) {
            addCriterion("untread_money not in", values, "untreadMoney");
            return (Criteria) this;
        }

        public Criteria andUntreadMoneyBetween(Float value1, Float value2) {
            addCriterion("untread_money between", value1, value2, "untreadMoney");
            return (Criteria) this;
        }

        public Criteria andUntreadMoneyNotBetween(Float value1, Float value2) {
            addCriterion("untread_money not between", value1, value2, "untreadMoney");
            return (Criteria) this;
        }

        public Criteria andUntreadDatelineIsNull() {
            addCriterion("untread_dateline is null");
            return (Criteria) this;
        }

        public Criteria andUntreadDatelineIsNotNull() {
            addCriterion("untread_dateline is not null");
            return (Criteria) this;
        }

        public Criteria andUntreadDatelineEqualTo(Integer value) {
            addCriterion("untread_dateline =", value, "untreadDateline");
            return (Criteria) this;
        }

        public Criteria andUntreadDatelineNotEqualTo(Integer value) {
            addCriterion("untread_dateline <>", value, "untreadDateline");
            return (Criteria) this;
        }

        public Criteria andUntreadDatelineGreaterThan(Integer value) {
            addCriterion("untread_dateline >", value, "untreadDateline");
            return (Criteria) this;
        }

        public Criteria andUntreadDatelineGreaterThanOrEqualTo(Integer value) {
            addCriterion("untread_dateline >=", value, "untreadDateline");
            return (Criteria) this;
        }

        public Criteria andUntreadDatelineLessThan(Integer value) {
            addCriterion("untread_dateline <", value, "untreadDateline");
            return (Criteria) this;
        }

        public Criteria andUntreadDatelineLessThanOrEqualTo(Integer value) {
            addCriterion("untread_dateline <=", value, "untreadDateline");
            return (Criteria) this;
        }

        public Criteria andUntreadDatelineIn(List<Integer> values) {
            addCriterion("untread_dateline in", values, "untreadDateline");
            return (Criteria) this;
        }

        public Criteria andUntreadDatelineNotIn(List<Integer> values) {
            addCriterion("untread_dateline not in", values, "untreadDateline");
            return (Criteria) this;
        }

        public Criteria andUntreadDatelineBetween(Integer value1, Integer value2) {
            addCriterion("untread_dateline between", value1, value2, "untreadDateline");
            return (Criteria) this;
        }

        public Criteria andUntreadDatelineNotBetween(Integer value1, Integer value2) {
            addCriterion("untread_dateline not between", value1, value2, "untreadDateline");
            return (Criteria) this;
        }

        public Criteria andUntreadContentIsNull() {
            addCriterion("untread_content is null");
            return (Criteria) this;
        }

        public Criteria andUntreadContentIsNotNull() {
            addCriterion("untread_content is not null");
            return (Criteria) this;
        }

        public Criteria andUntreadContentEqualTo(String value) {
            addCriterion("untread_content =", value, "untreadContent");
            return (Criteria) this;
        }

        public Criteria andUntreadContentNotEqualTo(String value) {
            addCriterion("untread_content <>", value, "untreadContent");
            return (Criteria) this;
        }

        public Criteria andUntreadContentGreaterThan(String value) {
            addCriterion("untread_content >", value, "untreadContent");
            return (Criteria) this;
        }

        public Criteria andUntreadContentGreaterThanOrEqualTo(String value) {
            addCriterion("untread_content >=", value, "untreadContent");
            return (Criteria) this;
        }

        public Criteria andUntreadContentLessThan(String value) {
            addCriterion("untread_content <", value, "untreadContent");
            return (Criteria) this;
        }

        public Criteria andUntreadContentLessThanOrEqualTo(String value) {
            addCriterion("untread_content <=", value, "untreadContent");
            return (Criteria) this;
        }

        public Criteria andUntreadContentLike(String value) {
            addCriterion("untread_content like", value, "untreadContent");
            return (Criteria) this;
        }

        public Criteria andUntreadContentNotLike(String value) {
            addCriterion("untread_content not like", value, "untreadContent");
            return (Criteria) this;
        }

        public Criteria andUntreadContentIn(List<String> values) {
            addCriterion("untread_content in", values, "untreadContent");
            return (Criteria) this;
        }

        public Criteria andUntreadContentNotIn(List<String> values) {
            addCriterion("untread_content not in", values, "untreadContent");
            return (Criteria) this;
        }

        public Criteria andUntreadContentBetween(String value1, String value2) {
            addCriterion("untread_content between", value1, value2, "untreadContent");
            return (Criteria) this;
        }

        public Criteria andUntreadContentNotBetween(String value1, String value2) {
            addCriterion("untread_content not between", value1, value2, "untreadContent");
            return (Criteria) this;
        }

        public Criteria andUntreadStatusIsNull() {
            addCriterion("untread_status is null");
            return (Criteria) this;
        }

        public Criteria andUntreadStatusIsNotNull() {
            addCriterion("untread_status is not null");
            return (Criteria) this;
        }

        public Criteria andUntreadStatusEqualTo(Integer value) {
            addCriterion("untread_status =", value, "untreadStatus");
            return (Criteria) this;
        }

        public Criteria andUntreadStatusNotEqualTo(Integer value) {
            addCriterion("untread_status <>", value, "untreadStatus");
            return (Criteria) this;
        }

        public Criteria andUntreadStatusGreaterThan(Integer value) {
            addCriterion("untread_status >", value, "untreadStatus");
            return (Criteria) this;
        }

        public Criteria andUntreadStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("untread_status >=", value, "untreadStatus");
            return (Criteria) this;
        }

        public Criteria andUntreadStatusLessThan(Integer value) {
            addCriterion("untread_status <", value, "untreadStatus");
            return (Criteria) this;
        }

        public Criteria andUntreadStatusLessThanOrEqualTo(Integer value) {
            addCriterion("untread_status <=", value, "untreadStatus");
            return (Criteria) this;
        }

        public Criteria andUntreadStatusIn(List<Integer> values) {
            addCriterion("untread_status in", values, "untreadStatus");
            return (Criteria) this;
        }

        public Criteria andUntreadStatusNotIn(List<Integer> values) {
            addCriterion("untread_status not in", values, "untreadStatus");
            return (Criteria) this;
        }

        public Criteria andUntreadStatusBetween(Integer value1, Integer value2) {
            addCriterion("untread_status between", value1, value2, "untreadStatus");
            return (Criteria) this;
        }

        public Criteria andUntreadStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("untread_status not between", value1, value2, "untreadStatus");
            return (Criteria) this;
        }

        public Criteria andPeerpayNoLikeInsensitive(String value) {
            addCriterion("upper(peerpay_no) like", value.toUpperCase(), "peerpayNo");
            return (Criteria) this;
        }

        public Criteria andNameLikeInsensitive(String value) {
            addCriterion("upper(`name`) like", value.toUpperCase(), "name");
            return (Criteria) this;
        }

        public Criteria andContentLikeInsensitive(String value) {
            addCriterion("upper(content) like", value.toUpperCase(), "content");
            return (Criteria) this;
        }

        public Criteria andThirdIdLikeInsensitive(String value) {
            addCriterion("upper(third_id) like", value.toUpperCase(), "thirdId");
            return (Criteria) this;
        }

        public Criteria andUntreadContentLikeInsensitive(String value) {
            addCriterion("upper(untread_content) like", value.toUpperCase(), "untreadContent");
            return (Criteria) this;
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table bestfenxiao_order_peerpay
     *
     * @mbggenerated do_not_delete_during_merge Mon Apr 23 22:23:43 CST 2018
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table bestfenxiao_order_peerpay
     *
     * @mbggenerated Mon Apr 23 22:23:43 CST 2018
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}