package com.micro.shop.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class Product implements Serializable {
    /**
     * 商品id
     * 表 : bestfenxiao_product
     * 对应字段 : product_id
     */
    private Integer productId;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : uid
     */
    private Integer uid;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : store_id
     */
    private Integer storeId;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : category_fid
     */
    private Integer categoryFid;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : category_id
     */
    private Integer categoryId;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : group_id
     */
    private Integer groupId;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : name
     */
    private String name;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : sale_way
     */
    private String saleWay;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : buy_way
     */
    private String buyWay;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : type
     */
    private String type;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : quantity
     */
    private Integer quantity;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : price
     */
    private BigDecimal price;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : original_price
     */
    private BigDecimal originalPrice;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : weight
     */
    private Float weight;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : code
     */
    private String code;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : image
     */
    private String image;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : image_size
     */
    private String imageSize;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : postage_type
     */
    private String postageType;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : postage
     */
    private BigDecimal postage;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : postage_template_id
     */
    private Integer postageTemplateId;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : buyer_quota
     */
    private Integer buyerQuota;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : allow_discount
     */
    private String allowDiscount;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : invoice
     */
    private String invoice;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : warranty
     */
    private String warranty;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : sold_time
     */
    private Integer soldTime;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : sales
     */
    private Integer sales;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : show_sku
     */
    private String showSku;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : status
     */
    private String status;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : date_added
     */
    private String dateAdded;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : soldout
     */
    private String soldout;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : pv
     */
    private Integer pv;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : uv
     */
    private Integer uv;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : buy_url
     */
    private String buyUrl;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : intro
     */
    private String intro;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : has_custom
     */
    private Integer hasCustom;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : has_category
     */
    private Integer hasCategory;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : has_property
     */
    private Integer hasProperty;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : is_fx
     */
    private Integer isFx;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : fx_type
     */
    private Integer fxType;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : cost_price
     */
    private BigDecimal costPrice;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : min_fx_price
     */
    private BigDecimal minFxPrice;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : max_fx_price
     */
    private BigDecimal maxFxPrice;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : is_recommend
     */
    private Integer isRecommend;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : source_product_id
     */
    private Integer sourceProductId;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : supplier_id
     */
    private Integer supplierId;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : delivery_address_id
     */
    private Integer deliveryAddressId;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : last_edit_time
     */
    private Integer lastEditTime;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : original_product_id
     */
    private Integer originalProductId;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : sort
     */
    private Integer sort;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : is_fx_setting
     */
    private Integer isFxSetting;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : collect
     */
    private Integer collect;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : attention_num
     */
    private Integer attentionNum;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : drp_profit
     */
    private BigDecimal drpProfit;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : drp_seller_qty
     */
    private Integer drpSellerQty;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : drp_sale_qty
     */
    private Integer drpSaleQty;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : unified_price_setting
     */
    private Integer unifiedPriceSetting;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : drp_level_1_price
     */
    private BigDecimal drpLevel1Price;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : drp_level_2_price
     */
    private BigDecimal drpLevel2Price;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : drp_level_3_price
     */
    private BigDecimal drpLevel3Price;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : drp_level_1_cost_price
     */
    private BigDecimal drpLevel1CostPrice;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : drp_level_2_cost_price
     */
    private BigDecimal drpLevel2CostPrice;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : drp_level_3_cost_price
     */
    private BigDecimal drpLevel3CostPrice;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : is_hot
     */
    private Integer isHot;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : is_wholesale
     */
    private Integer isWholesale;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : wholesale_price
     */
    private BigDecimal wholesalePrice;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : sale_min_price
     */
    private BigDecimal saleMinPrice;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : sale_max_price
     */
    private BigDecimal saleMaxPrice;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : wholesale_product_id
     */
    private Integer wholesaleProductId;

    /**
     * 
     * 表 : bestfenxiao_product
     * 对应字段 : public_display
     */
    private Integer publicDisplay;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table bestfenxiao_product
     *
     * @mbggenerated Fri Apr 20 02:14:07 CST 2018
     */
    private static final long serialVersionUID = 1L;

    /**
     * get method 
     *
     * @return bestfenxiao_product.product_id：商品id
     */
    public Integer getProductId() {
        return productId;
    }

    /**
     * set method 
     *
     * @param productId  商品id
     */
    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.uid：
     */
    public Integer getUid() {
        return uid;
    }

    /**
     * set method 
     *
     * @param uid  
     */
    public void setUid(Integer uid) {
        this.uid = uid;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.store_id：
     */
    public Integer getStoreId() {
        return storeId;
    }

    /**
     * set method 
     *
     * @param storeId  
     */
    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.category_fid：
     */
    public Integer getCategoryFid() {
        return categoryFid;
    }

    /**
     * set method 
     *
     * @param categoryFid  
     */
    public void setCategoryFid(Integer categoryFid) {
        this.categoryFid = categoryFid;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.category_id：
     */
    public Integer getCategoryId() {
        return categoryId;
    }

    /**
     * set method 
     *
     * @param categoryId  
     */
    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.group_id：
     */
    public Integer getGroupId() {
        return groupId;
    }

    /**
     * set method 
     *
     * @param groupId  
     */
    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.name：
     */
    public String getName() {
        return name;
    }

    /**
     * set method 
     *
     * @param name  
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.sale_way：
     */
    public String getSaleWay() {
        return saleWay;
    }

    /**
     * set method 
     *
     * @param saleWay  
     */
    public void setSaleWay(String saleWay) {
        this.saleWay = saleWay == null ? null : saleWay.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.buy_way：
     */
    public String getBuyWay() {
        return buyWay;
    }

    /**
     * set method 
     *
     * @param buyWay  
     */
    public void setBuyWay(String buyWay) {
        this.buyWay = buyWay == null ? null : buyWay.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.type：
     */
    public String getType() {
        return type;
    }

    /**
     * set method 
     *
     * @param type  
     */
    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.quantity：
     */
    public Integer getQuantity() {
        return quantity;
    }

    /**
     * set method 
     *
     * @param quantity  
     */
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.price：
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * set method 
     *
     * @param price  
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.original_price：
     */
    public BigDecimal getOriginalPrice() {
        return originalPrice;
    }

    /**
     * set method 
     *
     * @param originalPrice  
     */
    public void setOriginalPrice(BigDecimal originalPrice) {
        this.originalPrice = originalPrice;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.weight：
     */
    public Float getWeight() {
        return weight;
    }

    /**
     * set method 
     *
     * @param weight  
     */
    public void setWeight(Float weight) {
        this.weight = weight;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.code：
     */
    public String getCode() {
        return code;
    }

    /**
     * set method 
     *
     * @param code  
     */
    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.image：
     */
    public String getImage() {
        return image;
    }

    /**
     * set method 
     *
     * @param image  
     */
    public void setImage(String image) {
        this.image = image == null ? null : image.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.image_size：
     */
    public String getImageSize() {
        return imageSize;
    }

    /**
     * set method 
     *
     * @param imageSize  
     */
    public void setImageSize(String imageSize) {
        this.imageSize = imageSize == null ? null : imageSize.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.postage_type：
     */
    public String getPostageType() {
        return postageType;
    }

    /**
     * set method 
     *
     * @param postageType  
     */
    public void setPostageType(String postageType) {
        this.postageType = postageType == null ? null : postageType.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.postage：
     */
    public BigDecimal getPostage() {
        return postage;
    }

    /**
     * set method 
     *
     * @param postage  
     */
    public void setPostage(BigDecimal postage) {
        this.postage = postage;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.postage_template_id：
     */
    public Integer getPostageTemplateId() {
        return postageTemplateId;
    }

    /**
     * set method 
     *
     * @param postageTemplateId  
     */
    public void setPostageTemplateId(Integer postageTemplateId) {
        this.postageTemplateId = postageTemplateId;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.buyer_quota：
     */
    public Integer getBuyerQuota() {
        return buyerQuota;
    }

    /**
     * set method 
     *
     * @param buyerQuota  
     */
    public void setBuyerQuota(Integer buyerQuota) {
        this.buyerQuota = buyerQuota;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.allow_discount：
     */
    public String getAllowDiscount() {
        return allowDiscount;
    }

    /**
     * set method 
     *
     * @param allowDiscount  
     */
    public void setAllowDiscount(String allowDiscount) {
        this.allowDiscount = allowDiscount == null ? null : allowDiscount.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.invoice：
     */
    public String getInvoice() {
        return invoice;
    }

    /**
     * set method 
     *
     * @param invoice  
     */
    public void setInvoice(String invoice) {
        this.invoice = invoice == null ? null : invoice.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.warranty：
     */
    public String getWarranty() {
        return warranty;
    }

    /**
     * set method 
     *
     * @param warranty  
     */
    public void setWarranty(String warranty) {
        this.warranty = warranty == null ? null : warranty.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.sold_time：
     */
    public Integer getSoldTime() {
        return soldTime;
    }

    /**
     * set method 
     *
     * @param soldTime  
     */
    public void setSoldTime(Integer soldTime) {
        this.soldTime = soldTime;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.sales：
     */
    public Integer getSales() {
        return sales;
    }

    /**
     * set method 
     *
     * @param sales  
     */
    public void setSales(Integer sales) {
        this.sales = sales;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.show_sku：
     */
    public String getShowSku() {
        return showSku;
    }

    /**
     * set method 
     *
     * @param showSku  
     */
    public void setShowSku(String showSku) {
        this.showSku = showSku == null ? null : showSku.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.status：
     */
    public String getStatus() {
        return status;
    }

    /**
     * set method 
     *
     * @param status  
     */
    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.date_added：
     */
    public String getDateAdded() {
        return dateAdded;
    }

    /**
     * set method 
     *
     * @param dateAdded  
     */
    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded == null ? null : dateAdded.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.soldout：
     */
    public String getSoldout() {
        return soldout;
    }

    /**
     * set method 
     *
     * @param soldout  
     */
    public void setSoldout(String soldout) {
        this.soldout = soldout == null ? null : soldout.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.pv：
     */
    public Integer getPv() {
        return pv;
    }

    /**
     * set method 
     *
     * @param pv  
     */
    public void setPv(Integer pv) {
        this.pv = pv;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.uv：
     */
    public Integer getUv() {
        return uv;
    }

    /**
     * set method 
     *
     * @param uv  
     */
    public void setUv(Integer uv) {
        this.uv = uv;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.buy_url：
     */
    public String getBuyUrl() {
        return buyUrl;
    }

    /**
     * set method 
     *
     * @param buyUrl  
     */
    public void setBuyUrl(String buyUrl) {
        this.buyUrl = buyUrl == null ? null : buyUrl.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.intro：
     */
    public String getIntro() {
        return intro;
    }

    /**
     * set method 
     *
     * @param intro  
     */
    public void setIntro(String intro) {
        this.intro = intro == null ? null : intro.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.has_custom：
     */
    public Integer getHasCustom() {
        return hasCustom;
    }

    /**
     * set method 
     *
     * @param hasCustom  
     */
    public void setHasCustom(Integer hasCustom) {
        this.hasCustom = hasCustom;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.has_category：
     */
    public Integer getHasCategory() {
        return hasCategory;
    }

    /**
     * set method 
     *
     * @param hasCategory  
     */
    public void setHasCategory(Integer hasCategory) {
        this.hasCategory = hasCategory;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.has_property：
     */
    public Integer getHasProperty() {
        return hasProperty;
    }

    /**
     * set method 
     *
     * @param hasProperty  
     */
    public void setHasProperty(Integer hasProperty) {
        this.hasProperty = hasProperty;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.is_fx：
     */
    public Integer getIsFx() {
        return isFx;
    }

    /**
     * set method 
     *
     * @param isFx  
     */
    public void setIsFx(Integer isFx) {
        this.isFx = isFx;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.fx_type：
     */
    public Integer getFxType() {
        return fxType;
    }

    /**
     * set method 
     *
     * @param fxType  
     */
    public void setFxType(Integer fxType) {
        this.fxType = fxType;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.cost_price：
     */
    public BigDecimal getCostPrice() {
        return costPrice;
    }

    /**
     * set method 
     *
     * @param costPrice  
     */
    public void setCostPrice(BigDecimal costPrice) {
        this.costPrice = costPrice;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.min_fx_price：
     */
    public BigDecimal getMinFxPrice() {
        return minFxPrice;
    }

    /**
     * set method 
     *
     * @param minFxPrice  
     */
    public void setMinFxPrice(BigDecimal minFxPrice) {
        this.minFxPrice = minFxPrice;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.max_fx_price：
     */
    public BigDecimal getMaxFxPrice() {
        return maxFxPrice;
    }

    /**
     * set method 
     *
     * @param maxFxPrice  
     */
    public void setMaxFxPrice(BigDecimal maxFxPrice) {
        this.maxFxPrice = maxFxPrice;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.is_recommend：
     */
    public Integer getIsRecommend() {
        return isRecommend;
    }

    /**
     * set method 
     *
     * @param isRecommend  
     */
    public void setIsRecommend(Integer isRecommend) {
        this.isRecommend = isRecommend;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.source_product_id：
     */
    public Integer getSourceProductId() {
        return sourceProductId;
    }

    /**
     * set method 
     *
     * @param sourceProductId  
     */
    public void setSourceProductId(Integer sourceProductId) {
        this.sourceProductId = sourceProductId;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.supplier_id：
     */
    public Integer getSupplierId() {
        return supplierId;
    }

    /**
     * set method 
     *
     * @param supplierId  
     */
    public void setSupplierId(Integer supplierId) {
        this.supplierId = supplierId;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.delivery_address_id：
     */
    public Integer getDeliveryAddressId() {
        return deliveryAddressId;
    }

    /**
     * set method 
     *
     * @param deliveryAddressId  
     */
    public void setDeliveryAddressId(Integer deliveryAddressId) {
        this.deliveryAddressId = deliveryAddressId;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.last_edit_time：
     */
    public Integer getLastEditTime() {
        return lastEditTime;
    }

    /**
     * set method 
     *
     * @param lastEditTime  
     */
    public void setLastEditTime(Integer lastEditTime) {
        this.lastEditTime = lastEditTime;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.original_product_id：
     */
    public Integer getOriginalProductId() {
        return originalProductId;
    }

    /**
     * set method 
     *
     * @param originalProductId  
     */
    public void setOriginalProductId(Integer originalProductId) {
        this.originalProductId = originalProductId;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.sort：
     */
    public Integer getSort() {
        return sort;
    }

    /**
     * set method 
     *
     * @param sort  
     */
    public void setSort(Integer sort) {
        this.sort = sort;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.is_fx_setting：
     */
    public Integer getIsFxSetting() {
        return isFxSetting;
    }

    /**
     * set method 
     *
     * @param isFxSetting  
     */
    public void setIsFxSetting(Integer isFxSetting) {
        this.isFxSetting = isFxSetting;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.collect：
     */
    public Integer getCollect() {
        return collect;
    }

    /**
     * set method 
     *
     * @param collect  
     */
    public void setCollect(Integer collect) {
        this.collect = collect;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.attention_num：
     */
    public Integer getAttentionNum() {
        return attentionNum;
    }

    /**
     * set method 
     *
     * @param attentionNum  
     */
    public void setAttentionNum(Integer attentionNum) {
        this.attentionNum = attentionNum;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.drp_profit：
     */
    public BigDecimal getDrpProfit() {
        return drpProfit;
    }

    /**
     * set method 
     *
     * @param drpProfit  
     */
    public void setDrpProfit(BigDecimal drpProfit) {
        this.drpProfit = drpProfit;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.drp_seller_qty：
     */
    public Integer getDrpSellerQty() {
        return drpSellerQty;
    }

    /**
     * set method 
     *
     * @param drpSellerQty  
     */
    public void setDrpSellerQty(Integer drpSellerQty) {
        this.drpSellerQty = drpSellerQty;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.drp_sale_qty：
     */
    public Integer getDrpSaleQty() {
        return drpSaleQty;
    }

    /**
     * set method 
     *
     * @param drpSaleQty  
     */
    public void setDrpSaleQty(Integer drpSaleQty) {
        this.drpSaleQty = drpSaleQty;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.unified_price_setting：
     */
    public Integer getUnifiedPriceSetting() {
        return unifiedPriceSetting;
    }

    /**
     * set method 
     *
     * @param unifiedPriceSetting  
     */
    public void setUnifiedPriceSetting(Integer unifiedPriceSetting) {
        this.unifiedPriceSetting = unifiedPriceSetting;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.drp_level_1_price：
     */
    public BigDecimal getDrpLevel1Price() {
        return drpLevel1Price;
    }

    /**
     * set method 
     *
     * @param drpLevel1Price  
     */
    public void setDrpLevel1Price(BigDecimal drpLevel1Price) {
        this.drpLevel1Price = drpLevel1Price;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.drp_level_2_price：
     */
    public BigDecimal getDrpLevel2Price() {
        return drpLevel2Price;
    }

    /**
     * set method 
     *
     * @param drpLevel2Price  
     */
    public void setDrpLevel2Price(BigDecimal drpLevel2Price) {
        this.drpLevel2Price = drpLevel2Price;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.drp_level_3_price：
     */
    public BigDecimal getDrpLevel3Price() {
        return drpLevel3Price;
    }

    /**
     * set method 
     *
     * @param drpLevel3Price  
     */
    public void setDrpLevel3Price(BigDecimal drpLevel3Price) {
        this.drpLevel3Price = drpLevel3Price;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.drp_level_1_cost_price：
     */
    public BigDecimal getDrpLevel1CostPrice() {
        return drpLevel1CostPrice;
    }

    /**
     * set method 
     *
     * @param drpLevel1CostPrice  
     */
    public void setDrpLevel1CostPrice(BigDecimal drpLevel1CostPrice) {
        this.drpLevel1CostPrice = drpLevel1CostPrice;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.drp_level_2_cost_price：
     */
    public BigDecimal getDrpLevel2CostPrice() {
        return drpLevel2CostPrice;
    }

    /**
     * set method 
     *
     * @param drpLevel2CostPrice  
     */
    public void setDrpLevel2CostPrice(BigDecimal drpLevel2CostPrice) {
        this.drpLevel2CostPrice = drpLevel2CostPrice;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.drp_level_3_cost_price：
     */
    public BigDecimal getDrpLevel3CostPrice() {
        return drpLevel3CostPrice;
    }

    /**
     * set method 
     *
     * @param drpLevel3CostPrice  
     */
    public void setDrpLevel3CostPrice(BigDecimal drpLevel3CostPrice) {
        this.drpLevel3CostPrice = drpLevel3CostPrice;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.is_hot：
     */
    public Integer getIsHot() {
        return isHot;
    }

    /**
     * set method 
     *
     * @param isHot  
     */
    public void setIsHot(Integer isHot) {
        this.isHot = isHot;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.is_wholesale：
     */
    public Integer getIsWholesale() {
        return isWholesale;
    }

    /**
     * set method 
     *
     * @param isWholesale  
     */
    public void setIsWholesale(Integer isWholesale) {
        this.isWholesale = isWholesale;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.wholesale_price：
     */
    public BigDecimal getWholesalePrice() {
        return wholesalePrice;
    }

    /**
     * set method 
     *
     * @param wholesalePrice  
     */
    public void setWholesalePrice(BigDecimal wholesalePrice) {
        this.wholesalePrice = wholesalePrice;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.sale_min_price：
     */
    public BigDecimal getSaleMinPrice() {
        return saleMinPrice;
    }

    /**
     * set method 
     *
     * @param saleMinPrice  
     */
    public void setSaleMinPrice(BigDecimal saleMinPrice) {
        this.saleMinPrice = saleMinPrice;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.sale_max_price：
     */
    public BigDecimal getSaleMaxPrice() {
        return saleMaxPrice;
    }

    /**
     * set method 
     *
     * @param saleMaxPrice  
     */
    public void setSaleMaxPrice(BigDecimal saleMaxPrice) {
        this.saleMaxPrice = saleMaxPrice;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.wholesale_product_id：
     */
    public Integer getWholesaleProductId() {
        return wholesaleProductId;
    }

    /**
     * set method 
     *
     * @param wholesaleProductId  
     */
    public void setWholesaleProductId(Integer wholesaleProductId) {
        this.wholesaleProductId = wholesaleProductId;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_product.public_display：
     */
    public Integer getPublicDisplay() {
        return publicDisplay;
    }

    /**
     * set method 
     *
     * @param publicDisplay  
     */
    public void setPublicDisplay(Integer publicDisplay) {
        this.publicDisplay = publicDisplay;
    }

    /**
     *
     * @param that
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Product other = (Product) that;
        return (this.getProductId() == null ? other.getProductId() == null : this.getProductId().equals(other.getProductId()))
            && (this.getUid() == null ? other.getUid() == null : this.getUid().equals(other.getUid()))
            && (this.getStoreId() == null ? other.getStoreId() == null : this.getStoreId().equals(other.getStoreId()))
            && (this.getCategoryFid() == null ? other.getCategoryFid() == null : this.getCategoryFid().equals(other.getCategoryFid()))
            && (this.getCategoryId() == null ? other.getCategoryId() == null : this.getCategoryId().equals(other.getCategoryId()))
            && (this.getGroupId() == null ? other.getGroupId() == null : this.getGroupId().equals(other.getGroupId()))
            && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
            && (this.getSaleWay() == null ? other.getSaleWay() == null : this.getSaleWay().equals(other.getSaleWay()))
            && (this.getBuyWay() == null ? other.getBuyWay() == null : this.getBuyWay().equals(other.getBuyWay()))
            && (this.getType() == null ? other.getType() == null : this.getType().equals(other.getType()))
            && (this.getQuantity() == null ? other.getQuantity() == null : this.getQuantity().equals(other.getQuantity()))
            && (this.getPrice() == null ? other.getPrice() == null : this.getPrice().equals(other.getPrice()))
            && (this.getOriginalPrice() == null ? other.getOriginalPrice() == null : this.getOriginalPrice().equals(other.getOriginalPrice()))
            && (this.getWeight() == null ? other.getWeight() == null : this.getWeight().equals(other.getWeight()))
            && (this.getCode() == null ? other.getCode() == null : this.getCode().equals(other.getCode()))
            && (this.getImage() == null ? other.getImage() == null : this.getImage().equals(other.getImage()))
            && (this.getImageSize() == null ? other.getImageSize() == null : this.getImageSize().equals(other.getImageSize()))
            && (this.getPostageType() == null ? other.getPostageType() == null : this.getPostageType().equals(other.getPostageType()))
            && (this.getPostage() == null ? other.getPostage() == null : this.getPostage().equals(other.getPostage()))
            && (this.getPostageTemplateId() == null ? other.getPostageTemplateId() == null : this.getPostageTemplateId().equals(other.getPostageTemplateId()))
            && (this.getBuyerQuota() == null ? other.getBuyerQuota() == null : this.getBuyerQuota().equals(other.getBuyerQuota()))
            && (this.getAllowDiscount() == null ? other.getAllowDiscount() == null : this.getAllowDiscount().equals(other.getAllowDiscount()))
            && (this.getInvoice() == null ? other.getInvoice() == null : this.getInvoice().equals(other.getInvoice()))
            && (this.getWarranty() == null ? other.getWarranty() == null : this.getWarranty().equals(other.getWarranty()))
            && (this.getSoldTime() == null ? other.getSoldTime() == null : this.getSoldTime().equals(other.getSoldTime()))
            && (this.getSales() == null ? other.getSales() == null : this.getSales().equals(other.getSales()))
            && (this.getShowSku() == null ? other.getShowSku() == null : this.getShowSku().equals(other.getShowSku()))
            && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()))
            && (this.getDateAdded() == null ? other.getDateAdded() == null : this.getDateAdded().equals(other.getDateAdded()))
            && (this.getSoldout() == null ? other.getSoldout() == null : this.getSoldout().equals(other.getSoldout()))
            && (this.getPv() == null ? other.getPv() == null : this.getPv().equals(other.getPv()))
            && (this.getUv() == null ? other.getUv() == null : this.getUv().equals(other.getUv()))
            && (this.getBuyUrl() == null ? other.getBuyUrl() == null : this.getBuyUrl().equals(other.getBuyUrl()))
            && (this.getIntro() == null ? other.getIntro() == null : this.getIntro().equals(other.getIntro()))
            && (this.getHasCustom() == null ? other.getHasCustom() == null : this.getHasCustom().equals(other.getHasCustom()))
            && (this.getHasCategory() == null ? other.getHasCategory() == null : this.getHasCategory().equals(other.getHasCategory()))
            && (this.getHasProperty() == null ? other.getHasProperty() == null : this.getHasProperty().equals(other.getHasProperty()))
            && (this.getIsFx() == null ? other.getIsFx() == null : this.getIsFx().equals(other.getIsFx()))
            && (this.getFxType() == null ? other.getFxType() == null : this.getFxType().equals(other.getFxType()))
            && (this.getCostPrice() == null ? other.getCostPrice() == null : this.getCostPrice().equals(other.getCostPrice()))
            && (this.getMinFxPrice() == null ? other.getMinFxPrice() == null : this.getMinFxPrice().equals(other.getMinFxPrice()))
            && (this.getMaxFxPrice() == null ? other.getMaxFxPrice() == null : this.getMaxFxPrice().equals(other.getMaxFxPrice()))
            && (this.getIsRecommend() == null ? other.getIsRecommend() == null : this.getIsRecommend().equals(other.getIsRecommend()))
            && (this.getSourceProductId() == null ? other.getSourceProductId() == null : this.getSourceProductId().equals(other.getSourceProductId()))
            && (this.getSupplierId() == null ? other.getSupplierId() == null : this.getSupplierId().equals(other.getSupplierId()))
            && (this.getDeliveryAddressId() == null ? other.getDeliveryAddressId() == null : this.getDeliveryAddressId().equals(other.getDeliveryAddressId()))
            && (this.getLastEditTime() == null ? other.getLastEditTime() == null : this.getLastEditTime().equals(other.getLastEditTime()))
            && (this.getOriginalProductId() == null ? other.getOriginalProductId() == null : this.getOriginalProductId().equals(other.getOriginalProductId()))
            && (this.getSort() == null ? other.getSort() == null : this.getSort().equals(other.getSort()))
            && (this.getIsFxSetting() == null ? other.getIsFxSetting() == null : this.getIsFxSetting().equals(other.getIsFxSetting()))
            && (this.getCollect() == null ? other.getCollect() == null : this.getCollect().equals(other.getCollect()))
            && (this.getAttentionNum() == null ? other.getAttentionNum() == null : this.getAttentionNum().equals(other.getAttentionNum()))
            && (this.getDrpProfit() == null ? other.getDrpProfit() == null : this.getDrpProfit().equals(other.getDrpProfit()))
            && (this.getDrpSellerQty() == null ? other.getDrpSellerQty() == null : this.getDrpSellerQty().equals(other.getDrpSellerQty()))
            && (this.getDrpSaleQty() == null ? other.getDrpSaleQty() == null : this.getDrpSaleQty().equals(other.getDrpSaleQty()))
            && (this.getUnifiedPriceSetting() == null ? other.getUnifiedPriceSetting() == null : this.getUnifiedPriceSetting().equals(other.getUnifiedPriceSetting()))
            && (this.getDrpLevel1Price() == null ? other.getDrpLevel1Price() == null : this.getDrpLevel1Price().equals(other.getDrpLevel1Price()))
            && (this.getDrpLevel2Price() == null ? other.getDrpLevel2Price() == null : this.getDrpLevel2Price().equals(other.getDrpLevel2Price()))
            && (this.getDrpLevel3Price() == null ? other.getDrpLevel3Price() == null : this.getDrpLevel3Price().equals(other.getDrpLevel3Price()))
            && (this.getDrpLevel1CostPrice() == null ? other.getDrpLevel1CostPrice() == null : this.getDrpLevel1CostPrice().equals(other.getDrpLevel1CostPrice()))
            && (this.getDrpLevel2CostPrice() == null ? other.getDrpLevel2CostPrice() == null : this.getDrpLevel2CostPrice().equals(other.getDrpLevel2CostPrice()))
            && (this.getDrpLevel3CostPrice() == null ? other.getDrpLevel3CostPrice() == null : this.getDrpLevel3CostPrice().equals(other.getDrpLevel3CostPrice()))
            && (this.getIsHot() == null ? other.getIsHot() == null : this.getIsHot().equals(other.getIsHot()))
            && (this.getIsWholesale() == null ? other.getIsWholesale() == null : this.getIsWholesale().equals(other.getIsWholesale()))
            && (this.getWholesalePrice() == null ? other.getWholesalePrice() == null : this.getWholesalePrice().equals(other.getWholesalePrice()))
            && (this.getSaleMinPrice() == null ? other.getSaleMinPrice() == null : this.getSaleMinPrice().equals(other.getSaleMinPrice()))
            && (this.getSaleMaxPrice() == null ? other.getSaleMaxPrice() == null : this.getSaleMaxPrice().equals(other.getSaleMaxPrice()))
            && (this.getWholesaleProductId() == null ? other.getWholesaleProductId() == null : this.getWholesaleProductId().equals(other.getWholesaleProductId()))
            && (this.getPublicDisplay() == null ? other.getPublicDisplay() == null : this.getPublicDisplay().equals(other.getPublicDisplay()));
    }

    /**
     *
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getProductId() == null) ? 0 : getProductId().hashCode());
        result = prime * result + ((getUid() == null) ? 0 : getUid().hashCode());
        result = prime * result + ((getStoreId() == null) ? 0 : getStoreId().hashCode());
        result = prime * result + ((getCategoryFid() == null) ? 0 : getCategoryFid().hashCode());
        result = prime * result + ((getCategoryId() == null) ? 0 : getCategoryId().hashCode());
        result = prime * result + ((getGroupId() == null) ? 0 : getGroupId().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getSaleWay() == null) ? 0 : getSaleWay().hashCode());
        result = prime * result + ((getBuyWay() == null) ? 0 : getBuyWay().hashCode());
        result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
        result = prime * result + ((getQuantity() == null) ? 0 : getQuantity().hashCode());
        result = prime * result + ((getPrice() == null) ? 0 : getPrice().hashCode());
        result = prime * result + ((getOriginalPrice() == null) ? 0 : getOriginalPrice().hashCode());
        result = prime * result + ((getWeight() == null) ? 0 : getWeight().hashCode());
        result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
        result = prime * result + ((getImage() == null) ? 0 : getImage().hashCode());
        result = prime * result + ((getImageSize() == null) ? 0 : getImageSize().hashCode());
        result = prime * result + ((getPostageType() == null) ? 0 : getPostageType().hashCode());
        result = prime * result + ((getPostage() == null) ? 0 : getPostage().hashCode());
        result = prime * result + ((getPostageTemplateId() == null) ? 0 : getPostageTemplateId().hashCode());
        result = prime * result + ((getBuyerQuota() == null) ? 0 : getBuyerQuota().hashCode());
        result = prime * result + ((getAllowDiscount() == null) ? 0 : getAllowDiscount().hashCode());
        result = prime * result + ((getInvoice() == null) ? 0 : getInvoice().hashCode());
        result = prime * result + ((getWarranty() == null) ? 0 : getWarranty().hashCode());
        result = prime * result + ((getSoldTime() == null) ? 0 : getSoldTime().hashCode());
        result = prime * result + ((getSales() == null) ? 0 : getSales().hashCode());
        result = prime * result + ((getShowSku() == null) ? 0 : getShowSku().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        result = prime * result + ((getDateAdded() == null) ? 0 : getDateAdded().hashCode());
        result = prime * result + ((getSoldout() == null) ? 0 : getSoldout().hashCode());
        result = prime * result + ((getPv() == null) ? 0 : getPv().hashCode());
        result = prime * result + ((getUv() == null) ? 0 : getUv().hashCode());
        result = prime * result + ((getBuyUrl() == null) ? 0 : getBuyUrl().hashCode());
        result = prime * result + ((getIntro() == null) ? 0 : getIntro().hashCode());
        result = prime * result + ((getHasCustom() == null) ? 0 : getHasCustom().hashCode());
        result = prime * result + ((getHasCategory() == null) ? 0 : getHasCategory().hashCode());
        result = prime * result + ((getHasProperty() == null) ? 0 : getHasProperty().hashCode());
        result = prime * result + ((getIsFx() == null) ? 0 : getIsFx().hashCode());
        result = prime * result + ((getFxType() == null) ? 0 : getFxType().hashCode());
        result = prime * result + ((getCostPrice() == null) ? 0 : getCostPrice().hashCode());
        result = prime * result + ((getMinFxPrice() == null) ? 0 : getMinFxPrice().hashCode());
        result = prime * result + ((getMaxFxPrice() == null) ? 0 : getMaxFxPrice().hashCode());
        result = prime * result + ((getIsRecommend() == null) ? 0 : getIsRecommend().hashCode());
        result = prime * result + ((getSourceProductId() == null) ? 0 : getSourceProductId().hashCode());
        result = prime * result + ((getSupplierId() == null) ? 0 : getSupplierId().hashCode());
        result = prime * result + ((getDeliveryAddressId() == null) ? 0 : getDeliveryAddressId().hashCode());
        result = prime * result + ((getLastEditTime() == null) ? 0 : getLastEditTime().hashCode());
        result = prime * result + ((getOriginalProductId() == null) ? 0 : getOriginalProductId().hashCode());
        result = prime * result + ((getSort() == null) ? 0 : getSort().hashCode());
        result = prime * result + ((getIsFxSetting() == null) ? 0 : getIsFxSetting().hashCode());
        result = prime * result + ((getCollect() == null) ? 0 : getCollect().hashCode());
        result = prime * result + ((getAttentionNum() == null) ? 0 : getAttentionNum().hashCode());
        result = prime * result + ((getDrpProfit() == null) ? 0 : getDrpProfit().hashCode());
        result = prime * result + ((getDrpSellerQty() == null) ? 0 : getDrpSellerQty().hashCode());
        result = prime * result + ((getDrpSaleQty() == null) ? 0 : getDrpSaleQty().hashCode());
        result = prime * result + ((getUnifiedPriceSetting() == null) ? 0 : getUnifiedPriceSetting().hashCode());
        result = prime * result + ((getDrpLevel1Price() == null) ? 0 : getDrpLevel1Price().hashCode());
        result = prime * result + ((getDrpLevel2Price() == null) ? 0 : getDrpLevel2Price().hashCode());
        result = prime * result + ((getDrpLevel3Price() == null) ? 0 : getDrpLevel3Price().hashCode());
        result = prime * result + ((getDrpLevel1CostPrice() == null) ? 0 : getDrpLevel1CostPrice().hashCode());
        result = prime * result + ((getDrpLevel2CostPrice() == null) ? 0 : getDrpLevel2CostPrice().hashCode());
        result = prime * result + ((getDrpLevel3CostPrice() == null) ? 0 : getDrpLevel3CostPrice().hashCode());
        result = prime * result + ((getIsHot() == null) ? 0 : getIsHot().hashCode());
        result = prime * result + ((getIsWholesale() == null) ? 0 : getIsWholesale().hashCode());
        result = prime * result + ((getWholesalePrice() == null) ? 0 : getWholesalePrice().hashCode());
        result = prime * result + ((getSaleMinPrice() == null) ? 0 : getSaleMinPrice().hashCode());
        result = prime * result + ((getSaleMaxPrice() == null) ? 0 : getSaleMaxPrice().hashCode());
        result = prime * result + ((getWholesaleProductId() == null) ? 0 : getWholesaleProductId().hashCode());
        result = prime * result + ((getPublicDisplay() == null) ? 0 : getPublicDisplay().hashCode());
        return result;
    }
}