package com.micro.shop.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class Store implements Serializable {
    /**
     * 店铺id
     * 表 : bestfenxiao_store
     * 对应字段 : store_id
     */
    private Integer storeId;

    /**
     * 会员id
     * 表 : bestfenxiao_store
     * 对应字段 : uid
     */
    private Integer uid;

    /**
     * 店铺名称
     * 表 : bestfenxiao_store
     * 对应字段 : name
     */
    private String name;

    /**
     * 店铺名修改次数
     * 表 : bestfenxiao_store
     * 对应字段 : edit_name_count
     */
    private Integer editNameCount;

    /**
     * 店铺logo
     * 表 : bestfenxiao_store
     * 对应字段 : logo
     */
    private String logo;

    /**
     * 店铺二维码
     * 表 : bestfenxiao_store
     * 对应字段 : qcode
     */
    private String qcode;

    /**
     * 微信唯一标识 (关联绑定公众号)
     * 表 : bestfenxiao_store
     * 对应字段 : openid
     */
    private String openid;

    /**
     * 二维码开始生效时间
     * 表 : bestfenxiao_store
     * 对应字段 : qcode_starttime
     */
    private Integer qcodeStarttime;

    /**
     * 
     * 表 : bestfenxiao_store
     * 对应字段 : sale_category_fid
     */
    private Integer saleCategoryFid;

    /**
     * 主营类目
     * 表 : bestfenxiao_store
     * 对应字段 : sale_category_id
     */
    private Integer saleCategoryId;

    /**
     * 联系人
     * 表 : bestfenxiao_store
     * 对应字段 : linkman
     */
    private String linkman;

    /**
     * 电话
     * 表 : bestfenxiao_store
     * 对应字段 : tel
     */
    private String tel;

    /**
     * 店铺简介
     * 表 : bestfenxiao_store
     * 对应字段 : intro
     */
    private String intro;

    /**
     * 认证 0未认证 1已证
     * 表 : bestfenxiao_store
     * 对应字段 : approve
     */
    private String approve;

    /**
     * 状态 0禁用 1启用
     * 表 : bestfenxiao_store
     * 对应字段 : status
     */
    private String status;

    /**
     * 开启后将会在微信综合商城 和 pc综合商城展示 0：不展示，1：展示
     * 表 : bestfenxiao_store
     * 对应字段 : public_display
     */
    private Integer publicDisplay;

    /**
     * 店铺入驻时间
     * 表 : bestfenxiao_store
     * 对应字段 : date_added
     */
    private String dateAdded;

    /**
     * 客服电话
     * 表 : bestfenxiao_store
     * 对应字段 : service_tel
     */
    private String serviceTel;

    /**
     * 客服qq
     * 表 : bestfenxiao_store
     * 对应字段 : service_qq
     */
    private String serviceQq;

    /**
     * 客服微信
     * 表 : bestfenxiao_store
     * 对应字段 : service_weixin
     */
    private String serviceWeixin;

    /**
     * 绑定微信 0未绑定 1已绑定
     * 表 : bestfenxiao_store
     * 对应字段 : bind_weixin
     */
    private Integer bindWeixin;

    /**
     * 公众号名称
     * 表 : bestfenxiao_store
     * 对应字段 : weixin_name
     */
    private String weixinName;

    /**
     * 微信原始ID
     * 表 : bestfenxiao_store
     * 对应字段 : weixin_original_id
     */
    private String weixinOriginalId;

    /**
     * 微信ID
     * 表 : bestfenxiao_store
     * 对应字段 : weixin_id
     */
    private String weixinId;

    /**
     * qq
     * 表 : bestfenxiao_store
     * 对应字段 : qq
     */
    private String qq;

    /**
     * 绑定微信
     * 表 : bestfenxiao_store
     * 对应字段 : open_weixin
     */
    private String openWeixin;

    /**
     * 买家上门自提
     * 表 : bestfenxiao_store
     * 对应字段 : buyer_selffetch
     */
    private String buyerSelffetch;

    /**
     * “上门自提”自定义名称
     * 表 : bestfenxiao_store
     * 对应字段 : buyer_selffetch_name
     */
    private String buyerSelffetchName;

    /**
     * 代付
     * 表 : bestfenxiao_store
     * 对应字段 : pay_agent
     */
    private String payAgent;

    /**
     * 是否支持货到付款，0：是，1：否
     * 表 : bestfenxiao_store
     * 对应字段 : offline_payment
     */
    private Integer offlinePayment;

    /**
     * 是否开启物流配送，1：开启，0：关闭
     * 表 : bestfenxiao_store
     * 对应字段 : open_logistics
     */
    private Integer openLogistics;

    /**
     * 是否开启送朋友，1：是，0：否
     * 表 : bestfenxiao_store
     * 对应字段 : open_friend
     */
    private Integer openFriend;

    /**
     * 是否开启订单自动分配到门店： 1是 0否
     * 表 : bestfenxiao_store
     * 对应字段 : open_autoassign
     */
    private Integer openAutoassign;

    /**
     * 是否使用本地化物流: 0否 1是
     * 表 : bestfenxiao_store
     * 对应字段 : open_local_logistics
     */
    private Integer openLocalLogistics;

    /**
     * 门店库存报警：0为不报警
     * 表 : bestfenxiao_store
     * 对应字段 : warn_sp_quantity
     */
    private Integer warnSpQuantity;

    /**
     * 开启店铺导航
     * 表 : bestfenxiao_store
     * 对应字段 : open_nav
     */
    private Integer openNav;

    /**
     * 店铺导航样式
     * 表 : bestfenxiao_store
     * 对应字段 : nav_style_id
     */
    private Integer navStyleId;

    /**
     * 使用导航菜单的页面 1店铺主页 2会员主页 3微页面及分类 4商品分组 5商品搜索
     * 表 : bestfenxiao_store
     * 对应字段 : use_nav_pages
     */
    private String useNavPages;

    /**
     * 开启广告
     * 表 : bestfenxiao_store
     * 对应字段 : open_ad
     */
    private Integer openAd;

    /**
     * 是否有店铺广告
     * 表 : bestfenxiao_store
     * 对应字段 : has_ad
     */
    private Integer hasAd;

    /**
     * 广告位置 0页面头部 1页面底部
     * 表 : bestfenxiao_store
     * 对应字段 : ad_position
     */
    private Integer adPosition;

    /**
     * 使用广告的页面 1微页面 2微页面分类 3商品 4商品分组 5店铺主页 6会员主页
     * 表 : bestfenxiao_store
     * 对应字段 : use_ad_pages
     */
    private String useAdPages;

    /**
     * 更新时间
     * 表 : bestfenxiao_store
     * 对应字段 : date_edited
     */
    private String dateEdited;

    /**
     * 店铺收入
     * 表 : bestfenxiao_store
     * 对应字段 : income
     */
    private BigDecimal income;

    /**
     * 未提现余额
     * 表 : bestfenxiao_store
     * 对应字段 : balance
     */
    private BigDecimal balance;

    /**
     * 不可用余额
     * 表 : bestfenxiao_store
     * 对应字段 : unbalance
     */
    private BigDecimal unbalance;

    /**
     * 已提现金额
     * 表 : bestfenxiao_store
     * 对应字段 : withdrawal_amount
     */
    private BigDecimal withdrawalAmount;

    /**
     * 提现方式 0对私 1对公
     * 表 : bestfenxiao_store
     * 对应字段 : withdrawal_type
     */
    private Integer withdrawalType;

    /**
     * 开户银行
     * 表 : bestfenxiao_store
     * 对应字段 : bank_id
     */
    private Integer bankId;

    /**
     * 银行卡号
     * 表 : bestfenxiao_store
     * 对应字段 : bank_card
     */
    private String bankCard;

    /**
     * 开卡人姓名
     * 表 : bestfenxiao_store
     * 对应字段 : bank_card_user
     */
    private String bankCardUser;

    /**
     * 开户行
     * 表 : bestfenxiao_store
     * 对应字段 : opening_bank
     */
    private String openingBank;

    /**
     * 最后修改时间
     * 表 : bestfenxiao_store
     * 对应字段 : last_edit_time
     */
    private String lastEditTime;

    /**
     * 
     * 表 : bestfenxiao_store
     * 对应字段 : physical_count
     */
    private Integer physicalCount;

    /**
     * 分销店铺供货商id
     * 表 : bestfenxiao_store
     * 对应字段 : drp_supplier_id
     */
    private Integer drpSupplierId;

    /**
     * 分销级别
     * 表 : bestfenxiao_store
     * 对应字段 : drp_level
     */
    private Integer drpLevel;

    /**
     * 店铺收藏数
     * 表 : bestfenxiao_store
     * 对应字段 : collect
     */
    private Integer collect;

    /**
     * 
     * 表 : bestfenxiao_store
     * 对应字段 : wxpay
     */
    private Integer wxpay;

    /**
     * 是否开启分销商审核
     * 表 : bestfenxiao_store
     * 对应字段 : open_drp_approve
     */
    private Integer openDrpApprove;

    /**
     * 分销商审核状态
     * 表 : bestfenxiao_store
     * 对应字段 : drp_approve
     */
    private Integer drpApprove;

    /**
     * 分销利润
     * 表 : bestfenxiao_store
     * 对应字段 : drp_profit
     */
    private BigDecimal drpProfit;

    /**
     * 分销利润提现
     * 表 : bestfenxiao_store
     * 对应字段 : drp_profit_withdrawal
     */
    private BigDecimal drpProfitWithdrawal;

    /**
     * 是否开启客服
     * 表 : bestfenxiao_store
     * 对应字段 : open_service
     */
    private Integer openService;

    /**
     * 关注数
     * 表 : bestfenxiao_store
     * 对应字段 : attention_num
     */
    private Integer attentionNum;

    /**
     * 对接来源网站url
     * 表 : bestfenxiao_store
     * 对应字段 : source_site_url
     */
    private String sourceSiteUrl;

    /**
     * 站外支付地址
     * 表 : bestfenxiao_store
     * 对应字段 : payment_url
     */
    private String paymentUrl;

    /**
     * 通知地址
     * 表 : bestfenxiao_store
     * 对应字段 : notify_url
     */
    private String notifyUrl;

    /**
     * 对接网站用户认证地址
     * 表 : bestfenxiao_store
     * 对应字段 : oauth_url
     */
    private String oauthUrl;

    /**
     * 微信token
     * 表 : bestfenxiao_store
     * 对应字段 : token
     */
    private String token;

    /**
     * 店铺分销引导
     * 表 : bestfenxiao_store
     * 对应字段 : open_drp_guidance
     */
    private Integer openDrpGuidance;

    /**
     * 分销限制
     * 表 : bestfenxiao_store
     * 对应字段 : open_drp_limit
     */
    private Integer openDrpLimit;

    /**
     * 消费多少金额可分销
     * 表 : bestfenxiao_store
     * 对应字段 : drp_limit_buy
     */
    private BigDecimal drpLimitBuy;

    /**
     * 分享多少次可分销
     * 表 : bestfenxiao_store
     * 对应字段 : drp_limit_share
     */
    private Integer drpLimitShare;

    /**
     * 0 或（2个条件满足一个即可分销） 1 和（2个条件都满足即可分销）
     * 表 : bestfenxiao_store
     * 对应字段 : drp_limit_condition
     */
    private Integer drpLimitCondition;

    /**
     * 
     * 表 : bestfenxiao_store
     * 对应字段 : pigcmsToken
     */
    private String pigcmstoken;

    /**
     * 店铺模板ID
     * 表 : bestfenxiao_store
     * 对应字段 : template_cat_id
     */
    private Integer templateCatId;

    /**
     * 模板ID
     * 表 : bestfenxiao_store
     * 对应字段 : template_id
     */
    private Integer templateId;

    /**
     * 开启分销价设置
     * 表 : bestfenxiao_store
     * 对应字段 : open_drp_setting_price
     */
    private Integer openDrpSettingPrice;

    /**
     * 供货商统一定价
     * 表 : bestfenxiao_store
     * 对应字段 : unified_price_setting
     */
    private Integer unifiedPriceSetting;

    /**
     * 是否开启分销商装修店铺配置
     * 表 : bestfenxiao_store
     * 对应字段 : open_drp_diy_store
     */
    private Integer openDrpDiyStore;

    /**
     * 是否有装修店铺权限
     * 表 : bestfenxiao_store
     * 对应字段 : drp_diy_store
     */
    private Integer drpDiyStore;

    /**
     * 分销是否需要先关注公众号
     * 表 : bestfenxiao_store
     * 对应字段 : open_drp_subscribe
     */
    private Integer openDrpSubscribe;

    /**
     * 关注自动分销
     * 表 : bestfenxiao_store
     * 对应字段 : open_drp_subscribe_auto
     */
    private Integer openDrpSubscribeAuto;

    /**
     * 关注后发送的消息
     * 表 : bestfenxiao_store
     * 对应字段 : drp_subscribe_tpl
     */
    private String drpSubscribeTpl;

    /**
     * 申请分销商关注后发送的消息
     * 表 : bestfenxiao_store
     * 对应字段 : reg_drp_subscribe_tpl
     */
    private String regDrpSubscribeTpl;

    /**
     * 供货商设定的分销商店铺是显示分销商电话:0，供货商电话:1
     * 表 : bestfenxiao_store
     * 对应字段 : is_show_drp_tel
     */
    private Integer isShowDrpTel;

    /**
     * 关注后发送的消息封面图片
     * 表 : bestfenxiao_store
     * 对应字段 : drp_subscribe_img
     */
    private String drpSubscribeImg;

    /**
     * 申请分销商关注后发送的消息封面图片
     * 表 : bestfenxiao_store
     * 对应字段 : reg_drp_subscribe_img
     */
    private String regDrpSubscribeImg;

    /**
     * 是否允许分销商修改店铺名称(默认允许)
     * 表 : bestfenxiao_store
     * 对应字段 : update_drp_store_info
     */
    private Integer updateDrpStoreInfo;

    /**
     * 是否为官方店铺
     * 表 : bestfenxiao_store
     * 对应字段 : is_official_shop
     */
    private Integer isOfficialShop;

    /**
     * 粉丝是否终身制
     * 表 : bestfenxiao_store
     * 对应字段 : setting_fans_forever
     */
    private Integer settingFansForever;

    /**
     * 是否需要审核批发商(默认不允许)
     * 表 : bestfenxiao_store
     * 对应字段 : is_required_to_audit
     */
    private Integer isRequiredToAudit;

    /**
     * 是否需要审核交纳保证金(默认不需要)
     * 表 : bestfenxiao_store
     * 对应字段 : is_required_margin
     */
    private Integer isRequiredMargin;

    /**
     * 保证金额度
     * 表 : bestfenxiao_store
     * 对应字段 : bond
     */
    private Float bond;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table bestfenxiao_store
     *
     * @mbggenerated Fri Apr 20 02:14:07 CST 2018
     */
    private static final long serialVersionUID = 1L;

    /**
     * get method 
     *
     * @return bestfenxiao_store.store_id：店铺id
     */
    public Integer getStoreId() {
        return storeId;
    }

    /**
     * set method 
     *
     * @param storeId  店铺id
     */
    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.uid：会员id
     */
    public Integer getUid() {
        return uid;
    }

    /**
     * set method 
     *
     * @param uid  会员id
     */
    public void setUid(Integer uid) {
        this.uid = uid;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.name：店铺名称
     */
    public String getName() {
        return name;
    }

    /**
     * set method 
     *
     * @param name  店铺名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.edit_name_count：店铺名修改次数
     */
    public Integer getEditNameCount() {
        return editNameCount;
    }

    /**
     * set method 
     *
     * @param editNameCount  店铺名修改次数
     */
    public void setEditNameCount(Integer editNameCount) {
        this.editNameCount = editNameCount;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.logo：店铺logo
     */
    public String getLogo() {
        return logo;
    }

    /**
     * set method 
     *
     * @param logo  店铺logo
     */
    public void setLogo(String logo) {
        this.logo = logo == null ? null : logo.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.qcode：店铺二维码
     */
    public String getQcode() {
        return qcode;
    }

    /**
     * set method 
     *
     * @param qcode  店铺二维码
     */
    public void setQcode(String qcode) {
        this.qcode = qcode == null ? null : qcode.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.openid：微信唯一标识 (关联绑定公众号)
     */
    public String getOpenid() {
        return openid;
    }

    /**
     * set method 
     *
     * @param openid  微信唯一标识 (关联绑定公众号)
     */
    public void setOpenid(String openid) {
        this.openid = openid == null ? null : openid.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.qcode_starttime：二维码开始生效时间
     */
    public Integer getQcodeStarttime() {
        return qcodeStarttime;
    }

    /**
     * set method 
     *
     * @param qcodeStarttime  二维码开始生效时间
     */
    public void setQcodeStarttime(Integer qcodeStarttime) {
        this.qcodeStarttime = qcodeStarttime;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.sale_category_fid：
     */
    public Integer getSaleCategoryFid() {
        return saleCategoryFid;
    }

    /**
     * set method 
     *
     * @param saleCategoryFid  
     */
    public void setSaleCategoryFid(Integer saleCategoryFid) {
        this.saleCategoryFid = saleCategoryFid;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.sale_category_id：主营类目
     */
    public Integer getSaleCategoryId() {
        return saleCategoryId;
    }

    /**
     * set method 
     *
     * @param saleCategoryId  主营类目
     */
    public void setSaleCategoryId(Integer saleCategoryId) {
        this.saleCategoryId = saleCategoryId;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.linkman：联系人
     */
    public String getLinkman() {
        return linkman;
    }

    /**
     * set method 
     *
     * @param linkman  联系人
     */
    public void setLinkman(String linkman) {
        this.linkman = linkman == null ? null : linkman.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.tel：电话
     */
    public String getTel() {
        return tel;
    }

    /**
     * set method 
     *
     * @param tel  电话
     */
    public void setTel(String tel) {
        this.tel = tel == null ? null : tel.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.intro：店铺简介
     */
    public String getIntro() {
        return intro;
    }

    /**
     * set method 
     *
     * @param intro  店铺简介
     */
    public void setIntro(String intro) {
        this.intro = intro == null ? null : intro.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.approve：认证 0未认证 1已证
     */
    public String getApprove() {
        return approve;
    }

    /**
     * set method 
     *
     * @param approve  认证 0未认证 1已证
     */
    public void setApprove(String approve) {
        this.approve = approve == null ? null : approve.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.status：状态 0禁用 1启用
     */
    public String getStatus() {
        return status;
    }

    /**
     * set method 
     *
     * @param status  状态 0禁用 1启用
     */
    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.public_display：开启后将会在微信综合商城 和 pc综合商城展示 0：不展示，1：展示
     */
    public Integer getPublicDisplay() {
        return publicDisplay;
    }

    /**
     * set method 
     *
     * @param publicDisplay  开启后将会在微信综合商城 和 pc综合商城展示 0：不展示，1：展示
     */
    public void setPublicDisplay(Integer publicDisplay) {
        this.publicDisplay = publicDisplay;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.date_added：店铺入驻时间
     */
    public String getDateAdded() {
        return dateAdded;
    }

    /**
     * set method 
     *
     * @param dateAdded  店铺入驻时间
     */
    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded == null ? null : dateAdded.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.service_tel：客服电话
     */
    public String getServiceTel() {
        return serviceTel;
    }

    /**
     * set method 
     *
     * @param serviceTel  客服电话
     */
    public void setServiceTel(String serviceTel) {
        this.serviceTel = serviceTel == null ? null : serviceTel.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.service_qq：客服qq
     */
    public String getServiceQq() {
        return serviceQq;
    }

    /**
     * set method 
     *
     * @param serviceQq  客服qq
     */
    public void setServiceQq(String serviceQq) {
        this.serviceQq = serviceQq == null ? null : serviceQq.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.service_weixin：客服微信
     */
    public String getServiceWeixin() {
        return serviceWeixin;
    }

    /**
     * set method 
     *
     * @param serviceWeixin  客服微信
     */
    public void setServiceWeixin(String serviceWeixin) {
        this.serviceWeixin = serviceWeixin == null ? null : serviceWeixin.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.bind_weixin：绑定微信 0未绑定 1已绑定
     */
    public Integer getBindWeixin() {
        return bindWeixin;
    }

    /**
     * set method 
     *
     * @param bindWeixin  绑定微信 0未绑定 1已绑定
     */
    public void setBindWeixin(Integer bindWeixin) {
        this.bindWeixin = bindWeixin;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.weixin_name：公众号名称
     */
    public String getWeixinName() {
        return weixinName;
    }

    /**
     * set method 
     *
     * @param weixinName  公众号名称
     */
    public void setWeixinName(String weixinName) {
        this.weixinName = weixinName == null ? null : weixinName.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.weixin_original_id：微信原始ID
     */
    public String getWeixinOriginalId() {
        return weixinOriginalId;
    }

    /**
     * set method 
     *
     * @param weixinOriginalId  微信原始ID
     */
    public void setWeixinOriginalId(String weixinOriginalId) {
        this.weixinOriginalId = weixinOriginalId == null ? null : weixinOriginalId.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.weixin_id：微信ID
     */
    public String getWeixinId() {
        return weixinId;
    }

    /**
     * set method 
     *
     * @param weixinId  微信ID
     */
    public void setWeixinId(String weixinId) {
        this.weixinId = weixinId == null ? null : weixinId.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.qq：qq
     */
    public String getQq() {
        return qq;
    }

    /**
     * set method 
     *
     * @param qq  qq
     */
    public void setQq(String qq) {
        this.qq = qq == null ? null : qq.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.open_weixin：绑定微信
     */
    public String getOpenWeixin() {
        return openWeixin;
    }

    /**
     * set method 
     *
     * @param openWeixin  绑定微信
     */
    public void setOpenWeixin(String openWeixin) {
        this.openWeixin = openWeixin == null ? null : openWeixin.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.buyer_selffetch：买家上门自提
     */
    public String getBuyerSelffetch() {
        return buyerSelffetch;
    }

    /**
     * set method 
     *
     * @param buyerSelffetch  买家上门自提
     */
    public void setBuyerSelffetch(String buyerSelffetch) {
        this.buyerSelffetch = buyerSelffetch == null ? null : buyerSelffetch.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.buyer_selffetch_name：“上门自提”自定义名称
     */
    public String getBuyerSelffetchName() {
        return buyerSelffetchName;
    }

    /**
     * set method 
     *
     * @param buyerSelffetchName  “上门自提”自定义名称
     */
    public void setBuyerSelffetchName(String buyerSelffetchName) {
        this.buyerSelffetchName = buyerSelffetchName == null ? null : buyerSelffetchName.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.pay_agent：代付
     */
    public String getPayAgent() {
        return payAgent;
    }

    /**
     * set method 
     *
     * @param payAgent  代付
     */
    public void setPayAgent(String payAgent) {
        this.payAgent = payAgent == null ? null : payAgent.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.offline_payment：是否支持货到付款，0：是，1：否
     */
    public Integer getOfflinePayment() {
        return offlinePayment;
    }

    /**
     * set method 
     *
     * @param offlinePayment  是否支持货到付款，0：是，1：否
     */
    public void setOfflinePayment(Integer offlinePayment) {
        this.offlinePayment = offlinePayment;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.open_logistics：是否开启物流配送，1：开启，0：关闭
     */
    public Integer getOpenLogistics() {
        return openLogistics;
    }

    /**
     * set method 
     *
     * @param openLogistics  是否开启物流配送，1：开启，0：关闭
     */
    public void setOpenLogistics(Integer openLogistics) {
        this.openLogistics = openLogistics;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.open_friend：是否开启送朋友，1：是，0：否
     */
    public Integer getOpenFriend() {
        return openFriend;
    }

    /**
     * set method 
     *
     * @param openFriend  是否开启送朋友，1：是，0：否
     */
    public void setOpenFriend(Integer openFriend) {
        this.openFriend = openFriend;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.open_autoassign：是否开启订单自动分配到门店： 1是 0否
     */
    public Integer getOpenAutoassign() {
        return openAutoassign;
    }

    /**
     * set method 
     *
     * @param openAutoassign  是否开启订单自动分配到门店： 1是 0否
     */
    public void setOpenAutoassign(Integer openAutoassign) {
        this.openAutoassign = openAutoassign;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.open_local_logistics：是否使用本地化物流: 0否 1是
     */
    public Integer getOpenLocalLogistics() {
        return openLocalLogistics;
    }

    /**
     * set method 
     *
     * @param openLocalLogistics  是否使用本地化物流: 0否 1是
     */
    public void setOpenLocalLogistics(Integer openLocalLogistics) {
        this.openLocalLogistics = openLocalLogistics;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.warn_sp_quantity：门店库存报警：0为不报警
     */
    public Integer getWarnSpQuantity() {
        return warnSpQuantity;
    }

    /**
     * set method 
     *
     * @param warnSpQuantity  门店库存报警：0为不报警
     */
    public void setWarnSpQuantity(Integer warnSpQuantity) {
        this.warnSpQuantity = warnSpQuantity;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.open_nav：开启店铺导航
     */
    public Integer getOpenNav() {
        return openNav;
    }

    /**
     * set method 
     *
     * @param openNav  开启店铺导航
     */
    public void setOpenNav(Integer openNav) {
        this.openNav = openNav;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.nav_style_id：店铺导航样式
     */
    public Integer getNavStyleId() {
        return navStyleId;
    }

    /**
     * set method 
     *
     * @param navStyleId  店铺导航样式
     */
    public void setNavStyleId(Integer navStyleId) {
        this.navStyleId = navStyleId;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.use_nav_pages：使用导航菜单的页面 1店铺主页 2会员主页 3微页面及分类 4商品分组 5商品搜索
     */
    public String getUseNavPages() {
        return useNavPages;
    }

    /**
     * set method 
     *
     * @param useNavPages  使用导航菜单的页面 1店铺主页 2会员主页 3微页面及分类 4商品分组 5商品搜索
     */
    public void setUseNavPages(String useNavPages) {
        this.useNavPages = useNavPages == null ? null : useNavPages.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.open_ad：开启广告
     */
    public Integer getOpenAd() {
        return openAd;
    }

    /**
     * set method 
     *
     * @param openAd  开启广告
     */
    public void setOpenAd(Integer openAd) {
        this.openAd = openAd;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.has_ad：是否有店铺广告
     */
    public Integer getHasAd() {
        return hasAd;
    }

    /**
     * set method 
     *
     * @param hasAd  是否有店铺广告
     */
    public void setHasAd(Integer hasAd) {
        this.hasAd = hasAd;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.ad_position：广告位置 0页面头部 1页面底部
     */
    public Integer getAdPosition() {
        return adPosition;
    }

    /**
     * set method 
     *
     * @param adPosition  广告位置 0页面头部 1页面底部
     */
    public void setAdPosition(Integer adPosition) {
        this.adPosition = adPosition;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.use_ad_pages：使用广告的页面 1微页面 2微页面分类 3商品 4商品分组 5店铺主页 6会员主页
     */
    public String getUseAdPages() {
        return useAdPages;
    }

    /**
     * set method 
     *
     * @param useAdPages  使用广告的页面 1微页面 2微页面分类 3商品 4商品分组 5店铺主页 6会员主页
     */
    public void setUseAdPages(String useAdPages) {
        this.useAdPages = useAdPages == null ? null : useAdPages.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.date_edited：更新时间
     */
    public String getDateEdited() {
        return dateEdited;
    }

    /**
     * set method 
     *
     * @param dateEdited  更新时间
     */
    public void setDateEdited(String dateEdited) {
        this.dateEdited = dateEdited == null ? null : dateEdited.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.income：店铺收入
     */
    public BigDecimal getIncome() {
        return income;
    }

    /**
     * set method 
     *
     * @param income  店铺收入
     */
    public void setIncome(BigDecimal income) {
        this.income = income;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.balance：未提现余额
     */
    public BigDecimal getBalance() {
        return balance;
    }

    /**
     * set method 
     *
     * @param balance  未提现余额
     */
    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.unbalance：不可用余额
     */
    public BigDecimal getUnbalance() {
        return unbalance;
    }

    /**
     * set method 
     *
     * @param unbalance  不可用余额
     */
    public void setUnbalance(BigDecimal unbalance) {
        this.unbalance = unbalance;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.withdrawal_amount：已提现金额
     */
    public BigDecimal getWithdrawalAmount() {
        return withdrawalAmount;
    }

    /**
     * set method 
     *
     * @param withdrawalAmount  已提现金额
     */
    public void setWithdrawalAmount(BigDecimal withdrawalAmount) {
        this.withdrawalAmount = withdrawalAmount;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.withdrawal_type：提现方式 0对私 1对公
     */
    public Integer getWithdrawalType() {
        return withdrawalType;
    }

    /**
     * set method 
     *
     * @param withdrawalType  提现方式 0对私 1对公
     */
    public void setWithdrawalType(Integer withdrawalType) {
        this.withdrawalType = withdrawalType;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.bank_id：开户银行
     */
    public Integer getBankId() {
        return bankId;
    }

    /**
     * set method 
     *
     * @param bankId  开户银行
     */
    public void setBankId(Integer bankId) {
        this.bankId = bankId;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.bank_card：银行卡号
     */
    public String getBankCard() {
        return bankCard;
    }

    /**
     * set method 
     *
     * @param bankCard  银行卡号
     */
    public void setBankCard(String bankCard) {
        this.bankCard = bankCard == null ? null : bankCard.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.bank_card_user：开卡人姓名
     */
    public String getBankCardUser() {
        return bankCardUser;
    }

    /**
     * set method 
     *
     * @param bankCardUser  开卡人姓名
     */
    public void setBankCardUser(String bankCardUser) {
        this.bankCardUser = bankCardUser == null ? null : bankCardUser.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.opening_bank：开户行
     */
    public String getOpeningBank() {
        return openingBank;
    }

    /**
     * set method 
     *
     * @param openingBank  开户行
     */
    public void setOpeningBank(String openingBank) {
        this.openingBank = openingBank == null ? null : openingBank.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.last_edit_time：最后修改时间
     */
    public String getLastEditTime() {
        return lastEditTime;
    }

    /**
     * set method 
     *
     * @param lastEditTime  最后修改时间
     */
    public void setLastEditTime(String lastEditTime) {
        this.lastEditTime = lastEditTime == null ? null : lastEditTime.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.physical_count：
     */
    public Integer getPhysicalCount() {
        return physicalCount;
    }

    /**
     * set method 
     *
     * @param physicalCount  
     */
    public void setPhysicalCount(Integer physicalCount) {
        this.physicalCount = physicalCount;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.drp_supplier_id：分销店铺供货商id
     */
    public Integer getDrpSupplierId() {
        return drpSupplierId;
    }

    /**
     * set method 
     *
     * @param drpSupplierId  分销店铺供货商id
     */
    public void setDrpSupplierId(Integer drpSupplierId) {
        this.drpSupplierId = drpSupplierId;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.drp_level：分销级别
     */
    public Integer getDrpLevel() {
        return drpLevel;
    }

    /**
     * set method 
     *
     * @param drpLevel  分销级别
     */
    public void setDrpLevel(Integer drpLevel) {
        this.drpLevel = drpLevel;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.collect：店铺收藏数
     */
    public Integer getCollect() {
        return collect;
    }

    /**
     * set method 
     *
     * @param collect  店铺收藏数
     */
    public void setCollect(Integer collect) {
        this.collect = collect;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.wxpay：
     */
    public Integer getWxpay() {
        return wxpay;
    }

    /**
     * set method 
     *
     * @param wxpay  
     */
    public void setWxpay(Integer wxpay) {
        this.wxpay = wxpay;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.open_drp_approve：是否开启分销商审核
     */
    public Integer getOpenDrpApprove() {
        return openDrpApprove;
    }

    /**
     * set method 
     *
     * @param openDrpApprove  是否开启分销商审核
     */
    public void setOpenDrpApprove(Integer openDrpApprove) {
        this.openDrpApprove = openDrpApprove;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.drp_approve：分销商审核状态
     */
    public Integer getDrpApprove() {
        return drpApprove;
    }

    /**
     * set method 
     *
     * @param drpApprove  分销商审核状态
     */
    public void setDrpApprove(Integer drpApprove) {
        this.drpApprove = drpApprove;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.drp_profit：分销利润
     */
    public BigDecimal getDrpProfit() {
        return drpProfit;
    }

    /**
     * set method 
     *
     * @param drpProfit  分销利润
     */
    public void setDrpProfit(BigDecimal drpProfit) {
        this.drpProfit = drpProfit;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.drp_profit_withdrawal：分销利润提现
     */
    public BigDecimal getDrpProfitWithdrawal() {
        return drpProfitWithdrawal;
    }

    /**
     * set method 
     *
     * @param drpProfitWithdrawal  分销利润提现
     */
    public void setDrpProfitWithdrawal(BigDecimal drpProfitWithdrawal) {
        this.drpProfitWithdrawal = drpProfitWithdrawal;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.open_service：是否开启客服
     */
    public Integer getOpenService() {
        return openService;
    }

    /**
     * set method 
     *
     * @param openService  是否开启客服
     */
    public void setOpenService(Integer openService) {
        this.openService = openService;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.attention_num：关注数
     */
    public Integer getAttentionNum() {
        return attentionNum;
    }

    /**
     * set method 
     *
     * @param attentionNum  关注数
     */
    public void setAttentionNum(Integer attentionNum) {
        this.attentionNum = attentionNum;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.source_site_url：对接来源网站url
     */
    public String getSourceSiteUrl() {
        return sourceSiteUrl;
    }

    /**
     * set method 
     *
     * @param sourceSiteUrl  对接来源网站url
     */
    public void setSourceSiteUrl(String sourceSiteUrl) {
        this.sourceSiteUrl = sourceSiteUrl == null ? null : sourceSiteUrl.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.payment_url：站外支付地址
     */
    public String getPaymentUrl() {
        return paymentUrl;
    }

    /**
     * set method 
     *
     * @param paymentUrl  站外支付地址
     */
    public void setPaymentUrl(String paymentUrl) {
        this.paymentUrl = paymentUrl == null ? null : paymentUrl.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.notify_url：通知地址
     */
    public String getNotifyUrl() {
        return notifyUrl;
    }

    /**
     * set method 
     *
     * @param notifyUrl  通知地址
     */
    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl == null ? null : notifyUrl.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.oauth_url：对接网站用户认证地址
     */
    public String getOauthUrl() {
        return oauthUrl;
    }

    /**
     * set method 
     *
     * @param oauthUrl  对接网站用户认证地址
     */
    public void setOauthUrl(String oauthUrl) {
        this.oauthUrl = oauthUrl == null ? null : oauthUrl.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.token：微信token
     */
    public String getToken() {
        return token;
    }

    /**
     * set method 
     *
     * @param token  微信token
     */
    public void setToken(String token) {
        this.token = token == null ? null : token.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.open_drp_guidance：店铺分销引导
     */
    public Integer getOpenDrpGuidance() {
        return openDrpGuidance;
    }

    /**
     * set method 
     *
     * @param openDrpGuidance  店铺分销引导
     */
    public void setOpenDrpGuidance(Integer openDrpGuidance) {
        this.openDrpGuidance = openDrpGuidance;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.open_drp_limit：分销限制
     */
    public Integer getOpenDrpLimit() {
        return openDrpLimit;
    }

    /**
     * set method 
     *
     * @param openDrpLimit  分销限制
     */
    public void setOpenDrpLimit(Integer openDrpLimit) {
        this.openDrpLimit = openDrpLimit;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.drp_limit_buy：消费多少金额可分销
     */
    public BigDecimal getDrpLimitBuy() {
        return drpLimitBuy;
    }

    /**
     * set method 
     *
     * @param drpLimitBuy  消费多少金额可分销
     */
    public void setDrpLimitBuy(BigDecimal drpLimitBuy) {
        this.drpLimitBuy = drpLimitBuy;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.drp_limit_share：分享多少次可分销
     */
    public Integer getDrpLimitShare() {
        return drpLimitShare;
    }

    /**
     * set method 
     *
     * @param drpLimitShare  分享多少次可分销
     */
    public void setDrpLimitShare(Integer drpLimitShare) {
        this.drpLimitShare = drpLimitShare;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.drp_limit_condition：0 或（2个条件满足一个即可分销） 1 和（2个条件都满足即可分销）
     */
    public Integer getDrpLimitCondition() {
        return drpLimitCondition;
    }

    /**
     * set method 
     *
     * @param drpLimitCondition  0 或（2个条件满足一个即可分销） 1 和（2个条件都满足即可分销）
     */
    public void setDrpLimitCondition(Integer drpLimitCondition) {
        this.drpLimitCondition = drpLimitCondition;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.pigcmsToken：
     */
    public String getPigcmstoken() {
        return pigcmstoken;
    }

    /**
     * set method 
     *
     * @param pigcmstoken  
     */
    public void setPigcmstoken(String pigcmstoken) {
        this.pigcmstoken = pigcmstoken == null ? null : pigcmstoken.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.template_cat_id：店铺模板ID
     */
    public Integer getTemplateCatId() {
        return templateCatId;
    }

    /**
     * set method 
     *
     * @param templateCatId  店铺模板ID
     */
    public void setTemplateCatId(Integer templateCatId) {
        this.templateCatId = templateCatId;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.template_id：模板ID
     */
    public Integer getTemplateId() {
        return templateId;
    }

    /**
     * set method 
     *
     * @param templateId  模板ID
     */
    public void setTemplateId(Integer templateId) {
        this.templateId = templateId;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.open_drp_setting_price：开启分销价设置
     */
    public Integer getOpenDrpSettingPrice() {
        return openDrpSettingPrice;
    }

    /**
     * set method 
     *
     * @param openDrpSettingPrice  开启分销价设置
     */
    public void setOpenDrpSettingPrice(Integer openDrpSettingPrice) {
        this.openDrpSettingPrice = openDrpSettingPrice;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.unified_price_setting：供货商统一定价
     */
    public Integer getUnifiedPriceSetting() {
        return unifiedPriceSetting;
    }

    /**
     * set method 
     *
     * @param unifiedPriceSetting  供货商统一定价
     */
    public void setUnifiedPriceSetting(Integer unifiedPriceSetting) {
        this.unifiedPriceSetting = unifiedPriceSetting;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.open_drp_diy_store：是否开启分销商装修店铺配置
     */
    public Integer getOpenDrpDiyStore() {
        return openDrpDiyStore;
    }

    /**
     * set method 
     *
     * @param openDrpDiyStore  是否开启分销商装修店铺配置
     */
    public void setOpenDrpDiyStore(Integer openDrpDiyStore) {
        this.openDrpDiyStore = openDrpDiyStore;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.drp_diy_store：是否有装修店铺权限
     */
    public Integer getDrpDiyStore() {
        return drpDiyStore;
    }

    /**
     * set method 
     *
     * @param drpDiyStore  是否有装修店铺权限
     */
    public void setDrpDiyStore(Integer drpDiyStore) {
        this.drpDiyStore = drpDiyStore;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.open_drp_subscribe：分销是否需要先关注公众号
     */
    public Integer getOpenDrpSubscribe() {
        return openDrpSubscribe;
    }

    /**
     * set method 
     *
     * @param openDrpSubscribe  分销是否需要先关注公众号
     */
    public void setOpenDrpSubscribe(Integer openDrpSubscribe) {
        this.openDrpSubscribe = openDrpSubscribe;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.open_drp_subscribe_auto：关注自动分销
     */
    public Integer getOpenDrpSubscribeAuto() {
        return openDrpSubscribeAuto;
    }

    /**
     * set method 
     *
     * @param openDrpSubscribeAuto  关注自动分销
     */
    public void setOpenDrpSubscribeAuto(Integer openDrpSubscribeAuto) {
        this.openDrpSubscribeAuto = openDrpSubscribeAuto;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.drp_subscribe_tpl：关注后发送的消息
     */
    public String getDrpSubscribeTpl() {
        return drpSubscribeTpl;
    }

    /**
     * set method 
     *
     * @param drpSubscribeTpl  关注后发送的消息
     */
    public void setDrpSubscribeTpl(String drpSubscribeTpl) {
        this.drpSubscribeTpl = drpSubscribeTpl == null ? null : drpSubscribeTpl.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.reg_drp_subscribe_tpl：申请分销商关注后发送的消息
     */
    public String getRegDrpSubscribeTpl() {
        return regDrpSubscribeTpl;
    }

    /**
     * set method 
     *
     * @param regDrpSubscribeTpl  申请分销商关注后发送的消息
     */
    public void setRegDrpSubscribeTpl(String regDrpSubscribeTpl) {
        this.regDrpSubscribeTpl = regDrpSubscribeTpl == null ? null : regDrpSubscribeTpl.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.is_show_drp_tel：供货商设定的分销商店铺是显示分销商电话:0，供货商电话:1
     */
    public Integer getIsShowDrpTel() {
        return isShowDrpTel;
    }

    /**
     * set method 
     *
     * @param isShowDrpTel  供货商设定的分销商店铺是显示分销商电话:0，供货商电话:1
     */
    public void setIsShowDrpTel(Integer isShowDrpTel) {
        this.isShowDrpTel = isShowDrpTel;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.drp_subscribe_img：关注后发送的消息封面图片
     */
    public String getDrpSubscribeImg() {
        return drpSubscribeImg;
    }

    /**
     * set method 
     *
     * @param drpSubscribeImg  关注后发送的消息封面图片
     */
    public void setDrpSubscribeImg(String drpSubscribeImg) {
        this.drpSubscribeImg = drpSubscribeImg == null ? null : drpSubscribeImg.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.reg_drp_subscribe_img：申请分销商关注后发送的消息封面图片
     */
    public String getRegDrpSubscribeImg() {
        return regDrpSubscribeImg;
    }

    /**
     * set method 
     *
     * @param regDrpSubscribeImg  申请分销商关注后发送的消息封面图片
     */
    public void setRegDrpSubscribeImg(String regDrpSubscribeImg) {
        this.regDrpSubscribeImg = regDrpSubscribeImg == null ? null : regDrpSubscribeImg.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.update_drp_store_info：是否允许分销商修改店铺名称(默认允许)
     */
    public Integer getUpdateDrpStoreInfo() {
        return updateDrpStoreInfo;
    }

    /**
     * set method 
     *
     * @param updateDrpStoreInfo  是否允许分销商修改店铺名称(默认允许)
     */
    public void setUpdateDrpStoreInfo(Integer updateDrpStoreInfo) {
        this.updateDrpStoreInfo = updateDrpStoreInfo;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.is_official_shop：是否为官方店铺
     */
    public Integer getIsOfficialShop() {
        return isOfficialShop;
    }

    /**
     * set method 
     *
     * @param isOfficialShop  是否为官方店铺
     */
    public void setIsOfficialShop(Integer isOfficialShop) {
        this.isOfficialShop = isOfficialShop;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.setting_fans_forever：粉丝是否终身制
     */
    public Integer getSettingFansForever() {
        return settingFansForever;
    }

    /**
     * set method 
     *
     * @param settingFansForever  粉丝是否终身制
     */
    public void setSettingFansForever(Integer settingFansForever) {
        this.settingFansForever = settingFansForever;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.is_required_to_audit：是否需要审核批发商(默认不允许)
     */
    public Integer getIsRequiredToAudit() {
        return isRequiredToAudit;
    }

    /**
     * set method 
     *
     * @param isRequiredToAudit  是否需要审核批发商(默认不允许)
     */
    public void setIsRequiredToAudit(Integer isRequiredToAudit) {
        this.isRequiredToAudit = isRequiredToAudit;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.is_required_margin：是否需要审核交纳保证金(默认不需要)
     */
    public Integer getIsRequiredMargin() {
        return isRequiredMargin;
    }

    /**
     * set method 
     *
     * @param isRequiredMargin  是否需要审核交纳保证金(默认不需要)
     */
    public void setIsRequiredMargin(Integer isRequiredMargin) {
        this.isRequiredMargin = isRequiredMargin;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_store.bond：保证金额度
     */
    public Float getBond() {
        return bond;
    }

    /**
     * set method 
     *
     * @param bond  保证金额度
     */
    public void setBond(Float bond) {
        this.bond = bond;
    }

    /**
     *
     * @param that
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Store other = (Store) that;
        return (this.getStoreId() == null ? other.getStoreId() == null : this.getStoreId().equals(other.getStoreId()))
            && (this.getUid() == null ? other.getUid() == null : this.getUid().equals(other.getUid()))
            && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
            && (this.getEditNameCount() == null ? other.getEditNameCount() == null : this.getEditNameCount().equals(other.getEditNameCount()))
            && (this.getLogo() == null ? other.getLogo() == null : this.getLogo().equals(other.getLogo()))
            && (this.getQcode() == null ? other.getQcode() == null : this.getQcode().equals(other.getQcode()))
            && (this.getOpenid() == null ? other.getOpenid() == null : this.getOpenid().equals(other.getOpenid()))
            && (this.getQcodeStarttime() == null ? other.getQcodeStarttime() == null : this.getQcodeStarttime().equals(other.getQcodeStarttime()))
            && (this.getSaleCategoryFid() == null ? other.getSaleCategoryFid() == null : this.getSaleCategoryFid().equals(other.getSaleCategoryFid()))
            && (this.getSaleCategoryId() == null ? other.getSaleCategoryId() == null : this.getSaleCategoryId().equals(other.getSaleCategoryId()))
            && (this.getLinkman() == null ? other.getLinkman() == null : this.getLinkman().equals(other.getLinkman()))
            && (this.getTel() == null ? other.getTel() == null : this.getTel().equals(other.getTel()))
            && (this.getIntro() == null ? other.getIntro() == null : this.getIntro().equals(other.getIntro()))
            && (this.getApprove() == null ? other.getApprove() == null : this.getApprove().equals(other.getApprove()))
            && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()))
            && (this.getPublicDisplay() == null ? other.getPublicDisplay() == null : this.getPublicDisplay().equals(other.getPublicDisplay()))
            && (this.getDateAdded() == null ? other.getDateAdded() == null : this.getDateAdded().equals(other.getDateAdded()))
            && (this.getServiceTel() == null ? other.getServiceTel() == null : this.getServiceTel().equals(other.getServiceTel()))
            && (this.getServiceQq() == null ? other.getServiceQq() == null : this.getServiceQq().equals(other.getServiceQq()))
            && (this.getServiceWeixin() == null ? other.getServiceWeixin() == null : this.getServiceWeixin().equals(other.getServiceWeixin()))
            && (this.getBindWeixin() == null ? other.getBindWeixin() == null : this.getBindWeixin().equals(other.getBindWeixin()))
            && (this.getWeixinName() == null ? other.getWeixinName() == null : this.getWeixinName().equals(other.getWeixinName()))
            && (this.getWeixinOriginalId() == null ? other.getWeixinOriginalId() == null : this.getWeixinOriginalId().equals(other.getWeixinOriginalId()))
            && (this.getWeixinId() == null ? other.getWeixinId() == null : this.getWeixinId().equals(other.getWeixinId()))
            && (this.getQq() == null ? other.getQq() == null : this.getQq().equals(other.getQq()))
            && (this.getOpenWeixin() == null ? other.getOpenWeixin() == null : this.getOpenWeixin().equals(other.getOpenWeixin()))
            && (this.getBuyerSelffetch() == null ? other.getBuyerSelffetch() == null : this.getBuyerSelffetch().equals(other.getBuyerSelffetch()))
            && (this.getBuyerSelffetchName() == null ? other.getBuyerSelffetchName() == null : this.getBuyerSelffetchName().equals(other.getBuyerSelffetchName()))
            && (this.getPayAgent() == null ? other.getPayAgent() == null : this.getPayAgent().equals(other.getPayAgent()))
            && (this.getOfflinePayment() == null ? other.getOfflinePayment() == null : this.getOfflinePayment().equals(other.getOfflinePayment()))
            && (this.getOpenLogistics() == null ? other.getOpenLogistics() == null : this.getOpenLogistics().equals(other.getOpenLogistics()))
            && (this.getOpenFriend() == null ? other.getOpenFriend() == null : this.getOpenFriend().equals(other.getOpenFriend()))
            && (this.getOpenAutoassign() == null ? other.getOpenAutoassign() == null : this.getOpenAutoassign().equals(other.getOpenAutoassign()))
            && (this.getOpenLocalLogistics() == null ? other.getOpenLocalLogistics() == null : this.getOpenLocalLogistics().equals(other.getOpenLocalLogistics()))
            && (this.getWarnSpQuantity() == null ? other.getWarnSpQuantity() == null : this.getWarnSpQuantity().equals(other.getWarnSpQuantity()))
            && (this.getOpenNav() == null ? other.getOpenNav() == null : this.getOpenNav().equals(other.getOpenNav()))
            && (this.getNavStyleId() == null ? other.getNavStyleId() == null : this.getNavStyleId().equals(other.getNavStyleId()))
            && (this.getUseNavPages() == null ? other.getUseNavPages() == null : this.getUseNavPages().equals(other.getUseNavPages()))
            && (this.getOpenAd() == null ? other.getOpenAd() == null : this.getOpenAd().equals(other.getOpenAd()))
            && (this.getHasAd() == null ? other.getHasAd() == null : this.getHasAd().equals(other.getHasAd()))
            && (this.getAdPosition() == null ? other.getAdPosition() == null : this.getAdPosition().equals(other.getAdPosition()))
            && (this.getUseAdPages() == null ? other.getUseAdPages() == null : this.getUseAdPages().equals(other.getUseAdPages()))
            && (this.getDateEdited() == null ? other.getDateEdited() == null : this.getDateEdited().equals(other.getDateEdited()))
            && (this.getIncome() == null ? other.getIncome() == null : this.getIncome().equals(other.getIncome()))
            && (this.getBalance() == null ? other.getBalance() == null : this.getBalance().equals(other.getBalance()))
            && (this.getUnbalance() == null ? other.getUnbalance() == null : this.getUnbalance().equals(other.getUnbalance()))
            && (this.getWithdrawalAmount() == null ? other.getWithdrawalAmount() == null : this.getWithdrawalAmount().equals(other.getWithdrawalAmount()))
            && (this.getWithdrawalType() == null ? other.getWithdrawalType() == null : this.getWithdrawalType().equals(other.getWithdrawalType()))
            && (this.getBankId() == null ? other.getBankId() == null : this.getBankId().equals(other.getBankId()))
            && (this.getBankCard() == null ? other.getBankCard() == null : this.getBankCard().equals(other.getBankCard()))
            && (this.getBankCardUser() == null ? other.getBankCardUser() == null : this.getBankCardUser().equals(other.getBankCardUser()))
            && (this.getOpeningBank() == null ? other.getOpeningBank() == null : this.getOpeningBank().equals(other.getOpeningBank()))
            && (this.getLastEditTime() == null ? other.getLastEditTime() == null : this.getLastEditTime().equals(other.getLastEditTime()))
            && (this.getPhysicalCount() == null ? other.getPhysicalCount() == null : this.getPhysicalCount().equals(other.getPhysicalCount()))
            && (this.getDrpSupplierId() == null ? other.getDrpSupplierId() == null : this.getDrpSupplierId().equals(other.getDrpSupplierId()))
            && (this.getDrpLevel() == null ? other.getDrpLevel() == null : this.getDrpLevel().equals(other.getDrpLevel()))
            && (this.getCollect() == null ? other.getCollect() == null : this.getCollect().equals(other.getCollect()))
            && (this.getWxpay() == null ? other.getWxpay() == null : this.getWxpay().equals(other.getWxpay()))
            && (this.getOpenDrpApprove() == null ? other.getOpenDrpApprove() == null : this.getOpenDrpApprove().equals(other.getOpenDrpApprove()))
            && (this.getDrpApprove() == null ? other.getDrpApprove() == null : this.getDrpApprove().equals(other.getDrpApprove()))
            && (this.getDrpProfit() == null ? other.getDrpProfit() == null : this.getDrpProfit().equals(other.getDrpProfit()))
            && (this.getDrpProfitWithdrawal() == null ? other.getDrpProfitWithdrawal() == null : this.getDrpProfitWithdrawal().equals(other.getDrpProfitWithdrawal()))
            && (this.getOpenService() == null ? other.getOpenService() == null : this.getOpenService().equals(other.getOpenService()))
            && (this.getAttentionNum() == null ? other.getAttentionNum() == null : this.getAttentionNum().equals(other.getAttentionNum()))
            && (this.getSourceSiteUrl() == null ? other.getSourceSiteUrl() == null : this.getSourceSiteUrl().equals(other.getSourceSiteUrl()))
            && (this.getPaymentUrl() == null ? other.getPaymentUrl() == null : this.getPaymentUrl().equals(other.getPaymentUrl()))
            && (this.getNotifyUrl() == null ? other.getNotifyUrl() == null : this.getNotifyUrl().equals(other.getNotifyUrl()))
            && (this.getOauthUrl() == null ? other.getOauthUrl() == null : this.getOauthUrl().equals(other.getOauthUrl()))
            && (this.getToken() == null ? other.getToken() == null : this.getToken().equals(other.getToken()))
            && (this.getOpenDrpGuidance() == null ? other.getOpenDrpGuidance() == null : this.getOpenDrpGuidance().equals(other.getOpenDrpGuidance()))
            && (this.getOpenDrpLimit() == null ? other.getOpenDrpLimit() == null : this.getOpenDrpLimit().equals(other.getOpenDrpLimit()))
            && (this.getDrpLimitBuy() == null ? other.getDrpLimitBuy() == null : this.getDrpLimitBuy().equals(other.getDrpLimitBuy()))
            && (this.getDrpLimitShare() == null ? other.getDrpLimitShare() == null : this.getDrpLimitShare().equals(other.getDrpLimitShare()))
            && (this.getDrpLimitCondition() == null ? other.getDrpLimitCondition() == null : this.getDrpLimitCondition().equals(other.getDrpLimitCondition()))
            && (this.getPigcmstoken() == null ? other.getPigcmstoken() == null : this.getPigcmstoken().equals(other.getPigcmstoken()))
            && (this.getTemplateCatId() == null ? other.getTemplateCatId() == null : this.getTemplateCatId().equals(other.getTemplateCatId()))
            && (this.getTemplateId() == null ? other.getTemplateId() == null : this.getTemplateId().equals(other.getTemplateId()))
            && (this.getOpenDrpSettingPrice() == null ? other.getOpenDrpSettingPrice() == null : this.getOpenDrpSettingPrice().equals(other.getOpenDrpSettingPrice()))
            && (this.getUnifiedPriceSetting() == null ? other.getUnifiedPriceSetting() == null : this.getUnifiedPriceSetting().equals(other.getUnifiedPriceSetting()))
            && (this.getOpenDrpDiyStore() == null ? other.getOpenDrpDiyStore() == null : this.getOpenDrpDiyStore().equals(other.getOpenDrpDiyStore()))
            && (this.getDrpDiyStore() == null ? other.getDrpDiyStore() == null : this.getDrpDiyStore().equals(other.getDrpDiyStore()))
            && (this.getOpenDrpSubscribe() == null ? other.getOpenDrpSubscribe() == null : this.getOpenDrpSubscribe().equals(other.getOpenDrpSubscribe()))
            && (this.getOpenDrpSubscribeAuto() == null ? other.getOpenDrpSubscribeAuto() == null : this.getOpenDrpSubscribeAuto().equals(other.getOpenDrpSubscribeAuto()))
            && (this.getDrpSubscribeTpl() == null ? other.getDrpSubscribeTpl() == null : this.getDrpSubscribeTpl().equals(other.getDrpSubscribeTpl()))
            && (this.getRegDrpSubscribeTpl() == null ? other.getRegDrpSubscribeTpl() == null : this.getRegDrpSubscribeTpl().equals(other.getRegDrpSubscribeTpl()))
            && (this.getIsShowDrpTel() == null ? other.getIsShowDrpTel() == null : this.getIsShowDrpTel().equals(other.getIsShowDrpTel()))
            && (this.getDrpSubscribeImg() == null ? other.getDrpSubscribeImg() == null : this.getDrpSubscribeImg().equals(other.getDrpSubscribeImg()))
            && (this.getRegDrpSubscribeImg() == null ? other.getRegDrpSubscribeImg() == null : this.getRegDrpSubscribeImg().equals(other.getRegDrpSubscribeImg()))
            && (this.getUpdateDrpStoreInfo() == null ? other.getUpdateDrpStoreInfo() == null : this.getUpdateDrpStoreInfo().equals(other.getUpdateDrpStoreInfo()))
            && (this.getIsOfficialShop() == null ? other.getIsOfficialShop() == null : this.getIsOfficialShop().equals(other.getIsOfficialShop()))
            && (this.getSettingFansForever() == null ? other.getSettingFansForever() == null : this.getSettingFansForever().equals(other.getSettingFansForever()))
            && (this.getIsRequiredToAudit() == null ? other.getIsRequiredToAudit() == null : this.getIsRequiredToAudit().equals(other.getIsRequiredToAudit()))
            && (this.getIsRequiredMargin() == null ? other.getIsRequiredMargin() == null : this.getIsRequiredMargin().equals(other.getIsRequiredMargin()))
            && (this.getBond() == null ? other.getBond() == null : this.getBond().equals(other.getBond()));
    }

    /**
     *
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getStoreId() == null) ? 0 : getStoreId().hashCode());
        result = prime * result + ((getUid() == null) ? 0 : getUid().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getEditNameCount() == null) ? 0 : getEditNameCount().hashCode());
        result = prime * result + ((getLogo() == null) ? 0 : getLogo().hashCode());
        result = prime * result + ((getQcode() == null) ? 0 : getQcode().hashCode());
        result = prime * result + ((getOpenid() == null) ? 0 : getOpenid().hashCode());
        result = prime * result + ((getQcodeStarttime() == null) ? 0 : getQcodeStarttime().hashCode());
        result = prime * result + ((getSaleCategoryFid() == null) ? 0 : getSaleCategoryFid().hashCode());
        result = prime * result + ((getSaleCategoryId() == null) ? 0 : getSaleCategoryId().hashCode());
        result = prime * result + ((getLinkman() == null) ? 0 : getLinkman().hashCode());
        result = prime * result + ((getTel() == null) ? 0 : getTel().hashCode());
        result = prime * result + ((getIntro() == null) ? 0 : getIntro().hashCode());
        result = prime * result + ((getApprove() == null) ? 0 : getApprove().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        result = prime * result + ((getPublicDisplay() == null) ? 0 : getPublicDisplay().hashCode());
        result = prime * result + ((getDateAdded() == null) ? 0 : getDateAdded().hashCode());
        result = prime * result + ((getServiceTel() == null) ? 0 : getServiceTel().hashCode());
        result = prime * result + ((getServiceQq() == null) ? 0 : getServiceQq().hashCode());
        result = prime * result + ((getServiceWeixin() == null) ? 0 : getServiceWeixin().hashCode());
        result = prime * result + ((getBindWeixin() == null) ? 0 : getBindWeixin().hashCode());
        result = prime * result + ((getWeixinName() == null) ? 0 : getWeixinName().hashCode());
        result = prime * result + ((getWeixinOriginalId() == null) ? 0 : getWeixinOriginalId().hashCode());
        result = prime * result + ((getWeixinId() == null) ? 0 : getWeixinId().hashCode());
        result = prime * result + ((getQq() == null) ? 0 : getQq().hashCode());
        result = prime * result + ((getOpenWeixin() == null) ? 0 : getOpenWeixin().hashCode());
        result = prime * result + ((getBuyerSelffetch() == null) ? 0 : getBuyerSelffetch().hashCode());
        result = prime * result + ((getBuyerSelffetchName() == null) ? 0 : getBuyerSelffetchName().hashCode());
        result = prime * result + ((getPayAgent() == null) ? 0 : getPayAgent().hashCode());
        result = prime * result + ((getOfflinePayment() == null) ? 0 : getOfflinePayment().hashCode());
        result = prime * result + ((getOpenLogistics() == null) ? 0 : getOpenLogistics().hashCode());
        result = prime * result + ((getOpenFriend() == null) ? 0 : getOpenFriend().hashCode());
        result = prime * result + ((getOpenAutoassign() == null) ? 0 : getOpenAutoassign().hashCode());
        result = prime * result + ((getOpenLocalLogistics() == null) ? 0 : getOpenLocalLogistics().hashCode());
        result = prime * result + ((getWarnSpQuantity() == null) ? 0 : getWarnSpQuantity().hashCode());
        result = prime * result + ((getOpenNav() == null) ? 0 : getOpenNav().hashCode());
        result = prime * result + ((getNavStyleId() == null) ? 0 : getNavStyleId().hashCode());
        result = prime * result + ((getUseNavPages() == null) ? 0 : getUseNavPages().hashCode());
        result = prime * result + ((getOpenAd() == null) ? 0 : getOpenAd().hashCode());
        result = prime * result + ((getHasAd() == null) ? 0 : getHasAd().hashCode());
        result = prime * result + ((getAdPosition() == null) ? 0 : getAdPosition().hashCode());
        result = prime * result + ((getUseAdPages() == null) ? 0 : getUseAdPages().hashCode());
        result = prime * result + ((getDateEdited() == null) ? 0 : getDateEdited().hashCode());
        result = prime * result + ((getIncome() == null) ? 0 : getIncome().hashCode());
        result = prime * result + ((getBalance() == null) ? 0 : getBalance().hashCode());
        result = prime * result + ((getUnbalance() == null) ? 0 : getUnbalance().hashCode());
        result = prime * result + ((getWithdrawalAmount() == null) ? 0 : getWithdrawalAmount().hashCode());
        result = prime * result + ((getWithdrawalType() == null) ? 0 : getWithdrawalType().hashCode());
        result = prime * result + ((getBankId() == null) ? 0 : getBankId().hashCode());
        result = prime * result + ((getBankCard() == null) ? 0 : getBankCard().hashCode());
        result = prime * result + ((getBankCardUser() == null) ? 0 : getBankCardUser().hashCode());
        result = prime * result + ((getOpeningBank() == null) ? 0 : getOpeningBank().hashCode());
        result = prime * result + ((getLastEditTime() == null) ? 0 : getLastEditTime().hashCode());
        result = prime * result + ((getPhysicalCount() == null) ? 0 : getPhysicalCount().hashCode());
        result = prime * result + ((getDrpSupplierId() == null) ? 0 : getDrpSupplierId().hashCode());
        result = prime * result + ((getDrpLevel() == null) ? 0 : getDrpLevel().hashCode());
        result = prime * result + ((getCollect() == null) ? 0 : getCollect().hashCode());
        result = prime * result + ((getWxpay() == null) ? 0 : getWxpay().hashCode());
        result = prime * result + ((getOpenDrpApprove() == null) ? 0 : getOpenDrpApprove().hashCode());
        result = prime * result + ((getDrpApprove() == null) ? 0 : getDrpApprove().hashCode());
        result = prime * result + ((getDrpProfit() == null) ? 0 : getDrpProfit().hashCode());
        result = prime * result + ((getDrpProfitWithdrawal() == null) ? 0 : getDrpProfitWithdrawal().hashCode());
        result = prime * result + ((getOpenService() == null) ? 0 : getOpenService().hashCode());
        result = prime * result + ((getAttentionNum() == null) ? 0 : getAttentionNum().hashCode());
        result = prime * result + ((getSourceSiteUrl() == null) ? 0 : getSourceSiteUrl().hashCode());
        result = prime * result + ((getPaymentUrl() == null) ? 0 : getPaymentUrl().hashCode());
        result = prime * result + ((getNotifyUrl() == null) ? 0 : getNotifyUrl().hashCode());
        result = prime * result + ((getOauthUrl() == null) ? 0 : getOauthUrl().hashCode());
        result = prime * result + ((getToken() == null) ? 0 : getToken().hashCode());
        result = prime * result + ((getOpenDrpGuidance() == null) ? 0 : getOpenDrpGuidance().hashCode());
        result = prime * result + ((getOpenDrpLimit() == null) ? 0 : getOpenDrpLimit().hashCode());
        result = prime * result + ((getDrpLimitBuy() == null) ? 0 : getDrpLimitBuy().hashCode());
        result = prime * result + ((getDrpLimitShare() == null) ? 0 : getDrpLimitShare().hashCode());
        result = prime * result + ((getDrpLimitCondition() == null) ? 0 : getDrpLimitCondition().hashCode());
        result = prime * result + ((getPigcmstoken() == null) ? 0 : getPigcmstoken().hashCode());
        result = prime * result + ((getTemplateCatId() == null) ? 0 : getTemplateCatId().hashCode());
        result = prime * result + ((getTemplateId() == null) ? 0 : getTemplateId().hashCode());
        result = prime * result + ((getOpenDrpSettingPrice() == null) ? 0 : getOpenDrpSettingPrice().hashCode());
        result = prime * result + ((getUnifiedPriceSetting() == null) ? 0 : getUnifiedPriceSetting().hashCode());
        result = prime * result + ((getOpenDrpDiyStore() == null) ? 0 : getOpenDrpDiyStore().hashCode());
        result = prime * result + ((getDrpDiyStore() == null) ? 0 : getDrpDiyStore().hashCode());
        result = prime * result + ((getOpenDrpSubscribe() == null) ? 0 : getOpenDrpSubscribe().hashCode());
        result = prime * result + ((getOpenDrpSubscribeAuto() == null) ? 0 : getOpenDrpSubscribeAuto().hashCode());
        result = prime * result + ((getDrpSubscribeTpl() == null) ? 0 : getDrpSubscribeTpl().hashCode());
        result = prime * result + ((getRegDrpSubscribeTpl() == null) ? 0 : getRegDrpSubscribeTpl().hashCode());
        result = prime * result + ((getIsShowDrpTel() == null) ? 0 : getIsShowDrpTel().hashCode());
        result = prime * result + ((getDrpSubscribeImg() == null) ? 0 : getDrpSubscribeImg().hashCode());
        result = prime * result + ((getRegDrpSubscribeImg() == null) ? 0 : getRegDrpSubscribeImg().hashCode());
        result = prime * result + ((getUpdateDrpStoreInfo() == null) ? 0 : getUpdateDrpStoreInfo().hashCode());
        result = prime * result + ((getIsOfficialShop() == null) ? 0 : getIsOfficialShop().hashCode());
        result = prime * result + ((getSettingFansForever() == null) ? 0 : getSettingFansForever().hashCode());
        result = prime * result + ((getIsRequiredToAudit() == null) ? 0 : getIsRequiredToAudit().hashCode());
        result = prime * result + ((getIsRequiredMargin() == null) ? 0 : getIsRequiredMargin().hashCode());
        result = prime * result + ((getBond() == null) ? 0 : getBond().hashCode());
        return result;
    }
}