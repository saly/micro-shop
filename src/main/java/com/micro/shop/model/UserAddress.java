package com.micro.shop.model;

import java.io.Serializable;

public class UserAddress implements Serializable {
    /**
     * 
     * 表 : bestfenxiao_user_address
     * 对应字段 : address_id
     */
    private Integer addressId;

    /**
     * 用户id
     * 表 : bestfenxiao_user_address
     * 对应字段 : uid
     */
    private Integer uid;

    /**
     * 
     * 表 : bestfenxiao_user_address
     * 对应字段 : session_id
     */
    private String sessionId;

    /**
     * 收货人
     * 表 : bestfenxiao_user_address
     * 对应字段 : name
     */
    private String name;

    /**
     * 联系电话
     * 表 : bestfenxiao_user_address
     * 对应字段 : tel
     */
    private String tel;

    /**
     * 省code
     * 表 : bestfenxiao_user_address
     * 对应字段 : province
     */
    private Integer province;

    /**
     * 市code
     * 表 : bestfenxiao_user_address
     * 对应字段 : city
     */
    private Integer city;

    /**
     * 区code
     * 表 : bestfenxiao_user_address
     * 对应字段 : area
     */
    private Integer area;

    /**
     * 详细地址
     * 表 : bestfenxiao_user_address
     * 对应字段 : address
     */
    private String address;

    /**
     * 邮编
     * 表 : bestfenxiao_user_address
     * 对应字段 : zipcode
     */
    private String zipcode;


    /**
     * 
     * 表 : bestfenxiao_user_address
     * 对应字段 : add_time
     */
    private Integer addTime;
    
    private Integer isdefault;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table bestfenxiao_user_address
     *
     * @mbggenerated Sun Apr 22 13:38:31 CST 2018
     */
    private static final long serialVersionUID = 1L;

    /**
     * get method 
     *
     * @return bestfenxiao_user_address.address_id：
     */
    public Integer getAddressId() {
        return addressId;
    }

    /**
     * set method 
     *
     * @param addressId  
     */
    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_user_address.uid：用户id
     */
    public Integer getUid() {
        return uid;
    }

    /**
     * set method 
     *
     * @param uid  用户id
     */
    public void setUid(Integer uid) {
        this.uid = uid;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_user_address.session_id：
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * set method 
     *
     * @param sessionId  
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId == null ? null : sessionId.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_user_address.name：收货人
     */
    public String getName() {
        return name;
    }

    /**
     * set method 
     *
     * @param name  收货人
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_user_address.tel：联系电话
     */
    public String getTel() {
        return tel;
    }

    /**
     * set method 
     *
     * @param tel  联系电话
     */
    public void setTel(String tel) {
        this.tel = tel == null ? null : tel.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_user_address.province：省code
     */
    public Integer getProvince() {
        return province;
    }

    /**
     * set method 
     *
     * @param province  省code
     */
    public void setProvince(Integer province) {
        this.province = province;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_user_address.city：市code
     */
    public Integer getCity() {
        return city;
    }

    /**
     * set method 
     *
     * @param city  市code
     */
    public void setCity(Integer city) {
        this.city = city;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_user_address.area：区code
     */
    public Integer getArea() {
        return area;
    }

    /**
     * set method 
     *
     * @param area  区code
     */
    public void setArea(Integer area) {
        this.area = area;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_user_address.address：详细地址
     */
    public String getAddress() {
        return address;
    }

    /**
     * set method 
     *
     * @param address  详细地址
     */
    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_user_address.zipcode：邮编
     */
    public String getZipcode() {
        return zipcode;
    }

    /**
     * set method 
     *
     * @param zipcode  邮编
     */
    public void setZipcode(String zipcode) {
        this.zipcode = zipcode == null ? null : zipcode.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_user_address.add_time：
     */
    public Integer getAddTime() {
        return addTime;
    }

    /**
     * set method 
     *
     * @param addTime  
     */
    public void setAddTime(Integer addTime) {
        this.addTime = addTime;
    }
    
    

    public Integer getIsdefault() {
		return isdefault;
	}

	public void setIsdefault(Integer isdefault) {
		this.isdefault = isdefault;
	}

	/**
     *
     * @param that
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        UserAddress other = (UserAddress) that;
        return (this.getAddressId() == null ? other.getAddressId() == null : this.getAddressId().equals(other.getAddressId()))
            && (this.getUid() == null ? other.getUid() == null : this.getUid().equals(other.getUid()))
            && (this.getSessionId() == null ? other.getSessionId() == null : this.getSessionId().equals(other.getSessionId()))
            && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
            && (this.getTel() == null ? other.getTel() == null : this.getTel().equals(other.getTel()))
            && (this.getProvince() == null ? other.getProvince() == null : this.getProvince().equals(other.getProvince()))
            && (this.getCity() == null ? other.getCity() == null : this.getCity().equals(other.getCity()))
            && (this.getArea() == null ? other.getArea() == null : this.getArea().equals(other.getArea()))
            && (this.getAddress() == null ? other.getAddress() == null : this.getAddress().equals(other.getAddress()))
            && (this.getZipcode() == null ? other.getZipcode() == null : this.getZipcode().equals(other.getZipcode()))
            && (this.getAddTime() == null ? other.getAddTime() == null : this.getAddTime().equals(other.getAddTime()));
    }

    /**
     *
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getAddressId() == null) ? 0 : getAddressId().hashCode());
        result = prime * result + ((getUid() == null) ? 0 : getUid().hashCode());
        result = prime * result + ((getSessionId() == null) ? 0 : getSessionId().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getTel() == null) ? 0 : getTel().hashCode());
        result = prime * result + ((getProvince() == null) ? 0 : getProvince().hashCode());
        result = prime * result + ((getCity() == null) ? 0 : getCity().hashCode());
        result = prime * result + ((getArea() == null) ? 0 : getArea().hashCode());
        result = prime * result + ((getAddress() == null) ? 0 : getAddress().hashCode());
        result = prime * result + ((getZipcode() == null) ? 0 : getZipcode().hashCode());
        result = prime * result + ((getAddTime() == null) ? 0 : getAddTime().hashCode());
        return result;
    }
}