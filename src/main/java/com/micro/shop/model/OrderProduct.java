package com.micro.shop.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class OrderProduct implements Serializable {
    /**
     * 订单商品id
     * 表 : bestfenxiao_order_product
     * 对应字段 : bestfenxiao_id
     */
    private Integer bestfenxiaoId;

    /**
     * 
     * 表 : bestfenxiao_order_product
     * 对应字段 : order_id
     */
    private Integer orderId;

    /**
     * 
     * 表 : bestfenxiao_order_product
     * 对应字段 : product_id
     */
    private Integer productId;

    /**
     * 
     * 表 : bestfenxiao_order_product
     * 对应字段 : sku_id
     */
    private Integer skuId;

    /**
     * 
     * 表 : bestfenxiao_order_product
     * 对应字段 : pro_num
     */
    private Integer proNum;

    /**
     * 
     * 表 : bestfenxiao_order_product
     * 对应字段 : pro_price
     */
    private BigDecimal proPrice;

    /**
     * 
     * 表 : bestfenxiao_order_product
     * 对应字段 : pro_weight
     */
    private Float proWeight;

    /**
     * 
     * 表 : bestfenxiao_order_product
     * 对应字段 : is_packaged
     */
    private Integer isPackaged;

    /**
     * 
     * 表 : bestfenxiao_order_product
     * 对应字段 : in_package_status
     */
    private Integer inPackageStatus;

    /**
     * 
     * 表 : bestfenxiao_order_product
     * 对应字段 : is_fx
     */
    private Integer isFx;

    /**
     * 
     * 表 : bestfenxiao_order_product
     * 对应字段 : supplier_id
     */
    private Integer supplierId;

    /**
     * 
     * 表 : bestfenxiao_order_product
     * 对应字段 : original_product_id
     */
    private Integer originalProductId;

    /**
     * 
     * 表 : bestfenxiao_order_product
     * 对应字段 : user_order_id
     */
    private Integer userOrderId;

    /**
     * 
     * 表 : bestfenxiao_order_product
     * 对应字段 : is_comment
     */
    private Integer isComment;

    /**
     * 
     * 表 : bestfenxiao_order_product
     * 对应字段 : return_status
     */
    private Integer returnStatus;

    /**
     * 
     * 表 : bestfenxiao_order_product
     * 对应字段 : rights_status
     */
    private Integer rightsStatus;

    /**
     * 
     * 表 : bestfenxiao_order_product
     * 对应字段 : is_present
     */
    private Integer isPresent;

    /**
     * 
     * 表 : bestfenxiao_order_product
     * 对应字段 : sp_id
     */
    private Integer spId;

    /**
     * 
     * 表 : bestfenxiao_order_product
     * 对应字段 : profit
     */
    private Long profit;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table bestfenxiao_order_product
     *
     * @mbggenerated Mon Apr 23 22:23:43 CST 2018
     */
    private static final long serialVersionUID = 1L;

    /**
     * get method 
     *
     * @return bestfenxiao_order_product.bestfenxiao_id：订单商品id
     */
    public Integer getBestfenxiaoId() {
        return bestfenxiaoId;
    }

    /**
     * set method 
     *
     * @param bestfenxiaoId  订单商品id
     */
    public void setBestfenxiaoId(Integer bestfenxiaoId) {
        this.bestfenxiaoId = bestfenxiaoId;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order_product.order_id：
     */
    public Integer getOrderId() {
        return orderId;
    }

    /**
     * set method 
     *
     * @param orderId  
     */
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order_product.product_id：
     */
    public Integer getProductId() {
        return productId;
    }

    /**
     * set method 
     *
     * @param productId  
     */
    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order_product.sku_id：
     */
    public Integer getSkuId() {
        return skuId;
    }

    /**
     * set method 
     *
     * @param skuId  
     */
    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order_product.pro_num：
     */
    public Integer getProNum() {
        return proNum;
    }

    /**
     * set method 
     *
     * @param proNum  
     */
    public void setProNum(Integer proNum) {
        this.proNum = proNum;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order_product.pro_price：
     */
    public BigDecimal getProPrice() {
        return proPrice;
    }

    /**
     * set method 
     *
     * @param proPrice  
     */
    public void setProPrice(BigDecimal proPrice) {
        this.proPrice = proPrice;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order_product.pro_weight：
     */
    public Float getProWeight() {
        return proWeight;
    }

    /**
     * set method 
     *
     * @param proWeight  
     */
    public void setProWeight(Float proWeight) {
        this.proWeight = proWeight;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order_product.is_packaged：
     */
    public Integer getIsPackaged() {
        return isPackaged;
    }

    /**
     * set method 
     *
     * @param isPackaged  
     */
    public void setIsPackaged(Integer isPackaged) {
        this.isPackaged = isPackaged;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order_product.in_package_status：
     */
    public Integer getInPackageStatus() {
        return inPackageStatus;
    }

    /**
     * set method 
     *
     * @param inPackageStatus  
     */
    public void setInPackageStatus(Integer inPackageStatus) {
        this.inPackageStatus = inPackageStatus;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order_product.is_fx：
     */
    public Integer getIsFx() {
        return isFx;
    }

    /**
     * set method 
     *
     * @param isFx  
     */
    public void setIsFx(Integer isFx) {
        this.isFx = isFx;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order_product.supplier_id：
     */
    public Integer getSupplierId() {
        return supplierId;
    }

    /**
     * set method 
     *
     * @param supplierId  
     */
    public void setSupplierId(Integer supplierId) {
        this.supplierId = supplierId;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order_product.original_product_id：
     */
    public Integer getOriginalProductId() {
        return originalProductId;
    }

    /**
     * set method 
     *
     * @param originalProductId  
     */
    public void setOriginalProductId(Integer originalProductId) {
        this.originalProductId = originalProductId;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order_product.user_order_id：
     */
    public Integer getUserOrderId() {
        return userOrderId;
    }

    /**
     * set method 
     *
     * @param userOrderId  
     */
    public void setUserOrderId(Integer userOrderId) {
        this.userOrderId = userOrderId;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order_product.is_comment：
     */
    public Integer getIsComment() {
        return isComment;
    }

    /**
     * set method 
     *
     * @param isComment  
     */
    public void setIsComment(Integer isComment) {
        this.isComment = isComment;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order_product.return_status：
     */
    public Integer getReturnStatus() {
        return returnStatus;
    }

    /**
     * set method 
     *
     * @param returnStatus  
     */
    public void setReturnStatus(Integer returnStatus) {
        this.returnStatus = returnStatus;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order_product.rights_status：
     */
    public Integer getRightsStatus() {
        return rightsStatus;
    }

    /**
     * set method 
     *
     * @param rightsStatus  
     */
    public void setRightsStatus(Integer rightsStatus) {
        this.rightsStatus = rightsStatus;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order_product.is_present：
     */
    public Integer getIsPresent() {
        return isPresent;
    }

    /**
     * set method 
     *
     * @param isPresent  
     */
    public void setIsPresent(Integer isPresent) {
        this.isPresent = isPresent;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order_product.sp_id：
     */
    public Integer getSpId() {
        return spId;
    }

    /**
     * set method 
     *
     * @param spId  
     */
    public void setSpId(Integer spId) {
        this.spId = spId;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order_product.profit：
     */
    public Long getProfit() {
        return profit;
    }

    /**
     * set method 
     *
     * @param profit  
     */
    public void setProfit(Long profit) {
        this.profit = profit;
    }

    /**
     *
     * @param that
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        OrderProduct other = (OrderProduct) that;
        return (this.getBestfenxiaoId() == null ? other.getBestfenxiaoId() == null : this.getBestfenxiaoId().equals(other.getBestfenxiaoId()))
            && (this.getOrderId() == null ? other.getOrderId() == null : this.getOrderId().equals(other.getOrderId()))
            && (this.getProductId() == null ? other.getProductId() == null : this.getProductId().equals(other.getProductId()))
            && (this.getSkuId() == null ? other.getSkuId() == null : this.getSkuId().equals(other.getSkuId()))
            && (this.getProNum() == null ? other.getProNum() == null : this.getProNum().equals(other.getProNum()))
            && (this.getProPrice() == null ? other.getProPrice() == null : this.getProPrice().equals(other.getProPrice()))
            && (this.getProWeight() == null ? other.getProWeight() == null : this.getProWeight().equals(other.getProWeight()))
            && (this.getIsPackaged() == null ? other.getIsPackaged() == null : this.getIsPackaged().equals(other.getIsPackaged()))
            && (this.getInPackageStatus() == null ? other.getInPackageStatus() == null : this.getInPackageStatus().equals(other.getInPackageStatus()))
            && (this.getIsFx() == null ? other.getIsFx() == null : this.getIsFx().equals(other.getIsFx()))
            && (this.getSupplierId() == null ? other.getSupplierId() == null : this.getSupplierId().equals(other.getSupplierId()))
            && (this.getOriginalProductId() == null ? other.getOriginalProductId() == null : this.getOriginalProductId().equals(other.getOriginalProductId()))
            && (this.getUserOrderId() == null ? other.getUserOrderId() == null : this.getUserOrderId().equals(other.getUserOrderId()))
            && (this.getIsComment() == null ? other.getIsComment() == null : this.getIsComment().equals(other.getIsComment()))
            && (this.getReturnStatus() == null ? other.getReturnStatus() == null : this.getReturnStatus().equals(other.getReturnStatus()))
            && (this.getRightsStatus() == null ? other.getRightsStatus() == null : this.getRightsStatus().equals(other.getRightsStatus()))
            && (this.getIsPresent() == null ? other.getIsPresent() == null : this.getIsPresent().equals(other.getIsPresent()))
            && (this.getSpId() == null ? other.getSpId() == null : this.getSpId().equals(other.getSpId()))
            && (this.getProfit() == null ? other.getProfit() == null : this.getProfit().equals(other.getProfit()));
    }

    /**
     *
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getBestfenxiaoId() == null) ? 0 : getBestfenxiaoId().hashCode());
        result = prime * result + ((getOrderId() == null) ? 0 : getOrderId().hashCode());
        result = prime * result + ((getProductId() == null) ? 0 : getProductId().hashCode());
        result = prime * result + ((getSkuId() == null) ? 0 : getSkuId().hashCode());
        result = prime * result + ((getProNum() == null) ? 0 : getProNum().hashCode());
        result = prime * result + ((getProPrice() == null) ? 0 : getProPrice().hashCode());
        result = prime * result + ((getProWeight() == null) ? 0 : getProWeight().hashCode());
        result = prime * result + ((getIsPackaged() == null) ? 0 : getIsPackaged().hashCode());
        result = prime * result + ((getInPackageStatus() == null) ? 0 : getInPackageStatus().hashCode());
        result = prime * result + ((getIsFx() == null) ? 0 : getIsFx().hashCode());
        result = prime * result + ((getSupplierId() == null) ? 0 : getSupplierId().hashCode());
        result = prime * result + ((getOriginalProductId() == null) ? 0 : getOriginalProductId().hashCode());
        result = prime * result + ((getUserOrderId() == null) ? 0 : getUserOrderId().hashCode());
        result = prime * result + ((getIsComment() == null) ? 0 : getIsComment().hashCode());
        result = prime * result + ((getReturnStatus() == null) ? 0 : getReturnStatus().hashCode());
        result = prime * result + ((getRightsStatus() == null) ? 0 : getRightsStatus().hashCode());
        result = prime * result + ((getIsPresent() == null) ? 0 : getIsPresent().hashCode());
        result = prime * result + ((getSpId() == null) ? 0 : getSpId().hashCode());
        result = prime * result + ((getProfit() == null) ? 0 : getProfit().hashCode());
        return result;
    }
}