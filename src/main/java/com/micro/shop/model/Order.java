package com.micro.shop.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class Order implements Serializable {
    /**
     * 订单id
     * 表 : bestfenxiao_order
     * 对应字段 : order_id
     */
    private Integer orderId;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : store_id
     */
    private Integer storeId;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : order_no
     */
    private String orderNo;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : trade_no
     */
    private String tradeNo;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : pay_type
     */
    private String payType;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : third_id
     */
    private String thirdId;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : uid
     */
    private Integer uid;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : session_id
     */
    private String sessionId;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : postage
     */
    private BigDecimal postage;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : sub_total
     */
    private BigDecimal subTotal;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : total
     */
    private BigDecimal total;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : pro_count
     */
    private Integer proCount;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : pro_num
     */
    private Integer proNum;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : address_user
     */
    private String addressUser;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : address_tel
     */
    private String addressTel;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : payment_method
     */
    private String paymentMethod;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : peerpay_type
     */
    private Integer peerpayType;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : peerpay_content
     */
    private String peerpayContent;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : shipping_method
     */
    private String shippingMethod;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : type
     */
    private Integer type;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : status
     */
    private Integer status;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : add_time
     */
    private Integer addTime;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : paid_time
     */
    private Integer paidTime;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : sent_time
     */
    private Integer sentTime;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : delivery_time
     */
    private Integer deliveryTime;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : cancel_time
     */
    private Integer cancelTime;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : complate_time
     */
    private Integer complateTime;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : refund_time
     */
    private Integer refundTime;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : comment
     */
    private String comment;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : bak
     */
    private String bak;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : star
     */
    private Integer star;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : pay_money
     */
    private BigDecimal payMoney;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : cancel_method
     */
    private Integer cancelMethod;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : float_amount
     */
    private BigDecimal floatAmount;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : is_fx
     */
    private Integer isFx;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : fx_order_id
     */
    private Integer fxOrderId;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : user_order_id
     */
    private Integer userOrderId;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : suppliers
     */
    private String suppliers;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : packaging
     */
    private Integer packaging;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : fx_postage
     */
    private String fxPostage;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : useStorePay
     */
    private Integer usestorepay;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : storeOpenid
     */
    private String storeopenid;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : sales_ratio
     */
    private BigDecimal salesRatio;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : is_check
     */
    private Integer isCheck;

    /**
     * 
     * 表 : bestfenxiao_order
     * 对应字段 : is_assigned
     */
    private Integer isAssigned;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table bestfenxiao_order
     *
     * @mbggenerated Mon Apr 23 22:23:43 CST 2018
     */
    private static final long serialVersionUID = 1L;

    /**
     * get method 
     *
     * @return bestfenxiao_order.order_id：订单id
     */
    public Integer getOrderId() {
        return orderId;
    }

    /**
     * set method 
     *
     * @param orderId  订单id
     */
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.store_id：
     */
    public Integer getStoreId() {
        return storeId;
    }

    /**
     * set method 
     *
     * @param storeId  
     */
    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.order_no：
     */
    public String getOrderNo() {
        return orderNo;
    }

    /**
     * set method 
     *
     * @param orderNo  
     */
    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo == null ? null : orderNo.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.trade_no：
     */
    public String getTradeNo() {
        return tradeNo;
    }

    /**
     * set method 
     *
     * @param tradeNo  
     */
    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo == null ? null : tradeNo.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.pay_type：
     */
    public String getPayType() {
        return payType;
    }

    /**
     * set method 
     *
     * @param payType  
     */
    public void setPayType(String payType) {
        this.payType = payType == null ? null : payType.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.third_id：
     */
    public String getThirdId() {
        return thirdId;
    }

    /**
     * set method 
     *
     * @param thirdId  
     */
    public void setThirdId(String thirdId) {
        this.thirdId = thirdId == null ? null : thirdId.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.uid：
     */
    public Integer getUid() {
        return uid;
    }

    /**
     * set method 
     *
     * @param uid  
     */
    public void setUid(Integer uid) {
        this.uid = uid;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.session_id：
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * set method 
     *
     * @param sessionId  
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId == null ? null : sessionId.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.postage：
     */
    public BigDecimal getPostage() {
        return postage;
    }

    /**
     * set method 
     *
     * @param postage  
     */
    public void setPostage(BigDecimal postage) {
        this.postage = postage;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.sub_total：
     */
    public BigDecimal getSubTotal() {
        return subTotal;
    }

    /**
     * set method 
     *
     * @param subTotal  
     */
    public void setSubTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.total：
     */
    public BigDecimal getTotal() {
        return total;
    }

    /**
     * set method 
     *
     * @param total  
     */
    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.pro_count：
     */
    public Integer getProCount() {
        return proCount;
    }

    /**
     * set method 
     *
     * @param proCount  
     */
    public void setProCount(Integer proCount) {
        this.proCount = proCount;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.pro_num：
     */
    public Integer getProNum() {
        return proNum;
    }

    /**
     * set method 
     *
     * @param proNum  
     */
    public void setProNum(Integer proNum) {
        this.proNum = proNum;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.address_user：
     */
    public String getAddressUser() {
        return addressUser;
    }

    /**
     * set method 
     *
     * @param addressUser  
     */
    public void setAddressUser(String addressUser) {
        this.addressUser = addressUser == null ? null : addressUser.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.address_tel：
     */
    public String getAddressTel() {
        return addressTel;
    }

    /**
     * set method 
     *
     * @param addressTel  
     */
    public void setAddressTel(String addressTel) {
        this.addressTel = addressTel == null ? null : addressTel.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.payment_method：
     */
    public String getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * set method 
     *
     * @param paymentMethod  
     */
    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod == null ? null : paymentMethod.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.peerpay_type：
     */
    public Integer getPeerpayType() {
        return peerpayType;
    }

    /**
     * set method 
     *
     * @param peerpayType  
     */
    public void setPeerpayType(Integer peerpayType) {
        this.peerpayType = peerpayType;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.peerpay_content：
     */
    public String getPeerpayContent() {
        return peerpayContent;
    }

    /**
     * set method 
     *
     * @param peerpayContent  
     */
    public void setPeerpayContent(String peerpayContent) {
        this.peerpayContent = peerpayContent == null ? null : peerpayContent.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.shipping_method：
     */
    public String getShippingMethod() {
        return shippingMethod;
    }

    /**
     * set method 
     *
     * @param shippingMethod  
     */
    public void setShippingMethod(String shippingMethod) {
        this.shippingMethod = shippingMethod == null ? null : shippingMethod.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.type：
     */
    public Integer getType() {
        return type;
    }

    /**
     * set method 
     *
     * @param type  
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.status：
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * set method 
     *
     * @param status  
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.add_time：
     */
    public Integer getAddTime() {
        return addTime;
    }

    /**
     * set method 
     *
     * @param addTime  
     */
    public void setAddTime(Integer addTime) {
        this.addTime = addTime;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.paid_time：
     */
    public Integer getPaidTime() {
        return paidTime;
    }

    /**
     * set method 
     *
     * @param paidTime  
     */
    public void setPaidTime(Integer paidTime) {
        this.paidTime = paidTime;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.sent_time：
     */
    public Integer getSentTime() {
        return sentTime;
    }

    /**
     * set method 
     *
     * @param sentTime  
     */
    public void setSentTime(Integer sentTime) {
        this.sentTime = sentTime;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.delivery_time：
     */
    public Integer getDeliveryTime() {
        return deliveryTime;
    }

    /**
     * set method 
     *
     * @param deliveryTime  
     */
    public void setDeliveryTime(Integer deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.cancel_time：
     */
    public Integer getCancelTime() {
        return cancelTime;
    }

    /**
     * set method 
     *
     * @param cancelTime  
     */
    public void setCancelTime(Integer cancelTime) {
        this.cancelTime = cancelTime;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.complate_time：
     */
    public Integer getComplateTime() {
        return complateTime;
    }

    /**
     * set method 
     *
     * @param complateTime  
     */
    public void setComplateTime(Integer complateTime) {
        this.complateTime = complateTime;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.refund_time：
     */
    public Integer getRefundTime() {
        return refundTime;
    }

    /**
     * set method 
     *
     * @param refundTime  
     */
    public void setRefundTime(Integer refundTime) {
        this.refundTime = refundTime;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.comment：
     */
    public String getComment() {
        return comment;
    }

    /**
     * set method 
     *
     * @param comment  
     */
    public void setComment(String comment) {
        this.comment = comment == null ? null : comment.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.bak：
     */
    public String getBak() {
        return bak;
    }

    /**
     * set method 
     *
     * @param bak  
     */
    public void setBak(String bak) {
        this.bak = bak == null ? null : bak.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.star：
     */
    public Integer getStar() {
        return star;
    }

    /**
     * set method 
     *
     * @param star  
     */
    public void setStar(Integer star) {
        this.star = star;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.pay_money：
     */
    public BigDecimal getPayMoney() {
        return payMoney;
    }

    /**
     * set method 
     *
     * @param payMoney  
     */
    public void setPayMoney(BigDecimal payMoney) {
        this.payMoney = payMoney;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.cancel_method：
     */
    public Integer getCancelMethod() {
        return cancelMethod;
    }

    /**
     * set method 
     *
     * @param cancelMethod  
     */
    public void setCancelMethod(Integer cancelMethod) {
        this.cancelMethod = cancelMethod;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.float_amount：
     */
    public BigDecimal getFloatAmount() {
        return floatAmount;
    }

    /**
     * set method 
     *
     * @param floatAmount  
     */
    public void setFloatAmount(BigDecimal floatAmount) {
        this.floatAmount = floatAmount;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.is_fx：
     */
    public Integer getIsFx() {
        return isFx;
    }

    /**
     * set method 
     *
     * @param isFx  
     */
    public void setIsFx(Integer isFx) {
        this.isFx = isFx;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.fx_order_id：
     */
    public Integer getFxOrderId() {
        return fxOrderId;
    }

    /**
     * set method 
     *
     * @param fxOrderId  
     */
    public void setFxOrderId(Integer fxOrderId) {
        this.fxOrderId = fxOrderId;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.user_order_id：
     */
    public Integer getUserOrderId() {
        return userOrderId;
    }

    /**
     * set method 
     *
     * @param userOrderId  
     */
    public void setUserOrderId(Integer userOrderId) {
        this.userOrderId = userOrderId;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.suppliers：
     */
    public String getSuppliers() {
        return suppliers;
    }

    /**
     * set method 
     *
     * @param suppliers  
     */
    public void setSuppliers(String suppliers) {
        this.suppliers = suppliers == null ? null : suppliers.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.packaging：
     */
    public Integer getPackaging() {
        return packaging;
    }

    /**
     * set method 
     *
     * @param packaging  
     */
    public void setPackaging(Integer packaging) {
        this.packaging = packaging;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.fx_postage：
     */
    public String getFxPostage() {
        return fxPostage;
    }

    /**
     * set method 
     *
     * @param fxPostage  
     */
    public void setFxPostage(String fxPostage) {
        this.fxPostage = fxPostage == null ? null : fxPostage.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.useStorePay：
     */
    public Integer getUsestorepay() {
        return usestorepay;
    }

    /**
     * set method 
     *
     * @param usestorepay  
     */
    public void setUsestorepay(Integer usestorepay) {
        this.usestorepay = usestorepay;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.storeOpenid：
     */
    public String getStoreopenid() {
        return storeopenid;
    }

    /**
     * set method 
     *
     * @param storeopenid  
     */
    public void setStoreopenid(String storeopenid) {
        this.storeopenid = storeopenid == null ? null : storeopenid.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.sales_ratio：
     */
    public BigDecimal getSalesRatio() {
        return salesRatio;
    }

    /**
     * set method 
     *
     * @param salesRatio  
     */
    public void setSalesRatio(BigDecimal salesRatio) {
        this.salesRatio = salesRatio;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.is_check：
     */
    public Integer getIsCheck() {
        return isCheck;
    }

    /**
     * set method 
     *
     * @param isCheck  
     */
    public void setIsCheck(Integer isCheck) {
        this.isCheck = isCheck;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_order.is_assigned：
     */
    public Integer getIsAssigned() {
        return isAssigned;
    }

    /**
     * set method 
     *
     * @param isAssigned  
     */
    public void setIsAssigned(Integer isAssigned) {
        this.isAssigned = isAssigned;
    }

    /**
     *
     * @param that
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Order other = (Order) that;
        return (this.getOrderId() == null ? other.getOrderId() == null : this.getOrderId().equals(other.getOrderId()))
            && (this.getStoreId() == null ? other.getStoreId() == null : this.getStoreId().equals(other.getStoreId()))
            && (this.getOrderNo() == null ? other.getOrderNo() == null : this.getOrderNo().equals(other.getOrderNo()))
            && (this.getTradeNo() == null ? other.getTradeNo() == null : this.getTradeNo().equals(other.getTradeNo()))
            && (this.getPayType() == null ? other.getPayType() == null : this.getPayType().equals(other.getPayType()))
            && (this.getThirdId() == null ? other.getThirdId() == null : this.getThirdId().equals(other.getThirdId()))
            && (this.getUid() == null ? other.getUid() == null : this.getUid().equals(other.getUid()))
            && (this.getSessionId() == null ? other.getSessionId() == null : this.getSessionId().equals(other.getSessionId()))
            && (this.getPostage() == null ? other.getPostage() == null : this.getPostage().equals(other.getPostage()))
            && (this.getSubTotal() == null ? other.getSubTotal() == null : this.getSubTotal().equals(other.getSubTotal()))
            && (this.getTotal() == null ? other.getTotal() == null : this.getTotal().equals(other.getTotal()))
            && (this.getProCount() == null ? other.getProCount() == null : this.getProCount().equals(other.getProCount()))
            && (this.getProNum() == null ? other.getProNum() == null : this.getProNum().equals(other.getProNum()))
            && (this.getAddressUser() == null ? other.getAddressUser() == null : this.getAddressUser().equals(other.getAddressUser()))
            && (this.getAddressTel() == null ? other.getAddressTel() == null : this.getAddressTel().equals(other.getAddressTel()))
            && (this.getPaymentMethod() == null ? other.getPaymentMethod() == null : this.getPaymentMethod().equals(other.getPaymentMethod()))
            && (this.getPeerpayType() == null ? other.getPeerpayType() == null : this.getPeerpayType().equals(other.getPeerpayType()))
            && (this.getPeerpayContent() == null ? other.getPeerpayContent() == null : this.getPeerpayContent().equals(other.getPeerpayContent()))
            && (this.getShippingMethod() == null ? other.getShippingMethod() == null : this.getShippingMethod().equals(other.getShippingMethod()))
            && (this.getType() == null ? other.getType() == null : this.getType().equals(other.getType()))
            && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()))
            && (this.getAddTime() == null ? other.getAddTime() == null : this.getAddTime().equals(other.getAddTime()))
            && (this.getPaidTime() == null ? other.getPaidTime() == null : this.getPaidTime().equals(other.getPaidTime()))
            && (this.getSentTime() == null ? other.getSentTime() == null : this.getSentTime().equals(other.getSentTime()))
            && (this.getDeliveryTime() == null ? other.getDeliveryTime() == null : this.getDeliveryTime().equals(other.getDeliveryTime()))
            && (this.getCancelTime() == null ? other.getCancelTime() == null : this.getCancelTime().equals(other.getCancelTime()))
            && (this.getComplateTime() == null ? other.getComplateTime() == null : this.getComplateTime().equals(other.getComplateTime()))
            && (this.getRefundTime() == null ? other.getRefundTime() == null : this.getRefundTime().equals(other.getRefundTime()))
            && (this.getComment() == null ? other.getComment() == null : this.getComment().equals(other.getComment()))
            && (this.getBak() == null ? other.getBak() == null : this.getBak().equals(other.getBak()))
            && (this.getStar() == null ? other.getStar() == null : this.getStar().equals(other.getStar()))
            && (this.getPayMoney() == null ? other.getPayMoney() == null : this.getPayMoney().equals(other.getPayMoney()))
            && (this.getCancelMethod() == null ? other.getCancelMethod() == null : this.getCancelMethod().equals(other.getCancelMethod()))
            && (this.getFloatAmount() == null ? other.getFloatAmount() == null : this.getFloatAmount().equals(other.getFloatAmount()))
            && (this.getIsFx() == null ? other.getIsFx() == null : this.getIsFx().equals(other.getIsFx()))
            && (this.getFxOrderId() == null ? other.getFxOrderId() == null : this.getFxOrderId().equals(other.getFxOrderId()))
            && (this.getUserOrderId() == null ? other.getUserOrderId() == null : this.getUserOrderId().equals(other.getUserOrderId()))
            && (this.getSuppliers() == null ? other.getSuppliers() == null : this.getSuppliers().equals(other.getSuppliers()))
            && (this.getPackaging() == null ? other.getPackaging() == null : this.getPackaging().equals(other.getPackaging()))
            && (this.getFxPostage() == null ? other.getFxPostage() == null : this.getFxPostage().equals(other.getFxPostage()))
            && (this.getUsestorepay() == null ? other.getUsestorepay() == null : this.getUsestorepay().equals(other.getUsestorepay()))
            && (this.getStoreopenid() == null ? other.getStoreopenid() == null : this.getStoreopenid().equals(other.getStoreopenid()))
            && (this.getSalesRatio() == null ? other.getSalesRatio() == null : this.getSalesRatio().equals(other.getSalesRatio()))
            && (this.getIsCheck() == null ? other.getIsCheck() == null : this.getIsCheck().equals(other.getIsCheck()))
            && (this.getIsAssigned() == null ? other.getIsAssigned() == null : this.getIsAssigned().equals(other.getIsAssigned()));
    }

    /**
     *
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getOrderId() == null) ? 0 : getOrderId().hashCode());
        result = prime * result + ((getStoreId() == null) ? 0 : getStoreId().hashCode());
        result = prime * result + ((getOrderNo() == null) ? 0 : getOrderNo().hashCode());
        result = prime * result + ((getTradeNo() == null) ? 0 : getTradeNo().hashCode());
        result = prime * result + ((getPayType() == null) ? 0 : getPayType().hashCode());
        result = prime * result + ((getThirdId() == null) ? 0 : getThirdId().hashCode());
        result = prime * result + ((getUid() == null) ? 0 : getUid().hashCode());
        result = prime * result + ((getSessionId() == null) ? 0 : getSessionId().hashCode());
        result = prime * result + ((getPostage() == null) ? 0 : getPostage().hashCode());
        result = prime * result + ((getSubTotal() == null) ? 0 : getSubTotal().hashCode());
        result = prime * result + ((getTotal() == null) ? 0 : getTotal().hashCode());
        result = prime * result + ((getProCount() == null) ? 0 : getProCount().hashCode());
        result = prime * result + ((getProNum() == null) ? 0 : getProNum().hashCode());
        result = prime * result + ((getAddressUser() == null) ? 0 : getAddressUser().hashCode());
        result = prime * result + ((getAddressTel() == null) ? 0 : getAddressTel().hashCode());
        result = prime * result + ((getPaymentMethod() == null) ? 0 : getPaymentMethod().hashCode());
        result = prime * result + ((getPeerpayType() == null) ? 0 : getPeerpayType().hashCode());
        result = prime * result + ((getPeerpayContent() == null) ? 0 : getPeerpayContent().hashCode());
        result = prime * result + ((getShippingMethod() == null) ? 0 : getShippingMethod().hashCode());
        result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        result = prime * result + ((getAddTime() == null) ? 0 : getAddTime().hashCode());
        result = prime * result + ((getPaidTime() == null) ? 0 : getPaidTime().hashCode());
        result = prime * result + ((getSentTime() == null) ? 0 : getSentTime().hashCode());
        result = prime * result + ((getDeliveryTime() == null) ? 0 : getDeliveryTime().hashCode());
        result = prime * result + ((getCancelTime() == null) ? 0 : getCancelTime().hashCode());
        result = prime * result + ((getComplateTime() == null) ? 0 : getComplateTime().hashCode());
        result = prime * result + ((getRefundTime() == null) ? 0 : getRefundTime().hashCode());
        result = prime * result + ((getComment() == null) ? 0 : getComment().hashCode());
        result = prime * result + ((getBak() == null) ? 0 : getBak().hashCode());
        result = prime * result + ((getStar() == null) ? 0 : getStar().hashCode());
        result = prime * result + ((getPayMoney() == null) ? 0 : getPayMoney().hashCode());
        result = prime * result + ((getCancelMethod() == null) ? 0 : getCancelMethod().hashCode());
        result = prime * result + ((getFloatAmount() == null) ? 0 : getFloatAmount().hashCode());
        result = prime * result + ((getIsFx() == null) ? 0 : getIsFx().hashCode());
        result = prime * result + ((getFxOrderId() == null) ? 0 : getFxOrderId().hashCode());
        result = prime * result + ((getUserOrderId() == null) ? 0 : getUserOrderId().hashCode());
        result = prime * result + ((getSuppliers() == null) ? 0 : getSuppliers().hashCode());
        result = prime * result + ((getPackaging() == null) ? 0 : getPackaging().hashCode());
        result = prime * result + ((getFxPostage() == null) ? 0 : getFxPostage().hashCode());
        result = prime * result + ((getUsestorepay() == null) ? 0 : getUsestorepay().hashCode());
        result = prime * result + ((getStoreopenid() == null) ? 0 : getStoreopenid().hashCode());
        result = prime * result + ((getSalesRatio() == null) ? 0 : getSalesRatio().hashCode());
        result = prime * result + ((getIsCheck() == null) ? 0 : getIsCheck().hashCode());
        result = prime * result + ((getIsAssigned() == null) ? 0 : getIsAssigned().hashCode());
        return result;
    }
}