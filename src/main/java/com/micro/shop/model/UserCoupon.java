package com.micro.shop.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class UserCoupon implements Serializable {
    /**
     * 
     * 表 : bestfenxiao_user_coupon
     * 对应字段 : id
     */
    private Integer id;

    /**
     * 用户id
     * 表 : bestfenxiao_user_coupon
     * 对应字段 : uid
     */
    private Integer uid;

    /**
     * 商铺id
     * 表 : bestfenxiao_user_coupon
     * 对应字段 : store_id
     */
    private Integer storeId;

    /**
     * 优惠券ID
     * 表 : bestfenxiao_user_coupon
     * 对应字段 : coupon_id
     */
    private Integer couponId;

    /**
     * 卡号
     * 表 : bestfenxiao_user_coupon
     * 对应字段 : card_no
     */
    private String cardNo;

    /**
     * 优惠券名称
     * 表 : bestfenxiao_user_coupon
     * 对应字段 : cname
     */
    private String cname;

    /**
     * 优惠券面值(起始)
     * 表 : bestfenxiao_user_coupon
     * 对应字段 : face_money
     */
    private BigDecimal faceMoney;

    /**
     * 使用优惠券的订单金额下限（为0：为不限定）
     * 表 : bestfenxiao_user_coupon
     * 对应字段 : limit_money
     */
    private BigDecimal limitMoney;

    /**
     * 生效时间
     * 表 : bestfenxiao_user_coupon
     * 对应字段 : start_time
     */
    private Integer startTime;

    /**
     * 过期时间
     * 表 : bestfenxiao_user_coupon
     * 对应字段 : end_time
     */
    private Integer endTime;

    /**
     * 到期提醒（0：不提醒；1：提醒）
     * 表 : bestfenxiao_user_coupon
     * 对应字段 : is_expire_notice
     */
    private Integer isExpireNotice;

    /**
     * 是否允许分享链接（0：不允许；1：允许）
     * 表 : bestfenxiao_user_coupon
     * 对应字段 : is_share
     */
    private Integer isShare;

    /**
     * 是否全店通用（0：全店通用；1：指定商品使用）
     * 表 : bestfenxiao_user_coupon
     * 对应字段 : is_all_product
     */
    private Integer isAllProduct;

    /**
     * 0:非原价购买可使用；1：原价购买商品时可
     * 表 : bestfenxiao_user_coupon
     * 对应字段 : is_original_price
     */
    private Integer isOriginalPrice;

    /**
     * 是否使用
     * 表 : bestfenxiao_user_coupon
     * 对应字段 : is_use
     */
    private Integer isUse;

    /**
     * 0:不可以使用，1：可以使用
     * 表 : bestfenxiao_user_coupon
     * 对应字段 : is_valid
     */
    private Integer isValid;

    /**
     * 优惠券使用时间
     * 表 : bestfenxiao_user_coupon
     * 对应字段 : use_time
     */
    private Integer useTime;

    /**
     * 领取优惠券的时间
     * 表 : bestfenxiao_user_coupon
     * 对应字段 : timestamp
     */
    private Integer timestamp;

    /**
     * 券类型（1：优惠券，2：赠送券）
     * 表 : bestfenxiao_user_coupon
     * 对应字段 : type
     */
    private Integer type;

    /**
     * 赠送的订单id
     * 表 : bestfenxiao_user_coupon
     * 对应字段 : give_order_id
     */
    private Integer giveOrderId;

    /**
     * 使用的订单id
     * 表 : bestfenxiao_user_coupon
     * 对应字段 : use_order_id
     */
    private Integer useOrderId;

    /**
     * 是否删除(0:未删除，1：已删除)
     * 表 : bestfenxiao_user_coupon
     * 对应字段 : delete_flg
     */
    private Integer deleteFlg;

    /**
     * 使用说明
     * 表 : bestfenxiao_user_coupon
     * 对应字段 : description
     */
    private String description;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table bestfenxiao_user_coupon
     *
     * @mbggenerated Sun Apr 22 10:00:05 CST 2018
     */
    private static final long serialVersionUID = 1L;

    /**
     * get method 
     *
     * @return bestfenxiao_user_coupon.id：
     */
    public Integer getId() {
        return id;
    }

    /**
     * set method 
     *
     * @param id  
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_user_coupon.uid：用户id
     */
    public Integer getUid() {
        return uid;
    }

    /**
     * set method 
     *
     * @param uid  用户id
     */
    public void setUid(Integer uid) {
        this.uid = uid;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_user_coupon.store_id：商铺id
     */
    public Integer getStoreId() {
        return storeId;
    }

    /**
     * set method 
     *
     * @param storeId  商铺id
     */
    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_user_coupon.coupon_id：优惠券ID
     */
    public Integer getCouponId() {
        return couponId;
    }

    /**
     * set method 
     *
     * @param couponId  优惠券ID
     */
    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_user_coupon.card_no：卡号
     */
    public String getCardNo() {
        return cardNo;
    }

    /**
     * set method 
     *
     * @param cardNo  卡号
     */
    public void setCardNo(String cardNo) {
        this.cardNo = cardNo == null ? null : cardNo.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_user_coupon.cname：优惠券名称
     */
    public String getCname() {
        return cname;
    }

    /**
     * set method 
     *
     * @param cname  优惠券名称
     */
    public void setCname(String cname) {
        this.cname = cname == null ? null : cname.trim();
    }

    /**
     * get method 
     *
     * @return bestfenxiao_user_coupon.face_money：优惠券面值(起始)
     */
    public BigDecimal getFaceMoney() {
        return faceMoney;
    }

    /**
     * set method 
     *
     * @param faceMoney  优惠券面值(起始)
     */
    public void setFaceMoney(BigDecimal faceMoney) {
        this.faceMoney = faceMoney;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_user_coupon.limit_money：使用优惠券的订单金额下限（为0：为不限定）
     */
    public BigDecimal getLimitMoney() {
        return limitMoney;
    }

    /**
     * set method 
     *
     * @param limitMoney  使用优惠券的订单金额下限（为0：为不限定）
     */
    public void setLimitMoney(BigDecimal limitMoney) {
        this.limitMoney = limitMoney;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_user_coupon.start_time：生效时间
     */
    public Integer getStartTime() {
        return startTime;
    }

    /**
     * set method 
     *
     * @param startTime  生效时间
     */
    public void setStartTime(Integer startTime) {
        this.startTime = startTime;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_user_coupon.end_time：过期时间
     */
    public Integer getEndTime() {
        return endTime;
    }

    /**
     * set method 
     *
     * @param endTime  过期时间
     */
    public void setEndTime(Integer endTime) {
        this.endTime = endTime;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_user_coupon.is_expire_notice：到期提醒（0：不提醒；1：提醒）
     */
    public Integer getIsExpireNotice() {
        return isExpireNotice;
    }

    /**
     * set method 
     *
     * @param isExpireNotice  到期提醒（0：不提醒；1：提醒）
     */
    public void setIsExpireNotice(Integer isExpireNotice) {
        this.isExpireNotice = isExpireNotice;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_user_coupon.is_share：是否允许分享链接（0：不允许；1：允许）
     */
    public Integer getIsShare() {
        return isShare;
    }

    /**
     * set method 
     *
     * @param isShare  是否允许分享链接（0：不允许；1：允许）
     */
    public void setIsShare(Integer isShare) {
        this.isShare = isShare;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_user_coupon.is_all_product：是否全店通用（0：全店通用；1：指定商品使用）
     */
    public Integer getIsAllProduct() {
        return isAllProduct;
    }

    /**
     * set method 
     *
     * @param isAllProduct  是否全店通用（0：全店通用；1：指定商品使用）
     */
    public void setIsAllProduct(Integer isAllProduct) {
        this.isAllProduct = isAllProduct;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_user_coupon.is_original_price：0:非原价购买可使用；1：原价购买商品时可
     */
    public Integer getIsOriginalPrice() {
        return isOriginalPrice;
    }

    /**
     * set method 
     *
     * @param isOriginalPrice  0:非原价购买可使用；1：原价购买商品时可
     */
    public void setIsOriginalPrice(Integer isOriginalPrice) {
        this.isOriginalPrice = isOriginalPrice;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_user_coupon.is_use：是否使用
     */
    public Integer getIsUse() {
        return isUse;
    }

    /**
     * set method 
     *
     * @param isUse  是否使用
     */
    public void setIsUse(Integer isUse) {
        this.isUse = isUse;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_user_coupon.is_valid：0:不可以使用，1：可以使用
     */
    public Integer getIsValid() {
        return isValid;
    }

    /**
     * set method 
     *
     * @param isValid  0:不可以使用，1：可以使用
     */
    public void setIsValid(Integer isValid) {
        this.isValid = isValid;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_user_coupon.use_time：优惠券使用时间
     */
    public Integer getUseTime() {
        return useTime;
    }

    /**
     * set method 
     *
     * @param useTime  优惠券使用时间
     */
    public void setUseTime(Integer useTime) {
        this.useTime = useTime;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_user_coupon.timestamp：领取优惠券的时间
     */
    public Integer getTimestamp() {
        return timestamp;
    }

    /**
     * set method 
     *
     * @param timestamp  领取优惠券的时间
     */
    public void setTimestamp(Integer timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_user_coupon.type：券类型（1：优惠券，2：赠送券）
     */
    public Integer getType() {
        return type;
    }

    /**
     * set method 
     *
     * @param type  券类型（1：优惠券，2：赠送券）
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_user_coupon.give_order_id：赠送的订单id
     */
    public Integer getGiveOrderId() {
        return giveOrderId;
    }

    /**
     * set method 
     *
     * @param giveOrderId  赠送的订单id
     */
    public void setGiveOrderId(Integer giveOrderId) {
        this.giveOrderId = giveOrderId;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_user_coupon.use_order_id：使用的订单id
     */
    public Integer getUseOrderId() {
        return useOrderId;
    }

    /**
     * set method 
     *
     * @param useOrderId  使用的订单id
     */
    public void setUseOrderId(Integer useOrderId) {
        this.useOrderId = useOrderId;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_user_coupon.delete_flg：是否删除(0:未删除，1：已删除)
     */
    public Integer getDeleteFlg() {
        return deleteFlg;
    }

    /**
     * set method 
     *
     * @param deleteFlg  是否删除(0:未删除，1：已删除)
     */
    public void setDeleteFlg(Integer deleteFlg) {
        this.deleteFlg = deleteFlg;
    }

    /**
     * get method 
     *
     * @return bestfenxiao_user_coupon.description：使用说明
     */
    public String getDescription() {
        return description;
    }

    /**
     * set method 
     *
     * @param description  使用说明
     */
    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    /**
     *
     * @param that
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        UserCoupon other = (UserCoupon) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getUid() == null ? other.getUid() == null : this.getUid().equals(other.getUid()))
            && (this.getStoreId() == null ? other.getStoreId() == null : this.getStoreId().equals(other.getStoreId()))
            && (this.getCouponId() == null ? other.getCouponId() == null : this.getCouponId().equals(other.getCouponId()))
            && (this.getCardNo() == null ? other.getCardNo() == null : this.getCardNo().equals(other.getCardNo()))
            && (this.getCname() == null ? other.getCname() == null : this.getCname().equals(other.getCname()))
            && (this.getFaceMoney() == null ? other.getFaceMoney() == null : this.getFaceMoney().equals(other.getFaceMoney()))
            && (this.getLimitMoney() == null ? other.getLimitMoney() == null : this.getLimitMoney().equals(other.getLimitMoney()))
            && (this.getStartTime() == null ? other.getStartTime() == null : this.getStartTime().equals(other.getStartTime()))
            && (this.getEndTime() == null ? other.getEndTime() == null : this.getEndTime().equals(other.getEndTime()))
            && (this.getIsExpireNotice() == null ? other.getIsExpireNotice() == null : this.getIsExpireNotice().equals(other.getIsExpireNotice()))
            && (this.getIsShare() == null ? other.getIsShare() == null : this.getIsShare().equals(other.getIsShare()))
            && (this.getIsAllProduct() == null ? other.getIsAllProduct() == null : this.getIsAllProduct().equals(other.getIsAllProduct()))
            && (this.getIsOriginalPrice() == null ? other.getIsOriginalPrice() == null : this.getIsOriginalPrice().equals(other.getIsOriginalPrice()))
            && (this.getIsUse() == null ? other.getIsUse() == null : this.getIsUse().equals(other.getIsUse()))
            && (this.getIsValid() == null ? other.getIsValid() == null : this.getIsValid().equals(other.getIsValid()))
            && (this.getUseTime() == null ? other.getUseTime() == null : this.getUseTime().equals(other.getUseTime()))
            && (this.getTimestamp() == null ? other.getTimestamp() == null : this.getTimestamp().equals(other.getTimestamp()))
            && (this.getType() == null ? other.getType() == null : this.getType().equals(other.getType()))
            && (this.getGiveOrderId() == null ? other.getGiveOrderId() == null : this.getGiveOrderId().equals(other.getGiveOrderId()))
            && (this.getUseOrderId() == null ? other.getUseOrderId() == null : this.getUseOrderId().equals(other.getUseOrderId()))
            && (this.getDeleteFlg() == null ? other.getDeleteFlg() == null : this.getDeleteFlg().equals(other.getDeleteFlg()))
            && (this.getDescription() == null ? other.getDescription() == null : this.getDescription().equals(other.getDescription()));
    }

    /**
     *
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getUid() == null) ? 0 : getUid().hashCode());
        result = prime * result + ((getStoreId() == null) ? 0 : getStoreId().hashCode());
        result = prime * result + ((getCouponId() == null) ? 0 : getCouponId().hashCode());
        result = prime * result + ((getCardNo() == null) ? 0 : getCardNo().hashCode());
        result = prime * result + ((getCname() == null) ? 0 : getCname().hashCode());
        result = prime * result + ((getFaceMoney() == null) ? 0 : getFaceMoney().hashCode());
        result = prime * result + ((getLimitMoney() == null) ? 0 : getLimitMoney().hashCode());
        result = prime * result + ((getStartTime() == null) ? 0 : getStartTime().hashCode());
        result = prime * result + ((getEndTime() == null) ? 0 : getEndTime().hashCode());
        result = prime * result + ((getIsExpireNotice() == null) ? 0 : getIsExpireNotice().hashCode());
        result = prime * result + ((getIsShare() == null) ? 0 : getIsShare().hashCode());
        result = prime * result + ((getIsAllProduct() == null) ? 0 : getIsAllProduct().hashCode());
        result = prime * result + ((getIsOriginalPrice() == null) ? 0 : getIsOriginalPrice().hashCode());
        result = prime * result + ((getIsUse() == null) ? 0 : getIsUse().hashCode());
        result = prime * result + ((getIsValid() == null) ? 0 : getIsValid().hashCode());
        result = prime * result + ((getUseTime() == null) ? 0 : getUseTime().hashCode());
        result = prime * result + ((getTimestamp() == null) ? 0 : getTimestamp().hashCode());
        result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
        result = prime * result + ((getGiveOrderId() == null) ? 0 : getGiveOrderId().hashCode());
        result = prime * result + ((getUseOrderId() == null) ? 0 : getUseOrderId().hashCode());
        result = prime * result + ((getDeleteFlg() == null) ? 0 : getDeleteFlg().hashCode());
        result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
        return result;
    }
}