package com.micro.shop.redis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
/**
 * https://blog.csdn.net/abombhz/article/details/78123253?locationNum=6&fps=1
 */
@Configuration
@EnableCaching
public class RedisConfig extends CachingConfigurerSupport{
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	  @Value("${spring.redis.host}")
	  private String host;

	  @Value("${spring.redis.port}")
	  private int port;

	  @Value("${spring.redis.timeout}")
	  private int timeout;

	  @Value("${spring.redis.password}")
	  private String password;

	  @Value("${spring.redis.database}")
	  private int database;

	  @Value("${spring.redis.pool.max-idle}")
	  private int maxIdle;

	  @Value("${spring.redis.pool.min-idle}") 
	  private int minIdle;
	
	  /**
	   * 连接池配置
	   * @Description:
	   * @return
	   */
	  @Bean
	  public JedisPoolConfig jedisPoolConfig() {
	    JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
	    jedisPoolConfig.setMaxIdle(maxIdle);
	    jedisPoolConfig.setMinIdle(minIdle);
//	    jedisPoolConfig.set ...
	    return jedisPoolConfig;
	  }

	  
	  /**
	   * 连接池配置
	   * @Description:
	   * @return
	   */
	  @Bean
	  public JedisPool jedisPool() {
	    JedisPool jedisPool =
	     new JedisPool(jedisPoolConfig(),host,port,timeout);
	    return jedisPool;
	  }
	

}
