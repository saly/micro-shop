package com.micro.shop.redis;

import java.text.MessageFormat;

/**    
 * @Title: RedisKeys.java  
 * @Description: TODO  
 * @author fanchunfeng    
 * @date 2018年4月27日 下午4:22:41  
 * @version V1.0    
 */
public class RedisKeys {
	
	/**
	 * sessionid,使用hash保存，0占位符存token值，hash中会设置6个field：userId,menus、permissions、role、company、user
	 */
	public static final String SESSIONID_HASHKEY="cache:sessionid:{0}";
	
	/**
	 * 用户对应的token，string存储，0占位符存储用户id
	 */
	public static final String USER_TOKEN_STRINGKEY="user:{0}:sessionid";
	/**
	 * 手机短信验证码，string存储，0占位符存储验证码
	 */
	public  static final String PHONE_VERIFY_CODE="cache:phone:{0}";
	/**
	 * 手机短信验证码，string存储，0占位符存储openid,0占位符存储手机号码
	 */
	public  static final String OPENID_VERIFY_PHONE_CODE="cache:openid:{0}:phone:{1}";
	
	/**
	 * h5token 存储对应渠道,字符存储，缓存1个小时
	 */
	public static final String CACHE_H5TOKEN_STRINGKEY="cache:h5token:{0}";
	
	/**
	 * membertoken 存储对应会员,字符存储，缓存1个小时
	 */
	public static final String CACHE_MEMBERTOKEN_STRINGKEY="cache:h5token:{0}";
	
	/**
	 * pfidtocompanyid 存储pfid companyId映射关系，缓存1星期
	 */
	public static final String CACHE_PFIDTOCOMPANYID_STRINGKEY = "cache:pfidtocompanyid:{0}";
	
	/**
	 * downloadcnlinfo 用户下载渠道受纵详细数据
	 */
	public static final String CACHE_DOWNLOAD_CNLINFOKEY = "cache:downloadcnlinfo:{0}";
	
	/**
	 * 企业参数信息，存储企业的参数，缓存1天
	 */
	public static final String CACHE_COMPANY_PARAM_HASHKEY = "cache:company:param:{0}";
	
	/**
	 * 获取redis真正的key
	 * @param key 模式
	 * @param objects 参数
	 * @return String key
	 */
	public static String getKey(String key,Object...objects){
		return MessageFormat.format(key, objects);
	}
	
	/**
	 * 返回redis key
	 * @param key key
	 * @return String key
	 */
	public static String getKey(String key){
		return key;
	}

}
