package com.micro.shop.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.micro.shop.utils.JsonUtils;
import com.micro.shop.vo.UserTokenInfoVo;

@Component
public class RedisWechatApptoken {
	
	@Autowired
	private RedisUtils redisUtils;

	public void createH5Token(String h5token,UserTokenInfoVo value,int seconds){
		String key = RedisKeys.getKey(RedisKeys.CACHE_H5TOKEN_STRINGKEY, h5token);
		redisUtils.setex(key, JsonUtils.writeValueAsString(value), seconds);
	}

	public void updateH5TokenExpire(String h5token,int seconds){
		String key = RedisKeys.getKey(RedisKeys.CACHE_H5TOKEN_STRINGKEY, h5token);
		redisUtils.expire(key, seconds);
	}

	public boolean isExistH5token(String h5token){
		String key = RedisKeys.getKey(RedisKeys.CACHE_H5TOKEN_STRINGKEY, h5token);
		return redisUtils.exists(key);
	}
	
	public void createMemberToken(String membertoken,UserTokenInfoVo value,int seconds){
		String key = RedisKeys.getKey(RedisKeys.CACHE_MEMBERTOKEN_STRINGKEY, membertoken);
		redisUtils.setex(key, JsonUtils.writeValueAsString(value), seconds);
	}
	
	public UserTokenInfoVo getH5MemberVo(String membertoken){
		String json = redisUtils.get(RedisKeys.getKey(RedisKeys.CACHE_MEMBERTOKEN_STRINGKEY, membertoken));
		if(json != null ){
			return JsonUtils.convertValue(json, UserTokenInfoVo.class);
		}
		return null;
	}
	
	public void updateMemberTokenExpire(String membertoken,int seconds){
		String key = RedisKeys.getKey(RedisKeys.CACHE_MEMBERTOKEN_STRINGKEY, membertoken);
		redisUtils.expire(key, seconds);
	}
	
	public boolean isExistMembertoken(String membertoken){
		String key = RedisKeys.getKey(RedisKeys.CACHE_MEMBERTOKEN_STRINGKEY, membertoken);
		return redisUtils.exists(key);
	}
}
