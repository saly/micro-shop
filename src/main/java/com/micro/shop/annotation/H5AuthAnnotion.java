package com.micro.shop.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**  
 * h5 openid 认证自定义注解   
 * @ClassName: H5AuthAnnotion    
 * @author 陈剑飞    
 * @date 2017年5月21日 下午4:19:06    
 * @version  v 1.0    
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface H5AuthAnnotion {

}
