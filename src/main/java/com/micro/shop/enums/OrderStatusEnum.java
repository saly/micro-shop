package com.micro.shop.enums;

public enum OrderStatusEnum {
	PROVISIONAL(0,"临时订单"),
	WAITING_PAY(1,"等待买家付款"),
	WAITING_SEND_OUT(2,"等待卖家发"),
	SEND_OUT(3,"卖家已发货"),
	FINISH(4,"交易完成"),
	CLOSED(5,"订单关闭"),
	REFUND(6,"退款中");
	private Integer status;
	
	private String desc;
	
	OrderStatusEnum(Integer status,String desc){
		this.status = status;
		this.desc = desc;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	
	
	
}
