package com.micro.shop.mapper;

import com.micro.shop.model.ProductToProperty;
import com.micro.shop.model.ProductToPropertyCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ProductToPropertyMapper {
    /**
     * 根据条件计数
     *
     * @param example
     */
    int countByExample(ProductToPropertyCriteria example);

    /**
     *
     * @param example
     */
    int deleteByExample(ProductToPropertyCriteria example);

    /**
     * 根据主键删除数据库的记录
     *
     * @param bestfenxiaoId
     */
    int deleteByPrimaryKey(Integer bestfenxiaoId);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insert(ProductToProperty record);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insertSelective(ProductToProperty record);

    /**
     * 根据条件查询列表
     *
     * @param example
     */
    List<ProductToProperty> selectByExample(ProductToPropertyCriteria example);

    /**
     * 根据主键获取一条数据库记录
     *
     * @param bestfenxiaoId
     */
    ProductToProperty selectByPrimaryKey(Integer bestfenxiaoId);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExampleSelective(@Param("record") ProductToProperty record, @Param("example") ProductToPropertyCriteria example);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExample(@Param("record") ProductToProperty record, @Param("example") ProductToPropertyCriteria example);

    /**
     * 根据主键来更新部分数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(ProductToProperty record);

    /**
     * 根据主键来更新数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(ProductToProperty record);
}