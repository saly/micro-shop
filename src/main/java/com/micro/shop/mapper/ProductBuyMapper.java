package com.micro.shop.mapper;

import org.apache.ibatis.annotations.Param;

public interface ProductBuyMapper {
	
	Integer getProductPronum(@Param("productId")Integer productId,@Param("userId")Integer userId);

}
