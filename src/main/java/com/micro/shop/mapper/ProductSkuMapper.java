package com.micro.shop.mapper;

import com.micro.shop.model.ProductSku;
import com.micro.shop.model.ProductSkuCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ProductSkuMapper {
    /**
     * 根据条件计数
     *
     * @param example
     */
    int countByExample(ProductSkuCriteria example);

    /**
     *
     * @param example
     */
    int deleteByExample(ProductSkuCriteria example);

    /**
     * 根据主键删除数据库的记录
     *
     * @param skuId
     */
    int deleteByPrimaryKey(Integer skuId);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insert(ProductSku record);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insertSelective(ProductSku record);

    /**
     * 根据条件查询列表
     *
     * @param example
     */
    List<ProductSku> selectByExample(ProductSkuCriteria example);

    /**
     * 根据主键获取一条数据库记录
     *
     * @param skuId
     */
    ProductSku selectByPrimaryKey(Integer skuId);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExampleSelective(@Param("record") ProductSku record, @Param("example") ProductSkuCriteria example);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExample(@Param("record") ProductSku record, @Param("example") ProductSkuCriteria example);

    /**
     * 根据主键来更新部分数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(ProductSku record);

    /**
     * 根据主键来更新数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(ProductSku record);
}