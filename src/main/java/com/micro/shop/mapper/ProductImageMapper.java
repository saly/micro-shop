package com.micro.shop.mapper;

import com.micro.shop.model.ProductImage;
import com.micro.shop.model.ProductImageCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ProductImageMapper {
    /**
     * 根据条件计数
     *
     * @param example
     */
    int countByExample(ProductImageCriteria example);

    /**
     *
     * @param example
     */
    int deleteByExample(ProductImageCriteria example);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insert(ProductImage record);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insertSelective(ProductImage record);

    /**
     * 根据条件查询列表
     *
     * @param example
     */
    List<ProductImage> selectByExample(ProductImageCriteria example);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExampleSelective(@Param("record") ProductImage record, @Param("example") ProductImageCriteria example);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExample(@Param("record") ProductImage record, @Param("example") ProductImageCriteria example);
}