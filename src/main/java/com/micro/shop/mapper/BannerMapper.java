package com.micro.shop.mapper;

import java.util.List;

import com.micro.shop.model.Banner;

public interface BannerMapper {

	public List<Banner> findBanner();
	
}
