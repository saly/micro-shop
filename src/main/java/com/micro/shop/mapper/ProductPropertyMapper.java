package com.micro.shop.mapper;

import com.micro.shop.model.ProductProperty;
import com.micro.shop.model.ProductPropertyCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ProductPropertyMapper {
    /**
     * 根据条件计数
     *
     * @param example
     */
    int countByExample(ProductPropertyCriteria example);

    /**
     *
     * @param example
     */
    int deleteByExample(ProductPropertyCriteria example);

    /**
     * 根据主键删除数据库的记录
     *
     * @param pid
     */
    int deleteByPrimaryKey(Integer pid);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insert(ProductProperty record);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insertSelective(ProductProperty record);

    /**
     * 根据条件查询列表
     *
     * @param example
     */
    List<ProductProperty> selectByExample(ProductPropertyCriteria example);

    /**
     * 根据主键获取一条数据库记录
     *
     * @param pid
     */
    ProductProperty selectByPrimaryKey(Integer pid);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExampleSelective(@Param("record") ProductProperty record, @Param("example") ProductPropertyCriteria example);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExample(@Param("record") ProductProperty record, @Param("example") ProductPropertyCriteria example);

    /**
     * 根据主键来更新部分数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(ProductProperty record);

    /**
     * 根据主键来更新数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(ProductProperty record);
}