package com.micro.shop.mapper;

import com.micro.shop.model.OrderPackage;
import com.micro.shop.model.OrderPackageCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface OrderPackageMapper {
    /**
     * 根据条件计数
     *
     * @param example
     */
    int countByExample(OrderPackageCriteria example);

    /**
     *
     * @param example
     */
    int deleteByExample(OrderPackageCriteria example);

    /**
     * 根据主键删除数据库的记录
     *
     * @param packageId
     */
    int deleteByPrimaryKey(Integer packageId);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insert(OrderPackage record);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insertSelective(OrderPackage record);

    /**
     * 根据条件查询列表
     *
     * @param example
     */
    List<OrderPackage> selectByExample(OrderPackageCriteria example);

    /**
     * 根据主键获取一条数据库记录
     *
     * @param packageId
     */
    OrderPackage selectByPrimaryKey(Integer packageId);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExampleSelective(@Param("record") OrderPackage record, @Param("example") OrderPackageCriteria example);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExample(@Param("record") OrderPackage record, @Param("example") OrderPackageCriteria example);

    /**
     * 根据主键来更新部分数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(OrderPackage record);

    /**
     * 根据主键来更新数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(OrderPackage record);
}