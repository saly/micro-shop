package com.micro.shop.mapper;

import com.micro.shop.model.Product;
import com.micro.shop.model.ProductCriteria;
import com.micro.shop.model.ProductWithBLOBs;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ProductMapper {
    /**
     * 根据条件计数
     *
     * @param example
     */
    int countByExample(ProductCriteria example);

    /**
     *
     * @param example
     */
    int deleteByExample(ProductCriteria example);

    /**
     * 根据主键删除数据库的记录
     *
     * @param productId
     */
    int deleteByPrimaryKey(Integer productId);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insert(ProductWithBLOBs record);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insertSelective(ProductWithBLOBs record);

    /**
     *
     * @param example
     */
    List<ProductWithBLOBs> selectByExampleWithBLOBs(ProductCriteria example);

    /**
     * 根据条件查询列表
     *
     * @param example
     */
    List<Product> selectByExample(ProductCriteria example);

    /**
     * 根据主键获取一条数据库记录
     *
     * @param productId
     */
    ProductWithBLOBs selectByPrimaryKey(Integer productId);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExampleSelective(@Param("record") ProductWithBLOBs record, @Param("example") ProductCriteria example);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExampleWithBLOBs(@Param("record") ProductWithBLOBs record, @Param("example") ProductCriteria example);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExample(@Param("record") Product record, @Param("example") ProductCriteria example);

    /**
     * 根据主键来更新部分数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(ProductWithBLOBs record);

    /**
     *
     * @param record
     */
    int updateByPrimaryKeyWithBLOBs(ProductWithBLOBs record);

    /**
     * 根据主键来更新数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(Product record);
}