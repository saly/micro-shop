package com.micro.shop.mapper;

import com.micro.shop.model.ProductToPropertyValue;
import com.micro.shop.model.ProductToPropertyValueCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ProductToPropertyValueMapper {
    /**
     * 根据条件计数
     *
     * @param example
     */
    int countByExample(ProductToPropertyValueCriteria example);

    /**
     *
     * @param example
     */
    int deleteByExample(ProductToPropertyValueCriteria example);

    /**
     * 根据主键删除数据库的记录
     *
     * @param bestfenxiaoId
     */
    int deleteByPrimaryKey(Integer bestfenxiaoId);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insert(ProductToPropertyValue record);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insertSelective(ProductToPropertyValue record);

    /**
     * 根据条件查询列表
     *
     * @param example
     */
    List<ProductToPropertyValue> selectByExample(ProductToPropertyValueCriteria example);

    /**
     * 根据主键获取一条数据库记录
     *
     * @param bestfenxiaoId
     */
    ProductToPropertyValue selectByPrimaryKey(Integer bestfenxiaoId);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExampleSelective(@Param("record") ProductToPropertyValue record, @Param("example") ProductToPropertyValueCriteria example);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExample(@Param("record") ProductToPropertyValue record, @Param("example") ProductToPropertyValueCriteria example);

    /**
     * 根据主键来更新部分数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(ProductToPropertyValue record);

    /**
     * 根据主键来更新数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(ProductToPropertyValue record);
}