package com.micro.shop.mapper;

import com.micro.shop.model.OrderPeerpay;
import com.micro.shop.model.OrderPeerpayCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface OrderPeerpayMapper {
    /**
     * 根据条件计数
     *
     * @param example
     */
    int countByExample(OrderPeerpayCriteria example);

    /**
     *
     * @param example
     */
    int deleteByExample(OrderPeerpayCriteria example);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insert(OrderPeerpay record);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insertSelective(OrderPeerpay record);

    /**
     *
     * @param example
     */
    List<OrderPeerpay> selectByExampleWithBLOBs(OrderPeerpayCriteria example);

    /**
     * 根据条件查询列表
     *
     * @param example
     */
    List<OrderPeerpay> selectByExample(OrderPeerpayCriteria example);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExampleSelective(@Param("record") OrderPeerpay record, @Param("example") OrderPeerpayCriteria example);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExampleWithBLOBs(@Param("record") OrderPeerpay record, @Param("example") OrderPeerpayCriteria example);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExample(@Param("record") OrderPeerpay record, @Param("example") OrderPeerpayCriteria example);
}