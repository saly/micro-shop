package com.micro.shop.mapper;

import java.util.List;

import com.micro.shop.model.Notice;

public interface NoticeMapper {

	public List<Notice> findNotice();
	
	public Notice findDetail(Integer id);
	
}
