package com.micro.shop.mapper;

public interface GetCouponMapper {
	
	int reduceCoupon(Integer couponId);

}
