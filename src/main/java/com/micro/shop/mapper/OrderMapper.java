package com.micro.shop.mapper;

import com.micro.shop.model.Order;
import com.micro.shop.model.OrderCriteria;
import com.micro.shop.model.OrderWithBLOBs;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface OrderMapper {
    /**
     * 根据条件计数
     *
     * @param example
     */
    int countByExample(OrderCriteria example);

    /**
     *
     * @param example
     */
    int deleteByExample(OrderCriteria example);

    /**
     * 根据主键删除数据库的记录
     *
     * @param orderId
     */
    int deleteByPrimaryKey(Integer orderId);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insert(OrderWithBLOBs record);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insertSelective(OrderWithBLOBs record);

    /**
     *
     * @param example
     */
    List<OrderWithBLOBs> selectByExampleWithBLOBs(OrderCriteria example);

    /**
     * 根据条件查询列表
     *
     * @param example
     */
    List<Order> selectByExample(OrderCriteria example);

    /**
     * 根据主键获取一条数据库记录
     *
     * @param orderId
     */
    OrderWithBLOBs selectByPrimaryKey(Integer orderId);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExampleSelective(@Param("record") OrderWithBLOBs record, @Param("example") OrderCriteria example);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExampleWithBLOBs(@Param("record") OrderWithBLOBs record, @Param("example") OrderCriteria example);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExample(@Param("record") Order record, @Param("example") OrderCriteria example);

    /**
     * 根据主键来更新部分数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(OrderWithBLOBs record);

    /**
     *
     * @param record
     */
    int updateByPrimaryKeyWithBLOBs(OrderWithBLOBs record);

    /**
     * 根据主键来更新数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(Order record);
}