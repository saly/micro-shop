package com.micro.shop.mapper;

import com.micro.shop.model.UserCoupon;
import com.micro.shop.model.UserCouponCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserCouponMapper {
    /**
     * 根据条件计数
     *
     * @param example
     */
    int countByExample(UserCouponCriteria example);

    /**
     *
     * @param example
     */
    int deleteByExample(UserCouponCriteria example);

    /**
     * 根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insert(UserCoupon record);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insertSelective(UserCoupon record);

    /**
     *
     * @param example
     */
    List<UserCoupon> selectByExampleWithBLOBs(UserCouponCriteria example);

    /**
     * 根据条件查询列表
     *
     * @param example
     */
    List<UserCoupon> selectByExample(UserCouponCriteria example);

    /**
     * 根据主键获取一条数据库记录
     *
     * @param id
     */
    UserCoupon selectByPrimaryKey(Integer id);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExampleSelective(@Param("record") UserCoupon record, @Param("example") UserCouponCriteria example);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExampleWithBLOBs(@Param("record") UserCoupon record, @Param("example") UserCouponCriteria example);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExample(@Param("record") UserCoupon record, @Param("example") UserCouponCriteria example);

    /**
     * 根据主键来更新部分数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(UserCoupon record);

    /**
     *
     * @param record
     */
    int updateByPrimaryKeyWithBLOBs(UserCoupon record);

    /**
     * 根据主键来更新数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(UserCoupon record);
}