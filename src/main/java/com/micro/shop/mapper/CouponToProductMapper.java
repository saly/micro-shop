package com.micro.shop.mapper;

import com.micro.shop.model.CouponToProduct;
import com.micro.shop.model.CouponToProductCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CouponToProductMapper {
    /**
     * 根据条件计数
     *
     * @param example
     */
    int countByExample(CouponToProductCriteria example);

    /**
     *
     * @param example
     */
    int deleteByExample(CouponToProductCriteria example);

    /**
     * 根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insert(CouponToProduct record);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insertSelective(CouponToProduct record);

    /**
     * 根据条件查询列表
     *
     * @param example
     */
    List<CouponToProduct> selectByExample(CouponToProductCriteria example);

    /**
     * 根据主键获取一条数据库记录
     *
     * @param id
     */
    CouponToProduct selectByPrimaryKey(Integer id);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExampleSelective(@Param("record") CouponToProduct record, @Param("example") CouponToProductCriteria example);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExample(@Param("record") CouponToProduct record, @Param("example") CouponToProductCriteria example);

    /**
     * 根据主键来更新部分数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(CouponToProduct record);

    /**
     * 根据主键来更新数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(CouponToProduct record);
}