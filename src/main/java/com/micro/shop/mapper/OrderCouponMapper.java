package com.micro.shop.mapper;

import com.micro.shop.model.OrderCoupon;
import com.micro.shop.model.OrderCouponCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface OrderCouponMapper {
    /**
     * 根据条件计数
     *
     * @param example
     */
    int countByExample(OrderCouponCriteria example);

    /**
     *
     * @param example
     */
    int deleteByExample(OrderCouponCriteria example);

    /**
     * 根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insert(OrderCoupon record);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insertSelective(OrderCoupon record);

    /**
     * 根据条件查询列表
     *
     * @param example
     */
    List<OrderCoupon> selectByExample(OrderCouponCriteria example);

    /**
     * 根据主键获取一条数据库记录
     *
     * @param id
     */
    OrderCoupon selectByPrimaryKey(Integer id);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExampleSelective(@Param("record") OrderCoupon record, @Param("example") OrderCouponCriteria example);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExample(@Param("record") OrderCoupon record, @Param("example") OrderCouponCriteria example);

    /**
     * 根据主键来更新部分数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(OrderCoupon record);

    /**
     * 根据主键来更新数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(OrderCoupon record);
}