package com.micro.shop.mapper;

import com.micro.shop.model.UserCollect;
import com.micro.shop.model.UserCollectCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserCollectMapper {
    /**
     * 根据条件计数
     *
     * @param example
     */
    int countByExample(UserCollectCriteria example);

    /**
     *
     * @param example
     */
    int deleteByExample(UserCollectCriteria example);

    /**
     * 根据主键删除数据库的记录
     *
     * @param collectId
     */
    int deleteByPrimaryKey(Integer collectId);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insert(UserCollect record);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insertSelective(UserCollect record);

    /**
     * 根据条件查询列表
     *
     * @param example
     */
    List<UserCollect> selectByExample(UserCollectCriteria example);

    /**
     * 根据主键获取一条数据库记录
     *
     * @param collectId
     */
    UserCollect selectByPrimaryKey(Integer collectId);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExampleSelective(@Param("record") UserCollect record, @Param("example") UserCollectCriteria example);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExample(@Param("record") UserCollect record, @Param("example") UserCollectCriteria example);

    /**
     * 根据主键来更新部分数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(UserCollect record);

    /**
     * 根据主键来更新数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(UserCollect record);
}