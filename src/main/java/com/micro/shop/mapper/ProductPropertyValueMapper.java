package com.micro.shop.mapper;

import com.micro.shop.model.ProductPropertyValue;
import com.micro.shop.model.ProductPropertyValueCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ProductPropertyValueMapper {
    /**
     * 根据条件计数
     *
     * @param example
     */
    int countByExample(ProductPropertyValueCriteria example);

    /**
     *
     * @param example
     */
    int deleteByExample(ProductPropertyValueCriteria example);

    /**
     * 根据主键删除数据库的记录
     *
     * @param vid
     */
    int deleteByPrimaryKey(Integer vid);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insert(ProductPropertyValue record);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insertSelective(ProductPropertyValue record);

    /**
     * 根据条件查询列表
     *
     * @param example
     */
    List<ProductPropertyValue> selectByExample(ProductPropertyValueCriteria example);

    /**
     * 根据主键获取一条数据库记录
     *
     * @param vid
     */
    ProductPropertyValue selectByPrimaryKey(Integer vid);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExampleSelective(@Param("record") ProductPropertyValue record, @Param("example") ProductPropertyValueCriteria example);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExample(@Param("record") ProductPropertyValue record, @Param("example") ProductPropertyValueCriteria example);

    /**
     * 根据主键来更新部分数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(ProductPropertyValue record);

    /**
     * 根据主键来更新数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(ProductPropertyValue record);
}