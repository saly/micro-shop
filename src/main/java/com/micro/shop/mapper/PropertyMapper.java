package com.micro.shop.mapper;

import java.util.List;

import com.micro.shop.vo.PropertyVO;
import com.micro.shop.vo.PropertyValueVO;

public interface PropertyMapper {

	public List<PropertyVO> findPropertyByProductId(Integer productId);
	
	public List<PropertyValueVO> findPropertyValueByProductId(Integer productId);
	
	
}
