package com.micro.shop.mapper;

import com.micro.shop.model.Coupon;
import com.micro.shop.model.CouponCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CouponMapper {
    /**
     * 根据条件计数
     *
     * @param example
     */
    int countByExample(CouponCriteria example);

    /**
     *
     * @param example
     */
    int deleteByExample(CouponCriteria example);

    /**
     * 根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insert(Coupon record);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insertSelective(Coupon record);

    /**
     *
     * @param example
     */
    List<Coupon> selectByExampleWithBLOBs(CouponCriteria example);

    /**
     * 根据条件查询列表
     *
     * @param example
     */
    List<Coupon> selectByExample(CouponCriteria example);

    /**
     * 根据主键获取一条数据库记录
     *
     * @param id
     */
    Coupon selectByPrimaryKey(Integer id);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExampleSelective(@Param("record") Coupon record, @Param("example") CouponCriteria example);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExampleWithBLOBs(@Param("record") Coupon record, @Param("example") CouponCriteria example);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExample(@Param("record") Coupon record, @Param("example") CouponCriteria example);

    /**
     * 根据主键来更新部分数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(Coupon record);

    /**
     *
     * @param record
     */
    int updateByPrimaryKeyWithBLOBs(Coupon record);

    /**
     * 根据主键来更新数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(Coupon record);
}