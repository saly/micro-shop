package com.micro.shop.mapper;

import com.micro.shop.model.Store;
import com.micro.shop.model.StoreCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface StoreMapper {
    /**
     * 根据条件计数
     *
     * @param example
     */
    int countByExample(StoreCriteria example);

    /**
     *
     * @param example
     */
    int deleteByExample(StoreCriteria example);

    /**
     * 根据主键删除数据库的记录
     *
     * @param storeId
     */
    int deleteByPrimaryKey(Integer storeId);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insert(Store record);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insertSelective(Store record);

    /**
     * 根据条件查询列表
     *
     * @param example
     */
    List<Store> selectByExample(StoreCriteria example);

    /**
     * 根据主键获取一条数据库记录
     *
     * @param storeId
     */
    Store selectByPrimaryKey(Integer storeId);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExampleSelective(@Param("record") Store record, @Param("example") StoreCriteria example);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExample(@Param("record") Store record, @Param("example") StoreCriteria example);

    /**
     * 根据主键来更新部分数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(Store record);

    /**
     * 根据主键来更新数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(Store record);
}