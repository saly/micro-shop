package com.micro.shop.mapper;

import com.micro.shop.model.OrderProduct;
import com.micro.shop.model.OrderProductCriteria;
import com.micro.shop.model.OrderProductWithBLOBs;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface OrderProductMapper {
    /**
     * 根据条件计数
     *
     * @param example
     */
    int countByExample(OrderProductCriteria example);

    /**
     *
     * @param example
     */
    int deleteByExample(OrderProductCriteria example);

    /**
     * 根据主键删除数据库的记录
     *
     * @param bestfenxiaoId
     */
    int deleteByPrimaryKey(Integer bestfenxiaoId);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insert(OrderProductWithBLOBs record);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insertSelective(OrderProductWithBLOBs record);

    /**
     *
     * @param example
     */
    List<OrderProductWithBLOBs> selectByExampleWithBLOBs(OrderProductCriteria example);

    /**
     * 根据条件查询列表
     *
     * @param example
     */
    List<OrderProduct> selectByExample(OrderProductCriteria example);

    /**
     * 根据主键获取一条数据库记录
     *
     * @param bestfenxiaoId
     */
    OrderProductWithBLOBs selectByPrimaryKey(Integer bestfenxiaoId);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExampleSelective(@Param("record") OrderProductWithBLOBs record, @Param("example") OrderProductCriteria example);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExampleWithBLOBs(@Param("record") OrderProductWithBLOBs record, @Param("example") OrderProductCriteria example);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExample(@Param("record") OrderProduct record, @Param("example") OrderProductCriteria example);

    /**
     * 根据主键来更新部分数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(OrderProductWithBLOBs record);

    /**
     *
     * @param record
     */
    int updateByPrimaryKeyWithBLOBs(OrderProductWithBLOBs record);

    /**
     * 根据主键来更新数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(OrderProduct record);
}