package com.micro.shop.mapper;

import com.micro.shop.model.OrderReward;
import com.micro.shop.model.OrderRewardCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface OrderRewardMapper {
    /**
     * 根据条件计数
     *
     * @param example
     */
    int countByExample(OrderRewardCriteria example);

    /**
     *
     * @param example
     */
    int deleteByExample(OrderRewardCriteria example);

    /**
     * 根据主键删除数据库的记录
     *
     * @param id
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insert(OrderReward record);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insertSelective(OrderReward record);

    /**
     *
     * @param example
     */
    List<OrderReward> selectByExampleWithBLOBs(OrderRewardCriteria example);

    /**
     * 根据条件查询列表
     *
     * @param example
     */
    List<OrderReward> selectByExample(OrderRewardCriteria example);

    /**
     * 根据主键获取一条数据库记录
     *
     * @param id
     */
    OrderReward selectByPrimaryKey(Integer id);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExampleSelective(@Param("record") OrderReward record, @Param("example") OrderRewardCriteria example);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExampleWithBLOBs(@Param("record") OrderReward record, @Param("example") OrderRewardCriteria example);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExample(@Param("record") OrderReward record, @Param("example") OrderRewardCriteria example);

    /**
     * 根据主键来更新部分数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(OrderReward record);

    /**
     *
     * @param record
     */
    int updateByPrimaryKeyWithBLOBs(OrderReward record);

    /**
     * 根据主键来更新数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(OrderReward record);
}