package com.micro.shop.mapper;

import com.micro.shop.model.User;
import com.micro.shop.model.UserCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserMapper {
    /**
     * 根据条件计数
     *
     * @param example
     */
    int countByExample(UserCriteria example);

    /**
     *
     * @param example
     */
    int deleteByExample(UserCriteria example);

    /**
     * 根据主键删除数据库的记录
     *
     * @param uid
     */
    int deleteByPrimaryKey(Integer uid);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insert(User record);

    /**
     * 插入数据库记录
     *
     * @param record
     */
    int insertSelective(User record);

    /**
     * 根据条件查询列表
     *
     * @param example
     */
    List<User> selectByExample(UserCriteria example);

    /**
     * 根据主键获取一条数据库记录
     *
     * @param uid
     */
    User selectByPrimaryKey(Integer uid);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExampleSelective(@Param("record") User record, @Param("example") UserCriteria example);

    /**
     * 选择性更新数据库记录
     *
     * @param record
     * @param example
     */
    int updateByExample(@Param("record") User record, @Param("example") UserCriteria example);

    /**
     * 根据主键来更新部分数据库记录
     *
     * @param record
     */
    int updateByPrimaryKeySelective(User record);

    /**
     * 根据主键来更新数据库记录
     *
     * @param record
     */
    int updateByPrimaryKey(User record);
}