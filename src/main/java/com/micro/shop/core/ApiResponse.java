package com.micro.shop.core;

import java.io.Serializable;

/**  
 * api 返回类  
 * @ClassName: ApiResponse    
 * @author 陈剑飞    
 * @date 2017年1月20日 下午5:21:47    
 * @version  v 1.0
 */
@SuppressWarnings("serial")
public class ApiResponse<T> implements Serializable{
	/**
	 * 响应码
	 */
	private String code;
	/**
	 * 响应编码
	 */
	private String errorCode;
	/**
	 * 响应消息
	 */
	private String msg;
	/**
	 * 响应数据对象
	 */
	private T data;

	public ApiResponse() {
		this.code="200";
		this.errorCode="SUCCESS";
		this.msg="ok";
	}

	public ApiResponse(T data) {
		this.code="200";
		this.errorCode="SUCCESS";
		this.msg="ok";
		this.data = data;
	}

	public ApiResponse(String code, String errorCode) {
		this.code = code;
		this.errorCode = errorCode;
	}
	
	public ApiResponse(String code, String errorCode,String msg) {
		this.code = code;
		this.errorCode = errorCode;
		this.msg = msg;
	}
	
	public ApiResponse(BaseErrorCode baseErrorCode) {
		this.code = baseErrorCode.getCode();
		this.errorCode = baseErrorCode.getErrorCode();
		this.msg = baseErrorCode.getMsg();
	}

	public String getCode() {
		return code;
	}

	public ApiResponse<T> setCode(String code) {
		this.code = code;
		return this;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public  ApiResponse<T>  setErrorCode(String errorCode) {
		this.errorCode = errorCode;
		return this;
	}
	
	/**
	 * 设置错误对象  
	 * @author 陈剑飞    
	 * @Title: errorCode    
	 * @param baseErrorCode 
	 * @Return: void 返回值
	 */
	public  ApiResponse<T>  setErrorCode(BaseErrorCode baseErrorCode){
		this.code = baseErrorCode.getCode();
		this.errorCode = baseErrorCode.getErrorCode();
		this.msg = baseErrorCode.getMsg();
		return this;
	}
	
	public  ApiResponse<T>  setRetCode(RetCode retCode){
		this.code = retCode.getCode();
		this.errorCode = retCode.getErrorCode();
		return this;
	}

	public String getMsg() {
		return msg;
	}

	public  ApiResponse<T>  setMsg(String msg) {
		this.msg = msg;
		return this;
	}

	public T getData() {
		return data;
	}

	public  ApiResponse<T>  setData(T data) {
		this.data = data;
		return this;
	}
}
