package com.micro.shop.core;
public enum RetCode{
		SUCCESS("200","SUCCESS"),
		ERROR("500","ERROR");
	
		private String code;
		private String errorCode;
		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getErrorCode() {
			return errorCode;
		}

		public void setErrorCode(String errorCode) {
			this.errorCode = errorCode;
		}

		private RetCode(String code,String errorCode){
			this.code = code;
			this.errorCode=errorCode;
		}
	}