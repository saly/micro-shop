package com.micro.shop.core;


public class ErrorCode {
	/**
	 * 系统异常码定义  
	 * @ClassName: SystemError    
	 * @author 陈剑飞    
	 * @date 2017年3月8日 上午10:05:35    
	 * @version  v 1.0
	 */
	public enum SystemError implements BaseErrorCode {
		PRODUCT_NOT_FOUND("100000","SYSTEM_ERROR","商品不存在或已下架"),
		SYSTEM_ERROR("100100","SYSTEM_ERROR","系统错误"),
		SESSION_INVALID("100101","SESSION_INVALID","token 无效"),
		URL_NOT_FOUND("100102","URL_NOT_FOUND","请求URL不存在"),
		UN_AUTHORIZED("100103","UN_AUTHORIZED","未授权"),
		PARAM_PARSE_FAIL("100104","PARAM_PARSE_FAIL","参数解析失败"),
		METHOD_NOT_SUPPORTED("100105","METHOD_NOT_SUPPORTED","请求方法不支持"),
		CONTENT_TYPE_NOT_SUPPORTED("100106","CONTENT_TYPE_NOT_SUPPORTED","不支持当前媒体类型"),
		PARAM_VALIDATION_FAIL("100107","PARAM_VALIDATION_FAIL","参数验证失败"),
		PARAM_EMPTY("100108","PARAM_EMPTY","参数为空"),
		ILLEGAL_OPERATION("100109","ILLEGAL_OPERATION","管理员角色不可执行此操作"),
		DATA_NOT_EXISTS("100201","DATA_NOT_EXISTS","数据不存在"),
		DATA_EXISTS("100202","DATA_EXISTS","数据已存在"),
		OPERATION_FAIL("100203","OPERATION_FAIL","操作失败"),
		WEIXIN_NOT_EXISTS("100204","WEIXIN_NOT_EXISTS","企业微信渠道公众号未绑定"),
		DATA_MAX("100205","DATA_MAX","数据数量过大"),
		USER_MAX("100207","USER_MAX","导入用户数量超出企业可开通的剩余账号数量"),
		FILE_MAX("100206","FILE_MAX","文件内容过大，最大导入2MB内容");
		private String code;
		private String errorCode;
		private String msg;
		
		@Override
		public String getCode() {
			return code;
		}
		
		@Override
		public void setCode(String code) {
			this.code = code;
		}
		
		@Override
		public String getErrorCode() {
			return errorCode;
		}
		
		@Override
		public void setErrorCode(String errorCode) {
			this.errorCode = errorCode;
		}
		
		@Override
		public String getMsg() {
			return msg;
		}
		
		@Override
		public void setMsg(String msg) {
			this.msg = msg;
		}

		private SystemError(String code,String errorCode,String msg){
			this.code = code;
			this.errorCode=errorCode;
			this.msg=msg;
		}
	}
	
	/**
	 * 用户异常
	 * @ClassName: UserError    
	 * @author 陈剑飞    
	 * @date 2017年3月8日 上午10:07:09    
	 * @version  v 1.0
	 */
	public enum UserError implements BaseErrorCode{
		
		USER_NOT_FOUND("200101","USER_NOT_FOUND","用户不存在"),
		USER_EXISTS("200102","USER_EXISTS","用户已存在"),
		NAME_OR_PASSWORD_ERROR("200103","PASSWORD_ERROR","登录名或密码错误"),
		USER_PENDING_AUDIT("200104","USER_PENDING_AUDIT","用户待审核状态，不能登录"),
		USER_IS_NOT_ENABLE("200105","USER_IS_NOT_ENABLE","用户不是启用状态,不能重置密码"),
		SEND_VERIFY_CODE_FAILED("200106","SEND_VERIFY_CODE_FAILED","发送验证码失败"),
		UNLAWFUL_OPERATE("200107","UNLAWFUL_OPERATE","非法操作"),
		VERIFY_CODE_INVALID("200108","VERIFY_CODE_INVALID","验证码失效"),
		VERIFY_CODE_ERROR("200109","VERIFY_CODE_ERROR","短信验证码错误"),
		AUDIT_FAILED("200111","AUDIT_FAILED","审核失败"),
		RESET_PASSWORD_FAILED("200112","RESET_PASSWORD_FAILED","重置密码失败"),
		INDEX_FAILED("200113","INDEX_FAILED","获取首页信息失败"),
		ROLE_EXISTS("200114","ROLE_EXISTS","角色名称已存在"),
		USER_DISABLED("200110","USER_DISABLED","当前账号已被停用，具体情况请联系您的管理员。"),
		USER_EXPIRE("200120","USER_DISABLED","当前账号已过期，具体情况请联系您的管理员。"),
		SEND_VERIFY_CODE_OFTEN("200115","SEND_VERIFY_CODE_OFTEN","发送验证码频繁，发送失败，请稍后重试"),
		APPLY_GET_USER_INFO_ERROR("200116","APPLY_GET_USER_INFO_ERROR","申请成功，获取用户信息失败，请登录"),
		OPERATE_ERROR("200117","OPERATE_ERROR","操作失败"),
		PARAMETER_ERROR("200118","PARAMETER_ERROR","参数错误"),
		PHONE_ERROR("200119","PHONE_ERROR","手机号码错误"),
		NOT_CREATE_IDENTITY("200120","NOT_CREATE_IDENTITY","一个手机号最多绑定9个身份,请使用其他号码添加用户");
		private String code;
		private String errorCode;
		private String msg;
		@Override
		public String getCode() {
			return code;
		}
		@Override
		public void setCode(String code) {
			this.code = code;
		}
		@Override
		public String getErrorCode() {
			return errorCode;
		}
		@Override
		public void setErrorCode(String errorCode) {
			this.errorCode = errorCode;
		}
		@Override
		public String getMsg() {
			return msg;
		}
		@Override
		public void setMsg(String msg) {
			this.msg = msg;
		}

		private UserError(String code,String errorCode,String msg){
			this.code = code;
			this.errorCode=errorCode;
			this.msg=msg;
		}
	}
	
	/**
	 * 渠道异常
	 * @ClassName: ChannelError    
	 * @author 李星星
	 * @date 2017年3月10日 上午10:07:09    
	 * @version  v 1.0
	 */
	public enum ChannelError implements BaseErrorCode{
		CHANNELTYPE_LENGTH("200201","CHANNELTYPE_LENGTH","渠道类型名称、不得超过7个字符"),
		CHANNELTYPE_ERROR("200202","CHANNELTYPE_ERROR","渠道类型名称不允许包含系统等关键字"),
		CHANNELTYPE_EXIST("200203","CHANNELTYPE_EXIST","渠道类型名称不允许出现重复"),
		CHANNELTYPE_SONEXIST("200204","CHANNELTYPE_SONEXIST","渠道类型下存在子渠道、不允许删除"),
		CHANNELORG_EXIST("200205","CHANNELORG_EXIST","渠道层级名称不允许出现重复"),
		CHANNELSUBORDINATE_EXIST("200206","CHANNELSUBORDINATE_EXIST","存在下级层级不能删除、或者该层级存在渠道信息"),
		CHANNELTYPENAME_EXIST("200207","CHANNELTYPENAME_EXIST","同一渠道类型,已经存在此渠道,不可在重复"),
		CHANNEL_ARGS_ERROR("200208", "CHANNEL_ARGS_ERROR", "参数传入不正确"),
		CHANNEL_EXIST("200208", "CHANNEL_EXIST", "渠道已经存在"),
		CHANNEL_NOT_NULL("200208", "CHANNEL_NOT_NULL", "该参数为必传"),
		CHANNEL_NAME_LENG_ERRO("200208", "CHANNEL_NOT_NULL", "渠道名称不能超过40个字符"),
		CHANNEL_INFO_NOT_EXISTS("200209", "CHANNEL_INFO_NOT_EXISTS", "终端渠道不存在"),
		CHANNEL_NOT_OPEN_PHONEMGRSTATUS("200210", "CHANNEL_NOT_OPEN_PHONEMGRSTATUS", "该功能已被管理员禁用，请联系管理员"),
		CHANNEL_NULL("100100", "CHANNEL_NULL", "参数不能为空"),
		CHANNEL_NOT_EXIST("100200", "CHANNEL_NOT_EXIST", "文件不存在"),
		CHANNEL_FORMAT_ERROR("100200", "CHANNEL_FORMAT_ERROR", "格式不正确"),
		CHANNEL_EXCEL_ERROR("100300", "CHANNEL_EXCEL_ERROR", "excel格式不正确"),
		CHANNEL_TYPE_ERROR("100400", "CHANNEL_TYPE_ERROR", "类型不正确"),
		CHANNEL_HAVE_BIND("100500", "CHANNEL_HAVE_BIND", "渠道已绑定终端"),
		CHANNEL_STATUS_ERROR("100500", "CHANNEL_STATUS_ERROR", "渠道状态错误"),
		CHANNEL_VERCODE_ERROR("100500", "CHANNEL_VERCODE_ERROR", "验证码错误");
		private String code;
		private String errorCode;
		private String msg;
		
		@Override
		public String getCode() {
			return code;
		}
		
		@Override
		public void setCode(String code) {
			this.code = code;
		}
		
		@Override
		public String getErrorCode() {
			return errorCode;
		}
		
		@Override
		public void setErrorCode(String errorCode) {
			this.errorCode = errorCode;
		}
		
		@Override
		public String getMsg() {
			return msg;
		}
		
		@Override
		public void setMsg(String msg) {
			this.msg = msg;
		}

		private ChannelError(String code,String errorCode,String msg){
			this.code = code;
			this.errorCode=errorCode;
			this.msg=msg;
		}
	}
	
	/**
	 * 微信错误码定义 模块前缀定义 2003
	 * @ClassName: WechatError    
	 * @author 陈剑飞    
	 * @date 2017年5月16日 上午9:36:27    
	 * @version  v 1.0
	 */
	public enum WechatError implements BaseErrorCode{
		AUTH_FAIL("200301","AUTH_FAIL","授权失败"),
		UN_AUTH("200302","UN_AUTH","公众号未授权");
		private String code;
		private String errorCode;
		private String msg;
		
		@Override
		public String getCode() {
			return code;
		}
		
		@Override
		public void setCode(String code) {
			this.code = code;
		}
		
		@Override
		public String getErrorCode() {
			return errorCode;
		}
		
		@Override
		public void setErrorCode(String errorCode) {
			this.errorCode = errorCode;
		}
		
		@Override
		public String getMsg() {
			return msg;
		}
		
		@Override
		public void setMsg(String msg) {
			this.msg = msg;
		}

		private WechatError(String code,String errorCode,String msg){
			this.code = code;
			this.errorCode=errorCode;
			this.msg=msg;
		}
	}
	
	/**
	 * H5错误码定义 模块前缀定义 2004
	 * @ClassName: WechatError    
	 * @author 陈剑飞    
	 * @date 2017年5月16日 上午9:36:27    
	 * @version  v 1.0
	 */
	public enum H5Error implements BaseErrorCode{
		OPENID_NOET_EXIST("200401","OPENID_NOET_EXIST","openid 未授权"),
		H5TOKEN_INVALID("200402","H5TOKEN_INVALID","h5token 无效"),
		PARENT_INVALID("200403","H5TOKEN_INVALID","参数不正确"),
		COMPANY_NOET_EXIST("200404","H5TOKEN_INVALID","company 不存在该公司"),
		WEIXIN_AUTH_FAIL("200405","WEIXIN_AUTH_FAIL","微信授权失败");
		private String code;
		private String errorCode;
		private String msg;
		
		@Override
		public String getCode() {
			return code;
		}
		
		@Override
		public void setCode(String code) {
			this.code = code;
		}
		
		@Override
		public String getErrorCode() {
			return errorCode;
		}
		
		@Override
		public void setErrorCode(String errorCode) {
			this.errorCode = errorCode;
		}
		
		@Override
		public String getMsg() {
			return msg;
		}
		
		@Override
		public void setMsg(String msg) {
			this.msg = msg;
		}

		private H5Error(String code,String errorCode,String msg){
			this.code = code;
			this.errorCode=errorCode;
			this.msg=msg;
		}
	}
	
	/**
	 * @ClassName: MemberError 
	 * @Description: 会员相关异常
	 * @author 刘灶勇
	 * @date 2017年6月23日 上午11:23:53
	 */
	public enum MemberError implements BaseErrorCode {
		DATA_CARD_EXISTS("200501","DATA_CARD_EXISTS","需要领取的会员卡已经存在"),
		CARD_STATUS_ERROR("200502","CARD_STATUS_ERROR","会员卡非使用中状态领取失败");
		private String code;
		private String errorCode;
		private String msg;
		
		@Override
		public String getCode() {
			return code;
		}
		
		@Override
		public void setCode(String code) {
			this.code = code;
		}
		
		@Override
		public String getErrorCode() {
			return errorCode;
		}
		
		@Override
		public void setErrorCode(String errorCode) {
			this.errorCode = errorCode;
		}
		
		@Override
		public String getMsg() {
			return msg;
		}
		
		@Override
		public void setMsg(String msg) {
			this.msg = msg;
		}

		private MemberError(String code,String errorCode,String msg){
			this.code = code;
			this.errorCode=errorCode;
			this.msg=msg;
		}
	}
	
	/**
	 * @ClassName: OrganizeError 
	 * @Description: 组织错误码
	 * @author 刘灶勇
	 * @date 2017年7月12日 上午10:55:56
	 */
	public enum OrganizeError implements BaseErrorCode {
		COMPANY_NOT_EXISTS("200601","COMPANY_NOT_EXISTS","企业不存在"),
		ORGANIZE_NOT_EXISTS("200602","ORGANIZE_NOT_EXISTS","组织不存在"),
		ORGANIZE_NAME_EXISTS("200603","ORGANIZE_NAME_EXISTS","组织名称已经存在"),
		PARENT_NOT_EXISTS("200604","PARENT_NOT_EXISTS","上级部门不存在，请重新选择"),
		PARENT_NOT_SUPERIOR_DEPARTMENT("200605","PARENT_NOT_SUPERIOR_DEPARTMENT","所选上级部门已为最末级，请选择其它部门"),
		PARENT_LAST_LEVEL("200606","PARENT_LAST_LEVEL","部门已到最末级，无法添加子级"),
		REALNAME_NULL("200607","REALNAME_NULL","真实姓名不能为空"),
		VIVATE_URL_LOSE("200608","VIVATE_URL_LOSE","地址参数错误，团队邀请地址已失效"),
		LEVEL_MAX_ERROR("200609","LEVEL_MAX_ERROR","级别数已超过五级"),
		ORGANIZE_MOVE_NAME_EXISTS("200610","ORGANIZE_MOVE_NAME_EXISTS","迁移目标组织下，组织名称已经存在");
		private String code;
		private String errorCode;
		private String msg;
		
		@Override
		public String getCode() {
			return code;
		}
		
		@Override
		public void setCode(String code) {
			this.code = code;
		}
		
		@Override
		public String getErrorCode() {
			return errorCode;
		}
		
		@Override
		public void setErrorCode(String errorCode) {
			this.errorCode = errorCode;
		}
		
		@Override
		public String getMsg() {
			return msg;
		}
		
		@Override
		public void setMsg(String msg) {
			this.msg = msg;
		}

		private OrganizeError(String code,String errorCode,String msg){
			this.code = code;
			this.errorCode=errorCode;
			this.msg=msg;
		}
	}
	
	public static void main(String[] args) {
		System.out.println(ErrorCode.SystemError.SESSION_INVALID);
	}
}
