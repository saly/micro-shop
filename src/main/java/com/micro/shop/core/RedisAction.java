package com.micro.shop.core;

import redis.clients.jedis.Jedis;


/**  
 * redis操作接口定义类
 * @ClassName: RedisAction    
 * @author 陈剑飞    
 * @date 2017年2月16日 下午3:45:31    
 * @version  v 1.0
 * @param <T>    
 */
public abstract interface RedisAction<T> {
	/**
	 * jedis 操作  
	 * @author 陈剑飞    
	 * @Title: doAction    
	 * @param jedis
	 * @Return: T 返回值
	 */
	T doAction(Jedis jedis);
}
