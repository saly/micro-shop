package com.micro.shop.core.mybatis;

import java.io.Serializable;
import java.util.List;

/**
 * 分页对象.
 * 
 */
public final class Page<T> implements Serializable {

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}

	/**
	 * 默认的序列化版本 id.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 分页查询�?��记录位置.
	 */
	private int begin;
	/**
	 * 分页查看下结束位�?
	 */
	private int end;
	/**
	 * 每页显示记录�?
	 */
	private int length = 20;
	/**
	 * 查询结果总记录数.
	 */
	private long totalRecords;
	/**
	 * 当前页码.
	 */
	private int pageNo;
	/**
	 * 总共页数.
	 */
	private int pageCount;
	
	private String sort;//排序
	
	/**
	 * 结果列表
	 */
	private List<T> list;

	public Page() {
	}

	/**
	 * 构�?函数.
	 * 
	 * @param begin
	 * @param length
	 */
	public Page(int begin, int length) {
		this.begin = begin;
		this.length = length;
		this.end = this.begin + this.length;
		this.pageNo = (int) Math.floor((this.begin * 1.0d) / this.length) + 1;
	}

	/**
	 * @param begin
	 * @param length
	 * @param count
	 */
	public Page(int begin, int length, int totalRecords) {
		this(begin, length);
		this.totalRecords = totalRecords;
	}

	/**
	 * 设置页数，自动计算数据范�?
	 * 
	 * @param pageNo
	 */
	public Page(int pageNo) {
		this.pageNo = pageNo;
		pageNo = pageNo > 0 ? pageNo : 1;
		this.begin = this.length * (pageNo - 1);
		this.end = this.length * pageNo;
	}

	/**
	 * @return the begin
	 */
	public int getBegin() {
		return begin;
	}

	/**
	 * @return the end
	 */
	public int getEnd() {
		return end;
	}

	/**
	 * @param end
	 *            the end to set
	 */
	public void setEnd(int end) {
		this.end = end;
	}

	/**
	 * @param begin
	 *            the begin to set
	 */
	public void setBegin(int begin) {
		this.begin = begin;
		if (this.length != 0) {
			this.pageNo = (int) Math.floor((this.begin * 1.0d) / this.length) + 1;
		}
	}

	/**
	 * @return the length
	 */
	public int getLength() {
		return length;
	}

	/**
	 * @param length
	 *            the length to set
	 */
	public void setLength(int length) {
		//length = length>200?200:length;
		this.length = length;
		if (this.begin != 0) {
			this.pageNo = (int) Math.floor((this.begin * 1.0d) / this.length) + 1;
		}
	}

	/**
	 * @return the totalRecords
	 */
	public long getTotalRecords() {
		return totalRecords;
	}

	/**
	 * @param totalRecords
	 *            the totalRecords to set
	 */
	public void setTotalRecords(long totalRecords) {
		this.totalRecords = totalRecords;
		this.pageCount = (int) Math.floor((this.totalRecords * 1.0d)
				/ this.length);
		if (this.totalRecords % this.length != 0) {
			this.pageCount++;
		}
	}

	/**
	 * @return the pageNo
	 */
	public int getPageNo() {
		return pageNo;
	}

	/**
	 * @param pageNo
	 *            the pageNo to set
	 */
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
		pageNo = pageNo > 0 ? pageNo : 1;
		this.begin = this.length * (pageNo - 1);
		this.end = this.length * pageNo;
	}

	/**
	 * @return the pageCount
	 */
	public int getPageCount() {
		if (pageCount == 0) {
			return 1;
		}
		return pageCount;
	}

	/**
	 * @param pageCount
	 *            the pageCount to set
	 */
	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder("begin=").append(begin)
				.append(", end=").append(end).append(", length=")
				.append(length).append(", totalRecords=").append(totalRecords)
				.append(", pageNo=").append(pageNo).append(", pageCount=")
				.append(pageCount);
		return builder.toString();
	}
}