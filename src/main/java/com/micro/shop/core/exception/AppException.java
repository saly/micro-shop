package com.micro.shop.core.exception;

import com.micro.shop.core.BaseErrorCode;
import com.micro.shop.core.ErrorMsg;

/**  
 * app异常类    
 * @ClassName: AppException    
 * @author 陈剑飞    
 * @date 2017年1月11日 上午10:48:34    
 * @version  v 1.0    
 */
public class AppException extends RuntimeException{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ErrorMsg errorMsg;
	
	public ErrorMsg getErrorMsg() {
		return errorMsg;
	}
	public AppException(Exception ex) {
		super(ex.getMessage(), ex);
		errorMsg = new ErrorMsg(ex);
	}
	public  AppException ok(){
		this.getErrorMsg().setCode("200");
		return this;
	}
	public AppException(ErrorMsg errorMsg) {
		super(errorMsg.getMsg());
		this.errorMsg = errorMsg;
	}
	
	public AppException(ErrorMsg errorMsg,Exception ex) {
		super(errorMsg.getMsg(), ex);
		this.errorMsg = errorMsg;
	}
	
	public AppException(String code, String msg) {
		this(new ErrorMsg(code, msg));
	}
	
	public AppException(String code,String errorCode,String msg) {
		this(new ErrorMsg(code,errorCode,msg));
	}
	
	public AppException(String code,String errorCode,String msg,Exception ex) {
		this(new ErrorMsg(code,errorCode,msg),ex);
	}
	
	public AppException(BaseErrorCode errorCode) {
		this(new ErrorMsg(errorCode.getCode(),errorCode.getErrorCode(),errorCode.getMsg()));
	}
	
	public AppException(BaseErrorCode errorCode,String msg) {
		this(new ErrorMsg(errorCode.getCode(),errorCode.getErrorCode(),msg));
	}
	
	public AppException(BaseErrorCode errorCode,Exception ex) {
		this(new ErrorMsg(errorCode.getCode(),errorCode.getErrorCode(),errorCode.getMsg()),ex);
	}
	
	public AppException(String msg) {
		this(new ErrorMsg("", msg));
	}
	
	public AppException(String msg,Exception ex) {
		this(new ErrorMsg("", msg),ex);
	}
	
	public AppException(int httpCode, String code, String msg) {
		this(new ErrorMsg(httpCode, code, msg));
	}
}
