package com.micro.shop.core.exception;

import java.util.Map;

@SuppressWarnings("serial")
public class ValidException extends Exception{

	private int code;
	
	private String message;
	
	private Map<String, Object> data;

	public ValidException(int code, String message, String exceptionMsg, Exception e){
		super(exceptionMsg, e);
		this.code = code;
		this.message = message;
	}
	
	public ValidException(int code, String message, Exception e){
		super(e);
		this.code = code;
		this.message = message;
	}
	
	public ValidException(int code, String message){
		this(code, message, null);
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "{\"code\":\""+ code + "\",\"message:\":\"" + message + "\"}";
	}
	
	
	
}
