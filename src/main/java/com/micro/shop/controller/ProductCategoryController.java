package com.micro.shop.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.micro.shop.core.ApiResponse;
import com.micro.shop.model.ProductCategory;
import com.micro.shop.service.ProductCategoryService;


@Controller
@RequestMapping("product/category")
public class ProductCategoryController {
	@Autowired
	ProductCategoryService productCategoryService;
	
	@RequestMapping("/findMain")
	@ResponseBody
	public ApiResponse<List<ProductCategory>> findMainProductCategory() {
		ApiResponse<List<ProductCategory>> response = new ApiResponse<List<ProductCategory>>();
		List<ProductCategory> productCategoryList = productCategoryService.findMainProductCategory();
		response.setData(productCategoryList);
		return response;
	}

}
