package com.micro.shop.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.micro.shop.core.ApiResponse;


@Controller
@RequestMapping("config")
public class ConfigController {
	
	
	@RequestMapping("/getvalue")
	@ResponseBody
	public ApiResponse<String> mallName() {
		ApiResponse<String> response = new ApiResponse<String>();
		response.setData("豫生活");
		return response;
	}
	
	

}
