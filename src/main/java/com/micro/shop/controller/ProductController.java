package com.micro.shop.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.micro.shop.core.ApiResponse;
import com.micro.shop.model.Product;
import com.micro.shop.model.ProductSku;
import com.micro.shop.service.ProductService;
import com.micro.shop.vo.ProductDetailVO;


@Controller
@RequestMapping("product")
public class ProductController {
	
	@Autowired
	ProductService productService;
	
	@RequestMapping("/findMainCategoryProduct")
	@ResponseBody
	public ApiResponse<List<Product>> findMainCategoryProduct(Integer categoryId) {
		ApiResponse<List<Product>> response = new ApiResponse<List<Product>>();
		List<Product> productList = productService.findProductByMainCategoryId(categoryId);
		response.setData(productList);
		return response;
	}

	
	@RequestMapping("/detail")
	@ResponseBody
	public ApiResponse<ProductDetailVO> productDetail(Integer productId) {
		ApiResponse<ProductDetailVO> response = new ApiResponse<ProductDetailVO>();
		ProductDetailVO  productDetail = productService.productDetail(productId);
		response.setData(productDetail);
		return response;
	}
	
	@RequestMapping("/price")
	@ResponseBody
	public ApiResponse<ProductSku> productSkuPrice(Integer productId,String propertyChildIds) {
		ApiResponse<ProductSku> response = new ApiResponse<ProductSku>();
		ProductSku  productSku = productService.productSkuPrice(productId, propertyChildIds);
		response.setData(productSku);
		return response;
	}
	
	
}
