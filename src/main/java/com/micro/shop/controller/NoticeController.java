package com.micro.shop.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.micro.shop.core.ApiResponse;
import com.micro.shop.model.Notice;
import com.micro.shop.service.NoticeService;

@Controller
@RequestMapping("notice")
public class NoticeController {

	@Autowired
	private NoticeService noticeService;
	
	@RequestMapping("/find")
	@ResponseBody
	public ApiResponse<List<Notice>> findNotice() {
		ApiResponse<List<Notice>> response = new ApiResponse<List<Notice>>();
		 List<Notice> notice = noticeService.findNotice();
		 response.setData(notice);
		return response;
	}
	
	@RequestMapping("/detail")
	@ResponseBody
	public ApiResponse<Notice> detail(Integer id) {
		ApiResponse<Notice> response = new ApiResponse<Notice>();
		 Notice notice = noticeService.findDetail(id);
		 response.setData(notice);
		return response;
	}
	
}
