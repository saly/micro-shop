package com.micro.shop.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.micro.shop.annotation.H5AuthAnnotion;
import com.micro.shop.core.ApiResponse;
import com.micro.shop.model.Order;
import com.micro.shop.service.OrderService;
import com.micro.shop.vo.CreateOrderVO;
import com.micro.shop.vo.OrderListVO;

/**
 * 用户Controller控制器
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/order")
public class OrderController extends BaseController{

	@Autowired
	private OrderService orderService;
	
	@RequestMapping("/create")
	@ResponseBody
	@H5AuthAnnotion
	public  ApiResponse<Order> create(HttpServletRequest request,CreateOrderVO createOrderVO) {
		ApiResponse<Order> orderResponse = new ApiResponse<Order>();
		Integer userId = getUserId();
		orderService.creatOrder(createOrderVO,userId);
		return orderResponse;
	}
	
	@RequestMapping("/list")
	@ResponseBody
	@H5AuthAnnotion
	public  ApiResponse<List<OrderListVO>> list(HttpServletRequest request,Integer status) {
		ApiResponse<List<OrderListVO>> orderResponse = new ApiResponse<List<OrderListVO>>();
		Integer userId = getUserId();
		List<OrderListVO> orderList = orderService.orderList(status, userId);
		orderResponse.setData(orderList);
		return orderResponse;
	}
	
}
