package com.micro.shop.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.micro.shop.annotation.H5AuthAnnotion;
import com.micro.shop.core.ApiResponse;
import com.micro.shop.model.Coupon;
import com.micro.shop.service.CouponService;
import com.micro.shop.vo.CouponVO;

/**
 * 用户Controller控制器
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/coupon")
public class CouponController extends BaseController{

	@Autowired
	private CouponService couponService;
	
	@RequestMapping("/list")
	@ResponseBody
	public ApiResponse<List<CouponVO>> getUserInfo() {
		 ApiResponse<List<CouponVO>> couponListResponse = new ApiResponse<List<CouponVO>>();
		 List<CouponVO> couponList = couponService.recommendCoupon();
		 couponListResponse.setData(couponList);
		 return couponListResponse;
	}
	
	@RequestMapping("/my")
	@ResponseBody
	@H5AuthAnnotion
	public List<Coupon> myCoupon(HttpServletRequest request,Integer status){
		Integer userId = getUserId();
		return couponService.myCoupon(userId, status);
	}
	
}
