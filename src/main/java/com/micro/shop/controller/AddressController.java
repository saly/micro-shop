package com.micro.shop.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.micro.shop.annotation.H5AuthAnnotion;
import com.micro.shop.core.ApiResponse;
import com.micro.shop.model.UserAddress;
import com.micro.shop.service.UserAddressService;
import com.micro.shop.vo.UserAddressSubmitVO;

@Controller
@RequestMapping("/user/address")
public class AddressController  extends BaseController{

	@Autowired
	UserAddressService userAddressService;
	
	@RequestMapping("/list")
	@ResponseBody
	@H5AuthAnnotion
	public ApiResponse<List<UserAddress>> getUserAddress(HttpServletRequest request) {
		 Integer userId = getUserId();
		 ApiResponse<List<UserAddress>> addressListResponse = new ApiResponse<List<UserAddress>>();
		 List<UserAddress> userAddressList = userAddressService.getUserAddress(userId);
		 addressListResponse.setData(userAddressList);
		 return addressListResponse;
	}
	
	@RequestMapping(path="/add",method=RequestMethod.POST)
	@ResponseBody
	@H5AuthAnnotion
	public ApiResponse<Boolean> addOrUpdateUserAddress(HttpServletRequest request,@RequestBody UserAddressSubmitVO userAddressSubmitVO) {
		 ApiResponse<Boolean> addOrUpdateUserAddressResponse = new ApiResponse<Boolean>();
		 Integer userId = getUserId();
		 userAddressSubmitVO.setUserId(userId);
		 userAddressService.addOrUpdateUserAddress(userAddressSubmitVO);
		 return addOrUpdateUserAddressResponse.setData(true);
	}
	
	@RequestMapping("/detail")
	@ResponseBody
	@H5AuthAnnotion
	public ApiResponse<UserAddress> getUserAddressDetail(HttpServletRequest request,Integer addressId) {
		 Integer userId = getUserId();
		 ApiResponse<UserAddress> addressListResponse = new ApiResponse<UserAddress>();
		 UserAddress userAddress = userAddressService.getUserAddress(userId, addressId);
		 addressListResponse.setData(userAddress);
		 return addressListResponse;
	} 
	
	@RequestMapping("/default")
	@ResponseBody
	@H5AuthAnnotion
	public ApiResponse<UserAddress> getUserDefaultAddress(HttpServletRequest request) {
		 Integer userId = getUserId();
		 ApiResponse<UserAddress> addressListResponse = new ApiResponse<UserAddress>();
		 UserAddress userDefaultAddress = userAddressService.getUserDefaultAddress(userId);
		 addressListResponse.setData(userDefaultAddress);
		 return addressListResponse;
	}
}
