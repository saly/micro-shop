package com.micro.shop.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.micro.shop.core.ApiResponse;
import com.micro.shop.model.Banner;
import com.micro.shop.service.BannerService;

@Controller
@RequestMapping("banner")
public class BannerController {
	
	@Autowired
	private BannerService bannerService;
	
	@RequestMapping("/find")
	@ResponseBody
	public ApiResponse<List<Banner>> findNotice() {
		ApiResponse<List<Banner>> response = new ApiResponse<List<Banner>>();
		 List<Banner> notice = bannerService.findBanner();
		 response.setData(notice);
		return response;
	}

	
}
