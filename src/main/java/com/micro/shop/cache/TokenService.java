package com.micro.shop.cache;

import com.micro.shop.model.Token;


public interface TokenService {
	
	Token queryByToken(String token);

	/**
	 * 生成token
	 * @param userId  用户ID
	 * @return        返回token信息
	 */
	Token createToken(Integer userId,String token);

	/**
	 * 设置token过期
	 * @param userId 用户ID
	 */
	void expireToken(long userId);

}
