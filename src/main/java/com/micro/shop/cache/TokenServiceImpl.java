package com.micro.shop.cache;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.micro.shop.model.Token;
import com.micro.shop.redis.RedisUtils;

@Service
public class TokenServiceImpl implements TokenService {

	/**
	 * 12小时后过期
	 */
	private final static int EXPIRE = 3600 * 12;
	
	@Autowired
	RedisUtils redisUtils;
	
	@Override
	public Token queryByToken(String token) {
		String tokenStr = redisUtils.get("token:"+token);
		return (Token) JSON.parse(tokenStr);
	}

	@Override
	public Token createToken(Integer userId,String token) {
		//当前时间
		Date now = new Date();
		//过期时间
		Date expireTime = new Date(now.getTime() + EXPIRE * 1000);

		//生成token
		//String token = generateToken();

		//保存或更新用户token
		Token tokenEntity = new Token();
		tokenEntity.setUserId(userId);
		tokenEntity.setToken(token);
		tokenEntity.setUpdateTime(now);
		tokenEntity.setExpireTime(expireTime);
		redisUtils.set("token:"+token, JSON.toJSONString(tokenEntity));
		return tokenEntity;
	}

	@Override
	public void expireToken(long userId) {

	}

	
}
