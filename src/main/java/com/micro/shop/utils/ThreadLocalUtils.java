package com.micro.shop.utils;

/**    
 * @Title: ThreadLocalUtils.java  
 * @Description: TODO  
 * @author fanchunfeng    
 * @date 2018年4月27日 下午3:19:24  
 * @version V1.0    
 */
public class ThreadLocalUtils {
	
	private static ThreadLocal<String> h5TokenHolder = new ThreadLocal<String>();
	

	public static void setH5token(String h5token){
		h5TokenHolder.set(h5token);
	}
	
	public static String getH5token(){
		return h5TokenHolder.get();
	}
	
	public static void removeH5token(){
		h5TokenHolder.remove();
	}
	
	
}
