package com.micro.shop.vo;

import java.util.List;

import com.micro.shop.model.Coupon;

public class CouponProductVO  extends Coupon{

	private static final long serialVersionUID = -4095962194902326164L;
	
	private List<Integer> productList;

	public List<Integer> getProductList() {
		return productList;
	}

	public void setProductList(List<Integer> productList) {
		this.productList = productList;
	}
	
	
	
	

}
