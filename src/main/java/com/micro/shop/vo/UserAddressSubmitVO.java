package com.micro.shop.vo;

public class UserAddressSubmitVO {
	
	private Integer addressId;
	/**
     * 收货人
     */
	private String name;

    /**
     * 联系电话
     */
    private String tel;

    /**
     * 省code
     */
    private Integer province;

    /**
     * 市code
     */
    private Integer city;

    /**
     * 区code
     */
    private Integer area;

    /**
     * 详细地址
     */
    private String address;

    /**
     * 邮编
     */
    private String zipcode;
    
    private String isDefault;
    
    private Integer userId;

	public Integer getAddressId() {
		return addressId;
	}

	public void setAddressId(Integer addressId) {
		this.addressId = addressId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public Integer getProvince() {
		return province;
	}

	public void setProvince(Integer province) {
		this.province = province;
	}

	public Integer getCity() {
		return city;
	}

	public void setCity(Integer city) {
		this.city = city;
	}

	public Integer getArea() {
		return area;
	}

	public void setArea(Integer area) {
		this.area = area;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(String isDefault) {
		this.isDefault = isDefault;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	
    
}
