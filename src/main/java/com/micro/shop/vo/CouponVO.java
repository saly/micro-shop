package com.micro.shop.vo;

import java.math.BigDecimal;

public class CouponVO {
	
	private Integer id;

	private String name;
	 
	private BigDecimal faceMoney;
	
	private BigDecimal limitMoney;
	 
	private String startTime;
	 
	private String endTime;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getFaceMoney() {
		return faceMoney;
	}

	public void setFaceMoney(BigDecimal faceMoney) {
		this.faceMoney = faceMoney;
	}

	public BigDecimal getLimitMoney() {
		return limitMoney;
	}

	public void setLimitMoney(BigDecimal limitMoney) {
		this.limitMoney = limitMoney;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	

}
