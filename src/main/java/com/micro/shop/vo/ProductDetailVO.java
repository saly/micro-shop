package com.micro.shop.vo;

import java.util.List;

import com.micro.shop.model.Product;
import com.micro.shop.model.ProductImage;
import com.micro.shop.model.ProductSku;

public class ProductDetailVO {
	
	private String thumbnail;
	
	private Product product;
	
	private List<ProductImage> productImageList;
	
	List<ProductSku> productSkuList;
	
	List<PropertyVO> propertyList;
	
	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public List<ProductImage> getProductImageList() {
		return productImageList;
	}

	public void setProductImageList(List<ProductImage> productImageList) {
		this.productImageList = productImageList;
	}

	public List<ProductSku> getProductSkuList() {
		return productSkuList;
	}

	public void setProductSkuList(List<ProductSku> productSkuList) {
		this.productSkuList = productSkuList;
	}

	public List<PropertyVO> getPropertyList() {
		return propertyList;
	}

	public void setPropertyList(List<PropertyVO> propertyList) {
		this.propertyList = propertyList;
	}
	
	
	

}
