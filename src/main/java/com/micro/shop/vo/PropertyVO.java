package com.micro.shop.vo;

import java.util.List;

public class PropertyVO {
	
	private Integer pid;
	
	private String name;
	
	List<PropertyValueVO> propertyValue;

	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<PropertyValueVO> getPropertyValue() {
		return propertyValue;
	}

	public void setPropertyValue(List<PropertyValueVO> propertyValue) {
		this.propertyValue = propertyValue;
	}
	
	

}
