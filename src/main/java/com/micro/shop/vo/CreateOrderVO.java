package com.micro.shop.vo;

public class CreateOrderVO {

	private String token;
	
	private String goodsJsonStr;
	
	private Integer province;
	
	private Integer city;
	
	private Integer area;
	
	private String address;
	
	private String name;
	
	private String tel;
	
	private String zipcode;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getGoodsJsonStr() {
		return goodsJsonStr;
	}

	public void setGoodsJsonStr(String goodsJsonStr) {
		this.goodsJsonStr = goodsJsonStr;
	}

	public Integer getProvince() {
		return province;
	}

	public void setProvince(Integer province) {
		this.province = province;
	}

	public Integer getCity() {
		return city;
	}

	public void setCity(Integer city) {
		this.city = city;
	}

	public Integer getArea() {
		return area;
	}

	public void setArea(Integer area) {
		this.area = area;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	
	
	
	
	
}
