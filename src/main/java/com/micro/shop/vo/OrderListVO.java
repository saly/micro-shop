package com.micro.shop.vo;

import java.math.BigDecimal;
import java.util.List;

public class OrderListVO {
	
    private Integer orderId;

    
    private String orderNo;
    
    
    
    private String createTime;
    
  
    private Integer uid;
    

    private BigDecimal subTotal;

 
    private BigDecimal total;
    
    private Integer status;

    
    private List<OrderProductVO> orderProducts;


	public Integer getOrderId() {
		return orderId;
	}


	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}


	public String getOrderNo() {
		return orderNo;
	}


	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}


	public String getCreateTime() {
		return createTime;
	}


	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}


	public Integer getUid() {
		return uid;
	}


	public void setUid(Integer uid) {
		this.uid = uid;
	}


	public BigDecimal getSubTotal() {
		return subTotal;
	}


	public void setSubTotal(BigDecimal subTotal) {
		this.subTotal = subTotal;
	}


	public BigDecimal getTotal() {
		return total;
	}


	public void setTotal(BigDecimal total) {
		this.total = total;
	}


	public List<OrderProductVO> getOrderProducts() {
		return orderProducts;
	}


	public void setOrderProducts(List<OrderProductVO> orderProducts) {
		this.orderProducts = orderProducts;
	}


	public Integer getStatus() {
		return status;
	}


	public void setStatus(Integer status) {
		this.status = status;
	}
    
    
}
