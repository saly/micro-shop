package com.micro.shop.vo;

public class OrderProductVO {
	
	private Integer productId;
	
	private Integer number;
	
	private String propertyChildIds;
	
	private Integer logisticsType;
	
	private String image;

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public String getPropertyChildIds() {
		return propertyChildIds;
	}

	public void setPropertyChildIds(String propertyChildIds) {
		this.propertyChildIds = propertyChildIds;
	}

	public Integer getLogisticsType() {
		return logisticsType;
	}

	public void setLogisticsType(Integer logisticsType) {
		this.logisticsType = logisticsType;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
	
	
	

}
