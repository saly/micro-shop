package com.micro.shop.service;

import java.util.List;

import com.micro.shop.vo.CreateOrderVO;
import com.micro.shop.vo.OrderListVO;

public interface OrderService {
	
	public void creatOrder(CreateOrderVO createOrderVO,Integer userId);
	
	public List<OrderListVO> orderList(Integer status,Integer userId);

}
