package com.micro.shop.service;

import java.util.List;

import com.micro.shop.model.Banner;

public interface BannerService {

	public List<Banner> findBanner();

}
