package com.micro.shop.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.micro.shop.mapper.UserAddressMapper;
import com.micro.shop.model.UserAddress;
import com.micro.shop.model.UserAddressCriteria;
import com.micro.shop.service.UserAddressService;
import com.micro.shop.vo.UserAddressSubmitVO;

@Service
public class UserAddressServiceImpl implements UserAddressService {

	@Autowired
	UserAddressMapper userAddressMapper;
	
	@Override
	public void addOrUpdateUserAddress(UserAddressSubmitVO userAddressSubmitVO) {
		int IsDefault = 0;
		if("true".equals(userAddressSubmitVO.getIsDefault())){
			IsDefault = 1;
		}
		if(1==IsDefault){
			UserAddress userAddress = new UserAddress();
			UserAddressCriteria userAddressCriteriaQuery = new UserAddressCriteria();
			userAddressCriteriaQuery.createCriteria().andUidEqualTo(userAddressSubmitVO.getUserId());
			userAddress.setIsdefault(0);
			userAddressMapper.updateByExampleSelective(userAddress, userAddressCriteriaQuery);
		}
		if(userAddressSubmitVO.getAddressId()==null || userAddressSubmitVO.getAddressId()==0){
			UserAddress userAddress = new UserAddress();
			BeanUtils.copyProperties(userAddressSubmitVO, userAddress);
			userAddress.setAddressId(null);
			userAddress.setUid(userAddressSubmitVO.getUserId());
			userAddress.setAddTime(Long.valueOf(new Date().getTime()/1000).intValue());
			userAddress.setIsdefault(IsDefault);
			userAddressMapper.insert(userAddress);
		}else{
			UserAddress userAddress = new UserAddress();
			BeanUtils.copyProperties(userAddressSubmitVO, userAddress);
			UserAddressCriteria userAddressCriteriaQuery = new UserAddressCriteria();
			userAddressCriteriaQuery.createCriteria().andAddressIdEqualTo(userAddressSubmitVO.getAddressId());
			userAddress.setAddressId(null);
			userAddress.setIsdefault(IsDefault);
			userAddressMapper.updateByExampleSelective(userAddress, userAddressCriteriaQuery);
		}
	}

	@Override
	public List<UserAddress> getUserAddress(Integer userId) {
		UserAddressCriteria userAddressCriteriaQuery = new UserAddressCriteria();
		userAddressCriteriaQuery.createCriteria().andUidEqualTo(userId);
		return userAddressMapper.selectByExample(userAddressCriteriaQuery);
	}

	@Override
	public UserAddress getUserAddress(Integer userId, Integer addressId) {
		UserAddressCriteria userAddressCriteriaQuery = new UserAddressCriteria();
		userAddressCriteriaQuery.createCriteria().andUidEqualTo(userId).andAddressIdEqualTo(addressId);
		List<UserAddress> userAddressList = userAddressMapper.selectByExample(userAddressCriteriaQuery);
		return CollectionUtils.isEmpty(userAddressList)?null:userAddressList.get(0);
	}

	@Override
	public UserAddress getUserDefaultAddress(Integer userId) {
		UserAddressCriteria userAddressCriteriaQuery = new UserAddressCriteria();
		userAddressCriteriaQuery.createCriteria().andUidEqualTo(userId).andDefaultEqualTo(1);
		List<UserAddress> userDefaultAddressList = userAddressMapper.selectByExample(userAddressCriteriaQuery);
		if(CollectionUtils.isEmpty(userDefaultAddressList)){
			userAddressCriteriaQuery = new UserAddressCriteria();
			userAddressCriteriaQuery.createCriteria().andUidEqualTo(userId);
			List<UserAddress> userAddressList = userAddressMapper.selectByExample(userAddressCriteriaQuery);
			if(CollectionUtils.isEmpty(userAddressList))
				return null;
			return userAddressList.get(0);
		}
		return userDefaultAddressList.get(0);
	}

}
