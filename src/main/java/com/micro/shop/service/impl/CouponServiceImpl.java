package com.micro.shop.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.micro.shop.core.exception.AppException;
import com.micro.shop.mapper.CouponMapper;
import com.micro.shop.mapper.CouponToProductMapper;
import com.micro.shop.mapper.GetCouponMapper;
import com.micro.shop.mapper.UserCouponMapper;
import com.micro.shop.model.Coupon;
import com.micro.shop.model.CouponCriteria;
import com.micro.shop.model.CouponCriteria.Criteria;
import com.micro.shop.model.CouponToProduct;
import com.micro.shop.model.CouponToProductCriteria;
import com.micro.shop.model.UserCoupon;
import com.micro.shop.model.UserCouponCriteria;
import com.micro.shop.service.CouponService;
import com.micro.shop.utils.DateUtils;
import com.micro.shop.vo.CouponProductVO;
import com.micro.shop.vo.CouponVO;

@Service
public class CouponServiceImpl implements CouponService {

	@Autowired
	CouponMapper couponMapper;
	
	@Autowired
	UserCouponMapper userCouponMapper;
	
	@Autowired
	GetCouponMapper getCouponMapper;
	
	@Autowired
	CouponToProductMapper couponToProductMapper;
	
	
	@Override
	public List<CouponVO> recommendCoupon() {
		Date dateNow = new Date();
		Integer time = Long.valueOf(dateNow.getTime()/1000).intValue();
		CouponCriteria couponCriteriaQuery = new CouponCriteria();
		couponCriteriaQuery.createCriteria()
		.andRecommendEqualTo(1)
		.andStatusEqualTo(1)
		.andStartTimeLessThan(time)
		.andEndTimeGreaterThan(time);
		List<Coupon> couponList = couponMapper.selectByExample(couponCriteriaQuery);
		List<CouponVO> couponVOList = new ArrayList<CouponVO>();
		if(CollectionUtils.isNotEmpty(couponList)){
			couponList.stream().forEach((coupon)->{
				CouponVO couponVO = new CouponVO();
				couponVO.setId(coupon.getId());
				couponVO.setName(coupon.getName());
				couponVO.setFaceMoney(coupon.getFaceMoney());
				couponVO.setLimitMoney(coupon.getLimitMoney());
				couponVO.setStartTime(DateUtils.stampToDate(String.valueOf(coupon.getStartTime()+"000"), DateUtils.DATETIME_FORMAT));
				couponVO.setEndTime(DateUtils.stampToDate(String.valueOf(coupon.getEndTime()+"1000"), DateUtils.DATETIME_FORMAT));
				couponVOList.add(couponVO);
			});
		}
		return couponVOList;
	}

	@Override
	public List<Coupon> myCoupon(Integer userId,Integer status) {
		Date dateNow = new Date();
		Integer time = Long.valueOf(dateNow.getTime()/1000).intValue();
		CouponCriteria couponCriteriaQuery = new CouponCriteria();
		Criteria criteria = couponCriteriaQuery.createCriteria()
				.andStatusEqualTo(1)
				.andUidEqualTo(userId)
		.andStartTimeGreaterThan(time);
		if(status!=null){
			criteria.andStatusEqualTo(status);
		}
		return couponMapper.selectByExample(couponCriteriaQuery);
	}

	@Override
	public void getCoupon(Integer couponId,Integer userId) {
		Date dateNow = new Date();
		Integer time = Long.valueOf(dateNow.getTime()/1000).intValue();
		CouponCriteria couponCriteria = new CouponCriteria();
		couponCriteria.createCriteria()
		.andIdEqualTo(couponId);
		Coupon coupon = couponMapper.selectByPrimaryKey(couponId);
		if(coupon==null)
			throw new AppException("优惠券不存在");
		//查看是否已经领取
		if(coupon.getTotalAmount()<=coupon.getNumber())
			throw new AppException("该优惠券已经全部发放完了！");
		if(0==coupon.getStatus())
			throw new AppException("该优惠券已失效！");
		if(time<coupon.getStartTime()|| time >coupon.getEndTime())
			throw new AppException("该优惠券未开始或已结束！");
		if(2==coupon.getType())
			throw new AppException("不可领取赠送券！");
		
		UserCouponCriteria userCouponCriteriaCount = new UserCouponCriteria();
		userCouponCriteriaCount.createCriteria()
		.andIdEqualTo(couponId)
		.andUidEqualTo(userId);
		//查看当前用户是否达到最大领取限度
		if(coupon.getMostHave()!=0){
			int userCouponCount = userCouponMapper.countByExample(userCouponCriteriaCount);
			if(userCouponCount>=coupon.getMostHave()){
				throw new AppException("您已达到该优惠券允许的最大单人领取限额！");
			}
		}
		//领取
		UserCoupon userCoupon = new UserCoupon();
		userCoupon.setUid(userId);
		userCoupon.setCouponId(couponId);
		userCoupon.setDeleteFlg(0);
		userCouponMapper.insert(userCoupon);
		int count = getCouponMapper.reduceCoupon(couponId);
		if(count<1)
			throw new AppException("该优惠券已经全部发放完了！");
	}

	public List<CouponProductVO> getCouponListByProductId(Integer storeId,Integer userId,
			List<Integer> productIds, Integer time) {
		/**
		 * 查询通用优惠券
		 */
		Date dateNow = new Date();
		time = Long.valueOf(dateNow.getTime()/1000).intValue();
		UserCouponCriteria userCouponCriteriaQuery = new UserCouponCriteria();
		userCouponCriteriaQuery.createCriteria()
		.andStoreIdEqualTo(storeId)
		.andUidEqualTo(userId)
		.andStartTimeLessThanOrEqualTo(time)
		.andEndTimeGreaterThanOrEqualTo(time)
		.andIsAllProductEqualTo(1)
		.andIsUseEqualTo(0)
		.andIsValidEqualTo(1)
		.andDeleteFlgEqualTo(0);
		List<CouponProductVO> userProductCouponList = new ArrayList<CouponProductVO>();
		
		List<UserCoupon> userCouponList = userCouponMapper.selectByExample(userCouponCriteriaQuery);
		userCouponCriteriaQuery = new UserCouponCriteria();
		userCouponCriteriaQuery.createCriteria()
		.andStoreIdEqualTo(storeId)
		.andUidEqualTo(userId)
		.andStartTimeLessThanOrEqualTo(time)
		.andEndTimeGreaterThanOrEqualTo(time)
		.andIsAllProductEqualTo(0)
		.andIsUseEqualTo(0)
		.andIsValidEqualTo(1)
		.andDeleteFlgEqualTo(0);
		if(CollectionUtils.isNotEmpty(userCouponList)){
			userCouponList.stream().forEach((userCoupon)->{
				CouponProductVO couponProductVO = new CouponProductVO();
				BeanUtils.copyProperties(userCoupon, couponProductVO);
				userProductCouponList.add(couponProductVO);
			});
		}
		

		/**
		 * 查询商品优惠券
		 */
		List<UserCoupon> userCouponListTemp = userCouponMapper.selectByExample(userCouponCriteriaQuery);
		if(CollectionUtils.isNotEmpty(userCouponListTemp)){
			List<Integer> userCouponIds = new ArrayList<Integer>();
			userCouponListTemp.stream().forEach((userCoupon)->{
				userCouponIds.add(userCoupon.getCouponId());
			});
			
			CouponToProductCriteria couponToProductCriteriaQuery = new CouponToProductCriteria();
			couponToProductCriteriaQuery.createCriteria()
			.andCouponIdIn(userCouponIds)
			.andProductIdIn(productIds);
			List<CouponToProduct> couponToProductList = couponToProductMapper.selectByExample(couponToProductCriteriaQuery);
			
			userCouponListTemp.stream().forEach((userCoupon)->{
				couponToProductList.stream().forEach((couponToProduct)->{
					if(userCoupon.getCouponId().equals(couponToProduct.getCouponId())){
						CouponProductVO couponProductVO = new CouponProductVO();
						BeanUtils.copyProperties(userCoupon, couponProductVO);
						userProductCouponList.add(couponProductVO);
					}
						
				});
			});
		}
		return userProductCouponList;
		
	}

	
}