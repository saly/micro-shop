package com.micro.shop.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.micro.shop.mapper.ProductCategoryMapper;
import com.micro.shop.model.ProductCategory;
import com.micro.shop.model.ProductCategoryCriteria;
import com.micro.shop.service.ProductCategoryService;

@Service
public class ProductCategoryServiceImpl implements ProductCategoryService {

	@Autowired
	ProductCategoryMapper productCategoryMapper;
	
	@Override
	public List<ProductCategory> findMainProductCategory() {
		ProductCategoryCriteria productCategoryQuery = new ProductCategoryCriteria();
		 productCategoryQuery.createCriteria().andCatFidEqualTo(0).andCatStatusEqualTo(Byte.valueOf("1"));
		return productCategoryMapper.selectByExample(productCategoryQuery);
	}
	
	

}
