package com.micro.shop.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.micro.shop.core.ErrorCode;
import com.micro.shop.core.ErrorCode.SystemError;
import com.micro.shop.core.exception.AppException;
import com.micro.shop.mapper.ProductImageMapper;
import com.micro.shop.mapper.ProductMapper;
import com.micro.shop.mapper.ProductSkuMapper;
import com.micro.shop.mapper.PropertyMapper;
import com.micro.shop.mapper.StoreMapper;
import com.micro.shop.model.Product;
import com.micro.shop.model.ProductCriteria;
import com.micro.shop.model.ProductCriteria.Criteria;
import com.micro.shop.model.ProductImage;
import com.micro.shop.model.ProductImageCriteria;
import com.micro.shop.model.ProductSku;
import com.micro.shop.model.ProductSkuCriteria;
import com.micro.shop.model.StoreCriteria;
import com.micro.shop.service.ProductService;
import com.micro.shop.vo.ProductDetailVO;
import com.micro.shop.vo.PropertyVO;
import com.micro.shop.vo.PropertyValueVO;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductMapper productMapper;
	
	@Autowired
	StoreMapper storeMapper;
	
	@Autowired
	ProductSkuMapper productSkuMapper;
	
	@Autowired
	PropertyMapper propertyMapper;
	
	@Autowired
	ProductImageMapper productImageMapper;
	
	
	@Override
	public List<Product> findProductByMainCategoryId(Integer categoryId) {
		ProductCriteria productQuery = new ProductCriteria();
		Criteria criteria = productQuery.createCriteria()
		.andStatusEqualTo(String.valueOf(1));
		if(categoryId!=null)
			criteria.andCategoryFidEqualTo(categoryId);
		return productMapper.selectByExample(productQuery);
	}

	@Override
	public ProductDetailVO productDetail(Integer productId) {
		
	    ProductDetailVO productDetail = new ProductDetailVO();
		/**
		 * 判断商品是否存在
		 */
		ProductCriteria productCriteriaQuery = new ProductCriteria();
		productCriteriaQuery.createCriteria().andProductIdEqualTo(productId)
		.andStatusEqualTo(String.valueOf(1));
		Product product = productMapper.selectByPrimaryKey(productId);
		if(product==null|| !"1".equals(product.getStatus()))
			throw new AppException(ErrorCode.SystemError.PRODUCT_NOT_FOUND);
		productDetail.setProduct(product);
		/**
		 * 获取商品属性及SKU
		 */
		if(product.getHasProperty()!=null && 1==product.getHasProperty().intValue()){
			//获取SKU
			ProductSkuCriteria productSkuCriteriaQuery = new ProductSkuCriteria();
			productCriteriaQuery.createCriteria().andProductIdEqualTo(productId);
			List<ProductSku> productSkuList = productSkuMapper.selectByExample(productSkuCriteriaQuery);
			if(CollectionUtils.isNotEmpty(productSkuList))
				productDetail.setProductSkuList(productSkuList);
			
			//获取属性
			List<PropertyVO> propertyList = propertyMapper.findPropertyByProductId(productId);
			if(CollectionUtils.isNotEmpty(propertyList)){
				productDetail.setPropertyList(propertyList);
				List<PropertyValueVO> propertyValueList = propertyMapper.findPropertyValueByProductId(productId);
				propertyList.stream().forEach((property)->{
					List<PropertyValueVO> propertyValueTemp = new ArrayList<PropertyValueVO>();
					propertyValueList.stream().forEach((propertyValue)->{
						if(property.getPid().equals(propertyValue.getPid())){
							propertyValueTemp.add(propertyValue);
						}
					});
					property.setPropertyValue(propertyValueTemp);
				});
			}
				
			productDetail.setPropertyList(propertyList);
		}
		/**
		 * 店铺状态判断
		 */
		Integer storeId = product.getStoreId();
		StoreCriteria storeCriteriaQuery = new StoreCriteria();
		storeCriteriaQuery.createCriteria().andStoreIdEqualTo(storeId)
		.andStatusEqualTo(String.valueOf(1));
		
		int storeCount = storeMapper.countByExample(storeCriteriaQuery);
		if(storeCount<1)
			throw new AppException(ErrorCode.SystemError.PRODUCT_NOT_FOUND);
		/**
		 * 查询商品图片
		 */
		ProductImageCriteria productImageCriteriaQuery = new ProductImageCriteria();
		productImageCriteriaQuery.createCriteria().andProductIdEqualTo(productId);
		List<ProductImage> productImageList = productImageMapper.selectByExample(productImageCriteriaQuery);
		productDetail.setProductImageList(productImageList);
		
		//设置缩略图
		productDetail.setThumbnail(StringUtils.isNotBlank(product.getImage())?product.getImage():CollectionUtils.isEmpty(productImageList)?"":productImageList.get(0).getImage());
		return productDetail;
	}

	@Override
	public ProductSku productSkuPrice(Integer productId, String propertyChildIds) {
		ProductSkuCriteria productSkuCriteriaQuery = new ProductSkuCriteria();
		productSkuCriteriaQuery.createCriteria().andProductIdEqualTo(productId).andPropertiesEqualTo(propertyChildIds);
		List<ProductSku> productSkuList = productSkuMapper.selectByExample(productSkuCriteriaQuery);
		return CollectionUtils.isEmpty(productSkuList)?null:productSkuList.get(0);
	}
	
	

}
