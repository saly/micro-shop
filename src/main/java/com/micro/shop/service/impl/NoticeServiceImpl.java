package com.micro.shop.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.micro.shop.mapper.NoticeMapper;
import com.micro.shop.model.Notice;
import com.micro.shop.service.NoticeService;

@Service
public class NoticeServiceImpl implements NoticeService {

	@Autowired
	NoticeMapper noticeMapper;
	
	@Override
	public List<Notice> findNotice() {
		return noticeMapper.findNotice();
	}

	@Override
	public Notice findDetail(Integer id) {
		return noticeMapper.findDetail(id);
	}

}
