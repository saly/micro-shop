package com.micro.shop.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.micro.shop.mapper.BannerMapper;
import com.micro.shop.model.Banner;
import com.micro.shop.service.BannerService;

@Service
public class BannerServiceImpl implements BannerService {

	@Autowired
	BannerMapper bannerMapper;
	
	@Override
	public List<Banner> findBanner() {
		return bannerMapper.findBanner();
	}

}
