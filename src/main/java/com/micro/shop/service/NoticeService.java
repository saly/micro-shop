package com.micro.shop.service;

import java.util.List;

import com.micro.shop.model.Notice;

public interface NoticeService {

	public List<Notice> findNotice();
	
	public Notice findDetail(Integer id);
	
}
