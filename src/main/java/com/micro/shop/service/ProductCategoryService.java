package com.micro.shop.service;

import java.util.List;

import com.micro.shop.model.ProductCategory;

public interface ProductCategoryService {
	
	public List<ProductCategory> findMainProductCategory();

}
