package com.micro.shop.service;

import java.util.List;

import com.micro.shop.model.Coupon;
import com.micro.shop.vo.CouponProductVO;
import com.micro.shop.vo.CouponVO;

public interface CouponService {
	
	/**
	 * 推荐优惠券
	 * @return
	 */
	public List<CouponVO> recommendCoupon();
	
	/**
	 * 我的优惠券
	 * @param userId
	 * @return
	 */
	public List<Coupon> myCoupon(Integer userId,Integer status);
	
	
	/**
	 * 领优惠券
	 * @param couponId
	 * @param userId
	 */
	public void getCoupon(Integer couponId,Integer userId);
	
	/**
	 * 获取产品优惠券
	 * @param storeId
	 * @param productIds
	 * @param time
	 * @return
	 */
	List<CouponProductVO> getCouponListByProductId(Integer storeId,Integer userId,List<Integer> productIds,Integer time);
	
	

}
