package com.micro.shop.service;

import java.util.List;

import com.micro.shop.model.UserAddress;
import com.micro.shop.vo.UserAddressSubmitVO;

public interface UserAddressService {
	
	void addOrUpdateUserAddress(UserAddressSubmitVO userAddressSubmitVO);
	
	List<UserAddress> getUserAddress(Integer userId);
	
	UserAddress getUserAddress(Integer userId,Integer addressId);
	
	UserAddress getUserDefaultAddress(Integer userId);

}
