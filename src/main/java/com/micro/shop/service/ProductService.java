package com.micro.shop.service;

import java.util.List;

import com.micro.shop.model.Product;
import com.micro.shop.model.ProductSku;
import com.micro.shop.vo.ProductDetailVO;

public interface ProductService {
	
	List<Product> findProductByMainCategoryId(Integer categoryId);
	
	ProductDetailVO productDetail(Integer productId);
	
	ProductSku productSkuPrice(Integer productId,String propertyChildIds);

}
